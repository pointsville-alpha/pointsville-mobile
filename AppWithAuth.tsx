import React from 'react'
import { Authenticator, ConfirmSignIn, ConfirmSignUp, ForgotPassword, RequireNewPassword, SignIn, VerifyContact } from 'aws-amplify-react-native'
import CustomSignIn from './src/screens/sign_in'
import CustomSignUp from './src/screens/sign_up'
import CustomForgotPassword from './src/screens/forgot_password/ForgotPassword'
import CustomConfirmSignUp from './src/screens/sign_up/ConfirmSignUp'
const AppWithAuth = (props:any) => {


    return (

        <Authenticator hideDefault = {true} onStateChange={props.onStateChange} authState='signIn'>
            <CustomSignIn whoAmI={props.whoAmI}/>
            <CustomSignUp/>
            <CustomConfirmSignUp/>
            <CustomForgotPassword/>
        </Authenticator>
    )
}

export default AppWithAuth;
