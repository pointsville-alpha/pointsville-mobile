import React, { useEffect, useState } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import configure from './src/redux/config/store'
import AppNavigation from './src/navigation/AppNavigation'
import Authenticator from './AppWithAuth'
import SplashScreen from 'react-native-splash-screen'
import { WHO_AM_I } from './src/utilities/modules/GraphQLAPI'
import { useMutation } from '@apollo/client'
import { Auth, Hub } from 'aws-amplify'
import Loader from './src/utilities/modules/Loader'
const { store, persistor } = configure()


export default function App() {

    const [whoami] = useMutation(WHO_AM_I);
    const [authState, setAuthState] = useState('loading')

    Auth.currentAuthenticatedUser().then(result=>{
        setAuthState('signedIn')
        SplashScreen.hide()
    }).catch(e=>{
        setAuthState('signIn')
        SplashScreen.hide()
    })

    const handleAuthStateChange = (state: string) => {
        console.log('handleAuthStateChange', state)
        setAuthState(state)
        
    }
    
    const whoAmI = (user: any) => {
        console.log('user',user)
        whoami({variables:user}).then(result => {
            console.log("whoami", result)
        }).catch(e => {
            console.warn("whoami error", e)
        });
    }
    
    useEffect(()=>{

        // Hub.listen('auth', ({ payload: { event, data } }) => {
        //     switch (event) {
        //       case 'signIn':
        //       case 'cognitoHostedUI':
        //         console.log("cognitoHostedUI",JSON.stringify(data))
        //         break;
        //       case 'signOut':
        //         break;
        //       case 'signIn_failure':
        //       case 'cognitoHostedUI_failure':
        //         console.log('Sign in failure', data);
        //         break;
        //         default:
        //             console.log('default', data);
        //     }
        //   });
      

        return(()=>{
            //component will unmount
        })
    })

    return (

        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                {
                    authState === 'signedIn'
                        ?
                        <AppNavigation />
                        :
                        authState != 'loading'?
                        <Authenticator onStateChange={handleAuthStateChange} whoAmI={whoAmI} />
                        :
                        <Loader loading={authState === 'loading'}/>

                }

            </PersistGate>
        </Provider>
    )
}