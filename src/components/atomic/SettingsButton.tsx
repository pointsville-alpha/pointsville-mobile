import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Logo from './Logo'
import { SETTINGS_NAVIGATION_KEY } from '../../utilities'

export default function SettingsButton() {
    const navigation = useNavigation()
    return <TouchableOpacity children={<Logo size='small' />}
        onPress={() => navigation.navigate(SETTINGS_NAVIGATION_KEY)} />
}