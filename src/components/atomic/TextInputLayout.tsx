// @ts-nocheck
'use strict';
import React from 'react'
import {
    Animated,
    StyleSheet,
    View
} from 'react-native'
import {
    Colors,
    FONTSIZE,
    normalize
} from '../../utilities'

const DEFAULT_PLACEHOLDER_COLOR = Colors.WHITE_ALPHA_50
const DEFAULT_LABEL_COLOR = Colors.WHITE_ALPHA_50
const DEFAULT_LABEL_ERROR_COLOR = Colors.BRIGHT_RED

export default class TextInputLayout extends React.Component {

    static defaultProps = {
        hintColor: DEFAULT_PLACEHOLDER_COLOR,
        errorColor: DEFAULT_LABEL_ERROR_COLOR,
        focusColor: DEFAULT_LABEL_COLOR,
        labelFontSize: FONTSIZE.regular,
        labelText: '',
    }

    state = {
        showLabel: false,
        labelAnimationValue: new Animated.Value(0),
        isFocused: false,
        isError: false,
    }

    constructor(props) {
        super(props)
        this._onBlur = this._onBlur.bind(this)
        this._onFocus = this._onFocus.bind(this)
        this._onChangeText = this._onChangeText.bind(this)
        this._handleChildren(props)
    }

    _springValue(animatedValue, toValue) {
        Animated.spring(animatedValue, {
            toValue: toValue,
            friction: 10,
            useNativeDriver: false
        }).start()
    }

    /**
     * font, size, color, gravity, hintColor
     * @param props
     * @private
     */
    _handleChildren(props) {
        let edtChild = React.Children.only(props.children)
        this._oriEdtChild = edtChild
        this._oriEdtStyle = StyleSheet.flatten([edtChild.props.style])
        this._oriOnFocus = edtChild.props.onFocus
        this._oriOnBlur = edtChild.props.onBlur
        this._oriOnChangeText = edtChild.props.onChangeText

        const textValue = edtChild.props.value || edtChild.props.defaultValue

        if (textValue) {
            this._edtText = textValue
            this.state.showLabel = true
            this.state.labelAnimationValue = new Animated.Value(1)
        }
        this._edtChild = React.cloneElement(edtChild, {
            onFocus: this._onFocus,
            onBlur: this._onBlur,
            onChangeText: this._onChangeText,
            style: [edtChild.props.style, styles.editChild],
            placeholder: null,
            underlineColorAndroid: 'transparent'
        })

        let { height, fontSize } = this._oriEdtStyle
        let labelHeight = fontSize + 3
        let labelTransY = this.state.labelAnimationValue.interpolate({
            inputRange: [0, 1],
            outputRange: [normalize(29), labelHeight - this.props.labelFontSize + 10],
        })
        let labelFontSize = this.state.labelAnimationValue.interpolate({
            inputRange: [0, 1],
            outputRange: [fontSize, normalize(12)]
        })
        this._labelStyle = {
            fontSize: labelFontSize,
            height: labelHeight,
            transform: [{ translateY: labelTransY }]
        }
    }

    _onFocus() {
        if (!this._edtText)
            this.setState({ showLabel: true, isFocused: true })
        else this.setState({ isFocused: true })
        this._oriOnFocus && this._oriOnFocus()
    }

    _onBlur() {
        let isError = false
        if (!this._edtText)
            this.setState({ showLabel: false, isFocused: false, isError })
        else
            this.setState({ isFocused: false, isError })
        this._oriOnBlur && this._oriOnBlur()
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        this._handleChildren(nextProps)
    }

    UNSAFE_componentWillUpdate(nextProps, nextState) {
        if (nextState.showLabel !== this.state.showLabel) {
            this._springValue(this.state.labelAnimationValue, nextState.showLabel ? 1 : 0)
        }
    }

    _onChangeText(text) {
        this._edtText = text
        this._oriOnChangeText && this._oriOnChangeText(text)
    }

    render() {
        let { isFocused } = this.state
        let { hintColor, focusColor } = this.props
        let color = isFocused ? focusColor : hintColor
        return <View style={[styles.textStyle]}>
            <Animated.Text style={[this._labelStyle, { color: color }]}>
                {this.props.labelText || this._oriEdtChild.props.placeholder}
            </Animated.Text>
            {this._edtChild}
        </View>
    }
}

const styles = StyleSheet.create({
    textStyle: {
        flex: 1,
        justifyContent: 'center'
    },
    editChild: {
        backgroundColor: 'transparent',
        textAlign: 'left',
        padding: 0
    }
})