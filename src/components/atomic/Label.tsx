import React from 'react'
import {
    Text,
    StyleSheet,
    FlexStyle,
    GestureResponderEvent,
    TextStyle
} from 'react-native'
import { Colors } from '../../utilities'
import {
    FONT,
    FONTSIZE,
    FONTALIGNMENT,
} from '../../utilities/modules/Fonts'
import { hasKey } from '../../utilities/modules/Utils'

type fontType = keyof typeof FONT
type fontSize = keyof typeof FONTSIZE
type fontColor = keyof typeof Colors
type textAlignment = keyof typeof FONTALIGNMENT

interface LabelStyle extends FlexStyle {
    textAlign?: textAlignment
    lineHeight?: number
}

interface ILabelProps {
    children: string
    numberOfLines?: number
    style?: LabelStyle | TextStyle
    fontSize?: fontSize
    color?: fontColor
    fontFamily?: fontType
    darkColor?: fontColor
    textAlign?: textAlignment
    onPress?: (event: GestureResponderEvent) => void
}

export default function Label(props: ILabelProps) {
    var textStyle = {
        ...props.style,
        fontFamily: FONT.regular,
        color: Colors.WHITE,
        fontSize: FONTSIZE.regular,
        textAlign: FONTALIGNMENT.auto,
        lineHeight: FONTSIZE.regular * 1.25,
    }
    if (props.fontFamily && hasKey(FONT, props.fontFamily))
        textStyle['fontFamily'] = FONT[props.fontFamily]
    if (props.color && hasKey(Colors, props.color))
        textStyle['color'] = Colors[props.color]
    if (props.fontSize && hasKey(FONTSIZE, props.fontSize)) {
        const size = FONTSIZE[props.fontSize]
        textStyle['fontSize'] = size
        textStyle['lineHeight'] = size * 1.25
    }
    if (props.style?.textAlign && hasKey(FONTALIGNMENT, props.style.textAlign))
        textStyle['textAlign'] = FONTALIGNMENT[props.style.textAlign]
    if (props.style?.lineHeight)
        textStyle['lineHeight'] = props.style.lineHeight
    if (props.textAlign)
        textStyle['textAlign'] = FONTALIGNMENT[props.textAlign]

    return <Text style={[styles.defaultStyle, textStyle]}
        numberOfLines={props.numberOfLines}
        onPress={props.onPress}
        maxFontSizeMultiplier={1.5}>
        {props.children}
    </Text>
}

Label.defaultProps = {
    numberOfLines: 1
}

const styles = StyleSheet.create({
    defaultStyle: {
        backgroundColor: 'transparent',
        letterSpacing: 0
    }
})