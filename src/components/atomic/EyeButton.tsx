import React from 'react'
import { TouchableOpacity } from 'react-native'
import {
    EyeVisible,
    EyeHidden
} from '../../assets/svg_icons'

interface IEyeButtonProps {
    isSecure: boolean
    onPress: () => void
}

export default function EyeButton(props: IEyeButtonProps) {
    return <TouchableOpacity onPress={props.onPress}>
        {props.isSecure ? <EyeHidden /> : <EyeVisible />}
    </TouchableOpacity>
}