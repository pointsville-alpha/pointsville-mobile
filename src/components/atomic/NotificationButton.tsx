import { useNavigation } from '@react-navigation/native'
import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Bell } from '../../assets/svg_icons'
import {
    Colors,
    normalize,
    NOTIFICATION_NAVIGATION_KEY
} from '../../utilities'

export default function NotificationButton() {
    const navigation = useNavigation()
    return <TouchableOpacity
        onPress={() => navigation.navigate(NOTIFICATION_NAVIGATION_KEY)}>
        <Bell />
        <View style={styles.spot} />
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    spot: {
        position: 'absolute',
        height: normalize(10),
        width: normalize(10),
        borderRadius: normalize(5),
        backgroundColor: Colors.BRIGHT_RED,
        borderWidth: normalize(2),
        borderColor: Colors.BACKGROUND,
        end: 0
    }
})