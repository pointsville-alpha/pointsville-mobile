import React from 'react'
import { FlexStyle } from 'react-native'
import {
    LogoLarge,
    LogoSmall
} from '../../assets/svg_icons'

enum LogoSizeVariants {
    small = 'small',
    large = 'large'
}

type ILogoProps = {
    style?: FlexStyle
    size?: keyof typeof LogoSizeVariants
}

export default function Logo(props: ILogoProps) {
    switch (props.size) {
        case LogoSizeVariants.small:
            return <LogoSmall style={props.style} />
        case LogoSizeVariants.large:
            return <LogoLarge style={props.style} />
        default:
            return <LogoSmall style={props.style} />
    }
}