import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native';
import { Colors, normalize, RESEND_CODE } from '../../utilities';
import Label from './Label';

export default class CountDown extends Component {
    constructor(props) {
        super(props);
        this.state = { time: {}, seconds: this.props.timeout };
        this.timer = 0;
        this.startTimer = this.startTimer.bind(this);
        this.countDown = this.countDown.bind(this);
    }
    
    secondsToTime(secs) {
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);

        let obj = {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
        return obj;
    }

    componentDidMount() {
        this.initTimer(this.state.seconds)
        
    }

    initTimer(seconds){
        let timeLeftVar = this.secondsToTime(seconds);
        this.setState({ time: timeLeftVar },()=>{
            this.startTimer();
        });
    }

    startTimer() {
        if (this.timer == 0 && this.state.seconds > 0) {
            this.timer = setInterval(this.countDown, 1000);
        }
    }

    countDown() {
        // Remove one second, set state so a re-render happens.
        let seconds = this.state.seconds - 1;
        this.setState({
            time: this.secondsToTime(seconds),
            seconds: seconds,
        });

        // Check if we're at zero.
        if (seconds == 0) {
            this.props.onTimeout(true)
            clearInterval(this.timer);
        }
    }

    render() {
        let timeout = !(this.state.seconds >0);
        return (
            <View style={this.props.style}>
                <Label
                    style={timeout?styles.resendCode:styles.counter}
                    numberOfLines={1}
                    color={timeout ?'WHITE':'WHITE_ALPHA_50'}
                    children={timeout ? RESEND_CODE: `${this.state.time.m}:${this.state.time.s}`} 
                    onPress={()=>{
                        if(timeout){
                            this.props.onResendClick && this.props.onResendClick()
                            this.setState({ 
                                time: {}, 
                                seconds: this.props.timeout },()=>{
                                    this.timer = 0
                                    this.initTimer(this.state.seconds)
                                })
                            
                        }
                    }}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    counter: {
        alignSelf:'center',
    },
    resendCode: {
        alignSelf:'center',
        textDecorationLine: 'underline',
    },
})
