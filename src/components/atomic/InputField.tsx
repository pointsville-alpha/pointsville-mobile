import React from 'react'
import {
    View,
    TextInput,
    FlexStyle,
    StyleSheet
} from 'react-native'
import Label from './Label'
import {
    InputStyle,
    Colors,
    normalize
} from '../../utilities'

interface ITextInputProps {
    placeholder: string
    value: string
    autoFocus?: boolean
    containerStyle?: FlexStyle
    onChangeText?: (email: string) => void
    returnKeyType?: any
    onSubmitEditing?: () => void
    enablesReturnKeyAutomatically?: boolean
    onFocus?: any
    warning?: string
}

export default function InputField(props: ITextInputProps) {
    return <View style={[styles.container, props.containerStyle]}>
        <TextInput
            numberOfLines={1}
            onFocus={props.onFocus}
            onSubmitEditing={props.onSubmitEditing}
            returnKeyType={props.returnKeyType}
            enablesReturnKeyAutomatically={props.enablesReturnKeyAutomatically}
            style={[InputStyle.field, styles.field]}
            value={props.value}
            onChangeText={props.onChangeText}
            placeholder={props.placeholder}
            placeholderTextColor={Colors.WHITE_ALPHA_50}
            autoCorrect={false}
            autoFocus={props.autoFocus}
            autoCapitalize='none'
            underlineColorAndroid='transparent' />
        <View style={[styles.line, props.warning ? styles.errorLine : null]} />
        <Label children={props.warning || ''}
            color='BRIGHT_RED' />
    </View>
}

const styles = StyleSheet.create({
    container: {
        minHeight: normalize(78)
    },
    field: {
        height: normalize(58),
        color: Colors.WHITE,
        fontSize: normalize(16)
    },
    line: {
        height: 1,
        backgroundColor: Colors.WHITE_ALPHA_5
    },
    errorLine: {
        height: 1,
        backgroundColor: Colors.BRIGHT_RED
    }
})