import React, { useRef } from 'react';
import { View,StyleSheet } from 'react-native';
import OTPTextInput from 'react-native-otp-textinput'
import { Colors, normalize } from '../../utilities';
const OTPInputField = (props:any) => {
    var otpInput = useRef(null);

    const clearText = () => {
        otpInput.current.clear();
    }

    const setText = () => {
        otpInput.current.setValue(props.value);
    }

    return (
        <View style={props.style}>
            <OTPTextInput 
            ref={e => (otpInput = e)} 
            inputCount={6}
            handleTextChange={props.onValueChange}
            tintColor={Colors.WHITE}
            offTintColor={Colors.WHITE_ALPHA_50}
            textInputStyle={styles.roundedTextInput}
            containerStyle={styles.textInputContainer}
            underlineColorAndroid='transparent'
            />
        </View>
    );
}

export default OTPInputField;

const styles = StyleSheet.create({
    roundedTextInput: {
        borderRadius: normalize(4),
        borderWidth: normalize(1),
        color:Colors.WHITE,
        borderBottomWidth:normalize(1)
      },
      textInputContainer: {
        marginBottom: 20,
      },
})