import React from 'react'
import {
    FlexStyle,
    StyleSheet,
    View
} from 'react-native'
import {
    Colors,
    normalize
} from '../../utilities'

interface ILighterBackgroundProps {
    style?: FlexStyle
    header?: boolean
}

export default function LighterBackground(props: ILighterBackgroundProps) {
    return <View style={props.style}>
        <View style={[styles.topHalf, props.header && styles.header]} />
        <View style={styles.mainContainer} >
            <View style={styles.circle} />
            <View style={styles.middle} />
            <View style={styles.triangle} />
        </View>
    </View>
}

const styles = StyleSheet.create({
    topHalf: {
        width: normalize(700),
        height: normalize(100),
        backgroundColor: Colors.WHITE_ALPHA_5
    },
    header: {
        backgroundColor: 'transparent'
    },
    mainContainer: {
        flexDirection: 'row'
    },
    circle: {
        height: normalize(170),
        width: normalize(170),
        borderBottomStartRadius: normalize(85),
        borderBottomEndRadius: normalize(85),
        backgroundColor: Colors.WHITE_ALPHA_5,
        start: -normalize(20)
    },
    middle: {
        backgroundColor: Colors.BACKGROUND,
        height: normalize(170),
        width: normalize(100),
        position: 'absolute',
        start: normalize(85)
    },
    triangle: {
        position: 'absolute',
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderRightWidth: normalize(650),
        borderTopWidth: normalize(168),
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderTopColor: Colors.WHITE_ALPHA_5,
        start: normalize(85),
    }
})