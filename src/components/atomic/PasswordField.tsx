import React, { useState } from 'react'
import {
    View,
    TextInput,
    FlexStyle,
    StyleSheet
} from 'react-native'
import Label from './Label'
import {
    InputStyle,
    Colors,
    normalize
} from '../../utilities'
import EyeButton from './EyeButton'

export interface ITextInputProps {
    placeholder: string
    value: string
    autoFocus?: boolean
    containerStyle?: FlexStyle
    onChangeText?: (email: string) => void
    returnKeyType?: any
    onSubmitEditing?: () => void
    enablesReturnKeyAutomatically?: boolean
    onFocus?: any
    warning?: string
}

export default function PasswordField(props: ITextInputProps) {
    const [secure, toggle] = useState(true)
    return <View style={props.containerStyle}>
        <View style={styles.container}>
            <TextInput
                autoFocus={props.autoFocus}
                numberOfLines={1}
                onFocus={props.onFocus}
                returnKeyType={props.returnKeyType}
                enablesReturnKeyAutomatically={props.enablesReturnKeyAutomatically}
                style={[InputStyle.field, styles.field]}
                value={props.value}
                onChangeText={props.onChangeText}
                secureTextEntry={secure}
                placeholder={props.placeholder}
                placeholderTextColor={Colors.WHITE_ALPHA_50}
                autoCorrect={false}
                underlineColorAndroid={'transparent'} />
            <EyeButton isSecure={secure}
                onPress={() => toggle(!secure)} />
        </View>
        <View style={[props.warning ? styles.errorLine : styles.line]} />
        <Label children={props.warning || ''}
            color='BRIGHT_RED' />
    </View>
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    field: {
        height: normalize(58),
        color: Colors.WHITE,
        fontSize: normalize(16)
    },
    line: {
        height: 1,
        backgroundColor: Colors.WHITE_ALPHA_5
    },
    errorLine: {
        height: 1,
        backgroundColor: Colors.BRIGHT_RED
    }
})