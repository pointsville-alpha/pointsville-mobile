import React from 'react'
import {
    FlexStyle,
    StyleSheet,
    TouchableOpacity,
    View
} from 'react-native'
import {
    Colors,
    normalize
} from '../../utilities'
import Gradient from './Gradient'
import Label from './Label'

interface IButtonProps {
    title: string
    onPress: () => void
    style?: FlexStyle
    disabled?: boolean
}

export default function Button(props: IButtonProps) {
    return <TouchableOpacity activeOpacity={props.disabled ? 1 : 0.2}
        onPress={() => !props.disabled && props.onPress()}>
        {props.disabled ?
            <View style={[styles.default, styles.disabled, props.style]}>
                <Label children={props.title}
                    fontFamily='semiBold'
                    color='WHITE_ALPHA_20'
                    fontSize='extra_medium' />
            </View>
            : <Gradient style={{ ...styles.default, ...props.style }}>
                <Label children={props.title}
                    fontFamily='semiBold'
                    fontSize='extra_medium' />
            </Gradient>}
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    default: {
        marginBottom: normalize(16),
        height: normalize(54),
        width: '100%'
    },
    disabled: {
        backgroundColor: Colors.WHITE_ALPHA_40,
        borderRadius: normalize(6),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    }
})