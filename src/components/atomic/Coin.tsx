import React from "react"
import { FlexStyle } from "react-native"
import { CoinIcon } from "../../assets/svg_icons"
import { normalize } from "../../utilities"

interface ICoinProps {
    style?: FlexStyle
    size?: number
}

export default function Coin(props: ICoinProps) {
    return <CoinIcon style={props.style}
        height={props.size || normalize(12)}
        width={props.size || normalize(12)} />
}