import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { BackArrow } from '../../assets/svg_icons'
import { FlexStyle } from 'react-native'

interface IBackButtonProps {
    style?: FlexStyle
}

export default function BackButton(props: IBackButtonProps) {
    const navigation = useNavigation()
    return <TouchableOpacity onPress={() => navigation.goBack()}
        children={<BackArrow style={props.style} />} />
}