import React from 'react'
import {
    FlexStyle,
    View,
    StyleSheet,
    Platform,
    ShadowStyleIOS
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {
    Colors,
    normalize
} from '../../utilities'

interface IGradientProps {
    style?: FlexStyle
    children?: any
    shadowStyle?: ShadowStyleIOS
}

export default function Gradient(props: IGradientProps) {
    return <View style={[styles.shadow, props.shadowStyle]}>
        <LinearGradient start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            angle={135}
            colors={[Colors.GRADIENT_START, Colors.GRADIENT_END]}
            children={props.children}
            style={[styles.view, props.style]} />
    </View>
}

const styles = StyleSheet.create({
    view: {
        borderRadius: normalize(6),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 13,
        backgroundColor: Platform.OS === 'ios' ? 'transparent' : Colors.GRADIENT_START
    },
    shadow: {
        shadowColor: Colors.GRADIENT_START,
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 13
    }
})