import React from 'react'
import {
    View,
    StyleSheet,
    FlexStyle
} from 'react-native'
import { Colors } from '../../utilities'

interface IScreenContainerProps {
    children: any
    style?: FlexStyle
}

export default function ScreenContainer(props: IScreenContainerProps) {
    return <View style={[styles.container, props.style]}
        children={props.children} />
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.BACKGROUND,
        width:'100%',
    }
})