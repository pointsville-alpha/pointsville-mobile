import { useNavigation } from '@react-navigation/native'
import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { HeaderClose } from '../../assets/svg_icons'

export default function CloseButton() {
    const navigation: any = useNavigation()
    return <TouchableOpacity onPress={() => navigation.pop()}
        children={<HeaderClose />} />
}