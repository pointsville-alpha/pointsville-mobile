import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Home from '../screens/home'
import Wallet from '../screens/wallet'
import Hunt from '../screens/hunt'
import Team from '../screens/team'
import Account from '../screens/account'
import {
    HOME_NAVIGATION_KEY,
    WALLET_NAVIGATION_KEY,
    HUNT_NAVIGAION_KEY,
    Colors,
    ACCOUNT_NAVIGATION_KEY,
    TEAM_NAVIGATION_KEY,
    normalizeVertical
} from '../utilities'
import { createStackNavigator } from '@react-navigation/stack'
import SettingsButton from '../components/atomic/SettingsButton'
import {
    HomeIcon,
    WalletIcon,
    HuntIcon,
    TeamIcon,
    AccountIcon
} from '../assets/svg_icons'
import { defaultHeaderOptions } from '../utilities'
import { NotificationButton } from '../components'

const Tab = createBottomTabNavigator()
const Stack = createStackNavigator()

const headerOptions = {
    ...defaultHeaderOptions,
    headerLeft: () => <SettingsButton />,
    headerRight: () => <NotificationButton />
}

const tabBarOptions = {
    style: {
        backgroundColor: Colors.BACKGROUND_DEEP,
        borderTopWidth: 0,
    },
    activeTintColor: Colors.WHITE
}

const tabNavigatorScreenOptions = ({ route }: any) =>
    ({
        tabBarIcon: ({ focused }: any) => TabIcon(route.name, focused)
    })

const TabItem = (name: string, component: any) =>
    <Stack.Navigator>
        <Stack.Screen name={name}
            options={headerOptions}
            component={component} />
    </Stack.Navigator>

const HomeTab = () => TabItem(HOME_NAVIGATION_KEY, Home)
const WalletTab = () => TabItem(WALLET_NAVIGATION_KEY, Wallet)
const HuntTab = () => TabItem(HUNT_NAVIGAION_KEY, Hunt)
const TeamTab = () => TabItem(TEAM_NAVIGATION_KEY, Team)
const AccountTab = () => TabItem(ACCOUNT_NAVIGATION_KEY, Account)

const TabIcon = (name: string, focused: boolean) => {
    let fillOpacity = focused ? 1 : 0.4
    switch (name) {
        case HOME_NAVIGATION_KEY:
            return <HomeIcon fillOpacity={fillOpacity} />
        case WALLET_NAVIGATION_KEY:
            return <WalletIcon fillOpacity={fillOpacity} />
        case HUNT_NAVIGAION_KEY:
            return <HuntIcon fillOpacity={fillOpacity} />
        case TEAM_NAVIGATION_KEY:
            return <TeamIcon fillOpacity={fillOpacity} />
        case ACCOUNT_NAVIGATION_KEY:
            return <AccountIcon fillOpacity={fillOpacity} />
    }
}

export default function TabNavigation() {
    return <Tab.Navigator tabBarOptions={tabBarOptions}
        screenOptions={tabNavigatorScreenOptions}>
        <Tab.Screen name={HOME_NAVIGATION_KEY}
            component={HomeTab} />
        <Tab.Screen name={WALLET_NAVIGATION_KEY}
            component={WalletTab} />
        <Tab.Screen name={HUNT_NAVIGAION_KEY}
            component={HuntTab} />
        <Tab.Screen name={TEAM_NAVIGATION_KEY}
            component={TeamTab} />
        <Tab.Screen name={ACCOUNT_NAVIGATION_KEY}
            component={AccountTab} />
    </Tab.Navigator>
}