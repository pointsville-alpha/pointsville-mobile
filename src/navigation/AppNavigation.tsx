import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import {
    CardStyleInterpolators,
    createStackNavigator
} from '@react-navigation/stack'
import SafeAreaView from 'react-native-safe-area-view';
import {
    SIGN_IN_NAVIGATION_KEY,
    FORGOT_PASSWORD_NAVIGATION_KEY,
    FORGOT_PASSWORD_CHECK_EMAIL_NAVIGATION_KEY,
    FORGOT_PASSWORD_RESET_NAVIGATION_KEY,
    SIGN_UP_NAVIGATION_KEY,
    MAIN_NAVIGATION_KEY,
    INSTAGRAM_NAVIGATION_KEY,
    LINKEDIN_NAVIGATION_KEY,
    SETTINGS_NAVIGATION_KEY,
    TERMS_OF_USE_NAVIGATION_KEY,
    PRIVACY_POLICY_NAVIGATION_KEY,
    GIVE_US_FEEDBACK_NAVIGATION_KEY,
    NOTIFICATION_NAVIGATION_KEY,
    QRSCAN_NAVIGATION_KEY,
    TRANSFER_POINTS_NAVIGATION_KEY,
    REDEEM_POINTS_NAVIGATION_KEY,
    PROMO_NAVIGATION_KEY,
    EDIT_PROFILE_NAVIGATION_KEY,
    Colors,
    WIKITUDE_NAVIGATION_KEY,
    WIKITUDE_MENU_NAVIGATION_KEY,
    NEWS_DETAIL_NAVIGATION_KEY
} from '../utilities'
import SignIn from '../screens/sign_in'
import ForgotPassword from '../screens/forgot_password/ForgotPassword'
import CheckEmail from '../screens/forgot_password/CheckEmail'
import ResetPassword from '../screens/forgot_password/ResetPassword'
import SignUp from '../screens/sign_up'
import TabNavigation from './TabNavigation'
import InstagramSignIn from '../screens/sign_in/social/Instagram'
import LinkedinSignIn from '../screens/sign_in/social/LinkedIn'
import Settings from '../screens/settings'
import PrivacyPolicy from '../screens/privacy_policy'
import TermsOfUse from '../screens/terms_of_use'
import GiveUsFeedback from '../screens/give_us_feedback'
import { defaultHeaderOptions } from '../utilities'
import Notification from '../screens/notification'
import SplashScreen from 'react-native-splash-screen'
import QRScan from '../screens/qr_scan'
import TransferPoints from '../screens/transfer_points'
import RedeemPoints from '../screens/redeem_points'
import Promo from '../screens/promo'
import EditProfile from '../screens/edit_profile'
import WikitudeHome from '../screens/hunt/components/WikitudeHome'
import WikitudeComponent from '../screens/hunt/components/WikitudeComponent'
import NewsDetail from '../screens/home/components/news/NewsDetail'

const Stack = createStackNavigator()

const hideHeader = {
    headerShown: false
}

const HIDE_HEADER = true

const navigationTheme = {
    colors: {
        background: Colors.BACKGROUND,
        text: Colors.WHITE
    }
}

const navigatorScreenOptions = {
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
}

const ScreenItem = (key: string, component: any, noHeader = false) =>
    <Stack.Screen name={key}
        options={noHeader ? hideHeader : defaultHeaderOptions}
        component={component} />

export default function AppNavigation() {
	SplashScreen.hide()
	return (<NavigationContainer theme={navigationTheme}>
        <Stack.Navigator screenOptions={navigatorScreenOptions}
            initialRouteName={MAIN_NAVIGATION_KEY}>
            {ScreenItem(SIGN_IN_NAVIGATION_KEY, SignIn, HIDE_HEADER)}
            {ScreenItem(FORGOT_PASSWORD_NAVIGATION_KEY, ForgotPassword)}
            {ScreenItem(FORGOT_PASSWORD_CHECK_EMAIL_NAVIGATION_KEY, CheckEmail)}
            {ScreenItem(FORGOT_PASSWORD_RESET_NAVIGATION_KEY, ResetPassword)}
            {ScreenItem(SIGN_UP_NAVIGATION_KEY, SignUp, HIDE_HEADER)}
            {ScreenItem(INSTAGRAM_NAVIGATION_KEY, InstagramSignIn, HIDE_HEADER)}
            {ScreenItem(LINKEDIN_NAVIGATION_KEY, LinkedinSignIn, HIDE_HEADER)}
            {ScreenItem(MAIN_NAVIGATION_KEY, TabNavigation, HIDE_HEADER)}
            {ScreenItem(SETTINGS_NAVIGATION_KEY, Settings)}
            {ScreenItem(TERMS_OF_USE_NAVIGATION_KEY, TermsOfUse)}
            {ScreenItem(PRIVACY_POLICY_NAVIGATION_KEY, PrivacyPolicy)}
            {ScreenItem(GIVE_US_FEEDBACK_NAVIGATION_KEY, GiveUsFeedback)}
            {ScreenItem(NOTIFICATION_NAVIGATION_KEY, Notification)}
            {ScreenItem(QRSCAN_NAVIGATION_KEY, QRScan)}
            {ScreenItem(TRANSFER_POINTS_NAVIGATION_KEY, TransferPoints, HIDE_HEADER)}
            {ScreenItem(REDEEM_POINTS_NAVIGATION_KEY, RedeemPoints, HIDE_HEADER)}
            {ScreenItem(PROMO_NAVIGATION_KEY, Promo, HIDE_HEADER)}
            {ScreenItem(EDIT_PROFILE_NAVIGATION_KEY, EditProfile)}
            {ScreenItem(WIKITUDE_MENU_NAVIGATION_KEY, WikitudeHome)}
            {ScreenItem(WIKITUDE_NAVIGATION_KEY, WikitudeComponent, HIDE_HEADER)}
            {ScreenItem(NEWS_DETAIL_NAVIGATION_KEY, NewsDetail)}
        </Stack.Navigator>
    </NavigationContainer>)
}