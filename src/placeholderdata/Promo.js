import {
    MCD_KEY,
    MLB_KEY,
    NIKE_KEY
} from "../assets/ImageSelect"

export const NEW_PROMOS = [
    {
        image: MCD_KEY,
        title: 'McDonalds',
        date: 'Aug 20, 2020',
        validity: '*Valid till 4 September 2020',
        code: 'SALE250'
    },
    {
        image: MCD_KEY,
        title: 'McDonalds',
        date: 'Aug 20, 2020',
        validity: '*Valid till 4 September 2020',
        code: 'MCDOFF25'
    }
]

export const AVAILED_PROMOS = [
    {
        image: MLB_KEY,
        title: 'MLBshop.com',
        date: 'Aug 20, 2020',
        points: '50'
    },
    {
        image: NIKE_KEY,
        title: 'Nike',
        date: 'Aug 20, 2020',
        points: '100'
    }
]