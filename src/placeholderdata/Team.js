import {
    ARIZONA_KEY,
    BROOKLYN_KEY,
    PITTSBURGH_KEY,
    TAMPA_BAY_KEY,
    TEXAS_KEY
} from "../assets/ImageSelect";

export const TEAM_SCREEN_TOKENS = [
    {
        team: 'Pirates',
        price: '$1.50',
        image: PITTSBURGH_KEY
    },
    {
        team: 'Brooklyn',
        price: '$1.30',
        image: BROOKLYN_KEY
    },
    {
        team: 'Cardinals',
        price: '$2.50',
        image: ARIZONA_KEY
    },
    {
        team: 'Rays',
        price: '$1.10',
        image: TAMPA_BAY_KEY
    },
    {
        team: 'Texas Rangers',
        price: '$1.60',
        image: TEXAS_KEY
    },
    {
        team: 'Brooklyn',
        price: '$1.30',
        image: BROOKLYN_KEY
    },
    {
        team: 'Texas Rangers',
        price: '$1.60',
        image: TEXAS_KEY
    },
    {
        team: 'Brooklyn',
        price: '$1.30',
        image: BROOKLYN_KEY
    },
    {
        team: 'Cardinals',
        price: '$2.50',
        image: ARIZONA_KEY
    },
    {
        team: 'Rays',
        price: '$1.10',
        image: TAMPA_BAY_KEY
    },
    {
        team: 'Texas Rangers',
        price: '$1.60',
        image: TEXAS_KEY
    },
    {
        team: 'Brooklyn',
        price: '$1.30',
        image: BROOKLYN_KEY
    },
    {
        team: 'Pirates',
        price: '$1.50',
        image: PITTSBURGH_KEY
    },
    {
        team: 'Brooklyn',
        price: '$1.30',
        image: BROOKLYN_KEY
    },
    {
        team: 'Cardinals',
        price: '$2.50',
        image: ARIZONA_KEY
    },
]