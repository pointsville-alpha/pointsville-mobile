import { ARIZONA_KEY, BOSTAN_RED_SOX_KEY, BROOKLYN_KEY, CHICAGO_KEY, PITTSBURGH_KEY, SAMPLE_NEWS_1, SAMPLE_NEWS_2, SAN_FRANSICO_GIANTS_KEY, TAMPA_BAY_KEY, TORONTO_KEY, WASHINGTON_NATIONALS_KEY, NEW_YORK_YANKEES_KEY } from "../assets/ImageSelect"

export const HOME_TOKENS = [
    {
        name: 'Pirates',
        price: '$1.50',
        image: PITTSBURGH_KEY
    },
    {
        name: 'New York Yankees',
        price: '$1.50',
        image: NEW_YORK_YANKEES_KEY
    },
    {
        name: 'San Francisco Giants',
        price: '$2.50',
        image: SAN_FRANSICO_GIANTS_KEY
    },
    {
        name: 'Washington Nationals',
        price: '$1.30',
        image: WASHINGTON_NATIONALS_KEY
    },
    {
        name: 'Boston Red Sox',
        price: '$1.10',
        image: BOSTAN_RED_SOX_KEY
    },
    {
        name: 'San Fransico Giants',
        price: '$2.50',
        image: SAN_FRANSICO_GIANTS_KEY
    },
    {
        name: 'Boston Red Sox',
        price: '$1.30',
        image: BOSTAN_RED_SOX_KEY
    },
    {
        name: 'New York Yankees',
        price: '$1.10',
        image: NEW_YORK_YANKEES_KEY
    },
    {
        name: 'San Fransico Giants',
        price: '$2.50',
        image: SAN_FRANSICO_GIANTS_KEY
    },
    {
        name: 'Washington Nationals',
        price: '$1.30',
        image: WASHINGTON_NATIONALS_KEY
    },
    {
        name: 'Boston Red Sox',
        price: '$1.10',
        image: BOSTAN_RED_SOX_KEY
    }
]

export const TODAY_GAMES = [
    {
        home: {
            image: PITTSBURGH_KEY,
            name: 'Pirates',
            price: '$1.50'
        },
        away: {
            image: BOSTAN_RED_SOX_KEY,
            name: 'Boston Red Sox',
            price: '$1.30'
        },
        event: 'FINAL',
        date: '20 AUG',
        time: '9:00'
    },
    {
        home: {
            image: WASHINGTON_NATIONALS_KEY,
            name: 'Washington Nationals',
            price: '$1.30'
        },
        away: {
            image: SAN_FRANSICO_GIANTS_KEY,
            name: 'San Francisco Giants',
            price: '$2.50'
        },
        event: 'FINAL',
        date: '20 AUG',
        time: '9:00'
    }
]

export const UPCOMING_GAMES = []

export const NEWS = [
    {
        image: SAMPLE_NEWS_1,
        title: `What to know: Pirates 2020 offseason FAQ`,
        description: 'Ben Cherington did not dramatically change the roster he inherited during his first offseason as the Pirates’ general manager. He added a …'
    },
    {
        image: SAMPLE_NEWS_2,
        title: `Pirates sign 19-year-old righty from Taiwan`,
        description: 'On Sept. 20, the Pirates completed a pair of trades to acquire more than $1 million in additional international spending capacity. .…'
    }
]