export const icon1 = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Gold_coin_icon.png/481px-Gold_coin_icon.png";
export const icon2 = "https://purepng.com/public/uploads/large/purepng.com-gold-coinsflatcoinsroundmetalgold-1421526479302uo2qp.png";
export const LOCAL_POIS_DATA = {
    "team1": [
        { "id": "6", "longitude": "19.9129", "latitude": "19.8859", "description": "This is the description of POI#6", "name": "POI#6", "icon": icon1 },
        { "id": "7", "longitude": "20.0209", "latitude": "20.0689", "description": "This is the description of POI#7", "name": "POI#7", "icon": icon1 },
        { "id": "8", "longitude": "19.8809", "latitude": "19.9589", "description": "This is the description of POI#8", "name": "POI#8", "icon": icon1 },
        { "id": "9", "longitude": "19.8889", "latitude": "19.8819", "description": "This is the description of POI#9", "name": "POI#9", "icon": icon1 },
        { "id": "10", "longitude": "20.0529", "latitude": "19.9889", "description": "This is the description of POI#10", "name": "POI#10", "icon": icon1 }],
    // "team2": [{ "id": "1", "longitude": "20.0339", "latitude": "19.9849", "description": "This is the description of POI#1", "name": "POI#1", "icon": icon2 },
    // { "id": "2", "longitude": "20.0549", "latitude": "19.9929", "description": "This is the description of POI#2", "name": "POI#2", "icon": icon2 },
    // { "id": "3", "longitude": "19.9309", "latitude": "20.0569", "description": "This is the description of POI#3", "name": "POI#3", "icon": icon2 },
    // { "id": "4", "longitude": "19.9849", "latitude": "20.0259", "description": "This is the description of POI#4", "name": "POI#4", "icon": icon2 },
    // { "id": "5", "longitude": "19.9649", "latitude": "19.9209", "description": "This is the description of POI#5", "name": "POI#5", "icon": icon2 }]
};
/**
 export const LOCAL_POIS_COORDINATES = {
  "team1": [
      { "id": "6", "longitude": 19.933391, "latitude": 73.838480, "description": "This is the description of POI#6", "name": "POI#6", "icon": icon1 },
      { "id": "7", "longitude": 19.933315, "latitude": 73.838437, "description": "This is the description of POI#7", "name": "POI#7", "icon": icon1 },
      { "id": "8", "longitude": 19.933335, "latitude": 73.838668, "description": "This is the description of POI#8", "name": "POI#8", "icon": icon1 },
      { "id": "9", "longitude": 19.933204, "latitude": 73.838411, "description": "This is the description of POI#9", "name": "POI#9", "icon": icon1 },
      { "id": "10", "longitude": 19.933199, "latitude": 73.837970, "description": "This is the description of POI#10", "name": "POI#10", "icon": icon1 }],
  "team2": [{ "id": "1", "longitude": 19.929348, "latitude": 73.840940, "description": "This is the description of POI#1", "name": "POI#1", "icon": icon2 },
  { "id": "2", "longitude": 19.928998, "latitude": 73.841492, "description": "This is the description of POI#2", "name": "POI#2", "icon": icon2 },
  { "id": "3", "longitude": 19.929290, "latitude": 73.842050, "description": "This is the description of POI#3", "name": "POI#3", "icon": icon2 },
  { "id": "4", "longitude": 19.930309, "latitude": 73.842426, "description": "This is the description of POI#4", "name": "POI#4", "icon": icon2 },
  { "id": "5", "longitude": 19.929895, "latitude": 73.842555, "description": "This is the description of POI#5", "name": "POI#5", "icon": icon2 }]
};
 */
export const LOCAL_POIS_COORDINATES = {
  "team1": [
      { "id": "6", "latitude": 40.712529296874735, "longitude": -74.00541918352246, "description": "This is the description of POI#6", "name": "POI#6", "icon": icon1 },
      { "id": "7", "latitude": 40.71195977448578, "longitude": -74.00526160374284, "description": "This is the description of POI#7", "name": "POI#7", "icon": icon1 },
      { "id": "8", "latitude": 40.712415367992136, "longitude": -74.0052548982203, "description": "This is the description of POI#8", "name": "POI#8", "icon": icon1 },
      { "id": "9", "latitude": 40.71229276045472, "longitude": -74.0059801004827, "description": "This is the description of POI#9", "name": "POI#9", "icon": icon1 },
      { "id": "10", "latitude": 40.711409372893314, "longitude": -74.00603575631976, "description": "This is the description of POI#10", "name": "POI#10", "icon": icon1 }],
  "team2": [{ "id": "1", "latitude": 40.70862113579936, "longitude": -74.00726152583957, "description": "This is the description of POI#1", "name": "POI#1", "icon": icon2 },
  { "id": "2", "latitude": 40.70799388708673, "longitude": -74.00712272152305, "description": "This is the description of POI#2", "name": "POI#2", "icon": icon2 },
  { "id": "3", "latitude": 40.70770084706932, "longitude": -74.00752605870366, "description": "This is the description of POI#3", "name": "POI#3", "icon": icon2 },
  { "id": "4", "latitude": 40.70768610608035, "longitude": -74.00845779106021, "description": "This is the description of POI#4", "name": "POI#4", "icon": icon2 },
  { "id": "5", "latitude": 40.708890789933236, "longitude": -74.00901300832629, "description": "This is the description of POI#5", "name": "POI#5", "icon": icon2 }]
};
export const BOTTOM_BANNER_TITLE = "Collect 10 Pirates points"
export const BOTTOM_POINT_PRICE = "$1.50"
export const BOTTOM_ADD_TO_WALLET= "Add $75 to your wallet"

export const EXIT_POPUP_TITLE1 = "Yeh! you have earned"
export const EXIT_POPUP_TITLE2 = "Ponits"
export const EXIT_POPUP_TITLE3 = "in this hunt"
export const EXIT_POPUP_DESC = "Would you like to continue\nhunt for more points?"
export const EXIT_POPUP_BUTTON1 = "Hunt Again!"
export const EXIT_POPUP_BUTTON2 = "No, I am done!"

export const MAP_BOTTOM_BANNER_TITLE = "100 points available to grab"
export const MAP_BOTTOM_POINT_TYPE = "Carson Toyota"
export const MAP_BOTTOM_POINT_ADDRESS= "1333 E 223rd Carson, CA 90745, Unites States"

export const MAP_AUTOCOMPLETE_SAMPLE_DATA = [
  "New York","Los Angeles","San Diego","San Antonio","San Francisco","Las Vegas"]

export const MAP_STYLE = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#242f3e"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#746855"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#242f3e"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "poi",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#263c3f"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#6b9a76"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#38414e"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#212a37"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9ca5b3"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#746855"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#1f2835"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#f3d19c"
        }
      ]
    },
    {
      "featureType": "transit",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#2f3948"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#d59563"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#17263c"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#515c6d"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#17263c"
        }
      ]
    }
  ]
