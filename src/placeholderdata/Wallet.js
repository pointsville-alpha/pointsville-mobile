import {
    ARIZONA_KEY,
    BOSTAN_RED_SOX_KEY,
    BROOKLYN_KEY,
    CHICAGO_KEY,
    PITTSBURGH_KEY,
    SAN_FRANSICO_GIANTS_KEY,
    TEXAS_KEY,
    TORONTO_KEY,
    WASHINGTON_NATIONALS_KEY,
    NEW_YORK_YANKEES_KEY
} from "../assets/ImageSelect"

export const CARD = {
    balance: 3089,
    amount: '$6,908.30',
    hunt: 1589,
    promo: 1500,
    received: 1500
}

export const MY_POINTS = [
    {
        image: PITTSBURGH_KEY,
        team: 'Pittsburgh Pirates',
        price: '$1.50',
        points: 201,
        total: '$ 301.50'
    },
    {
        image: BOSTAN_RED_SOX_KEY,
        team: 'Boston Red Sox',
        price: '$1.30',
        points: 330,
        total: '$ 429.00'
    },
    {
        image: SAN_FRANSICO_GIANTS_KEY,
        team: 'San Francisco Giants',
        price: '$1.75',
        points: 201,
        total: '$ 502.50'
    },
    {
        image: NEW_YORK_YANKEES_KEY,
        team: 'New York Yankees',
        price: '$2.50',
        points: 201,
        total: '$ 301.50'
    },
    {
        image: CHICAGO_KEY,
        team: 'Chicago',
        price: '$1.20',
        points: 330,
        total: '$ 396.00'
    },
    {
        image: WASHINGTON_NATIONALS_KEY,
        team: 'Washington National',
        price: '$1.75',
        points: 201,
        total: '$ 351.75'
    },
]

export const TRANSACTIONS = [
    {
        image: NEW_YORK_YANKEES_KEY,
        team: 'New York Yankees',
        date: 'Aug 20, 2020 - 11:30',
        points: '+20',
        code: 'BY3838284'
    },
    {
        image: BOSTAN_RED_SOX_KEY,
        team: 'Boston Red Sox',
        date: 'Aug 20, 2020 - 9:00',
        points: '+35',
        code: 'BY4678343'
    },
    {
        image: WASHINGTON_NATIONALS_KEY,
        team: 'San Francisco Giants',
        date: 'Aug 19, 2020 - 11:30',
        points: '-15',
        code: 'SL3832354'
    },
    {
        image: SAN_FRANSICO_GIANTS_KEY,
        team: 'San Francisco Giants',
        date: 'Aug 19, 2020 - 9:00',
        points: '+100',
        code: 'BY9874532'
    }
]