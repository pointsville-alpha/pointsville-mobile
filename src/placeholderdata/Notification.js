import { MCD_KEY, MLB_KEY, NIKE_KEY } from "../assets/ImageSelect"

export const NOTIFICATIONS = [
    {
        image: MCD_KEY,
        title: `McDonald's promo code - Avail to add points to your wallet`,
        highlight: 'Get 55 Sign Up',
        date: '2mins ago'
    },
    {
        image: MCD_KEY,
        title: `McDonald's promo code - Avail to add points to your wallet`,
        highlight: 'MCDPIR250',
        date: '2hrs ago'
    },
    {
        image: MLB_KEY,
        title: `MBLshop's promo code - Watch HomeRun video by Pirates player`,
        highlight: 'Get 20 Points',
        date: '21 Jul, 2020'
    },
    {
        image: MLB_KEY,
        title: `MBLshop's promo code - Invite friends to fans club`,
        highlight: 'Get 10 Points',
        date: '21 Jul, 2020'
    },
    {
        image: NIKE_KEY,
        title: `Nike's promo code - Watch highlights: CLUBS Vs PIRATES`,
        highlight: 'Get 55 Points',
        date: '21 Jul, 2020'
    },
    {
        image: NIKE_KEY,
        title: `Nike's promo code - Watch HomeRun video by Pirates player name`,
        highlight: 'Get 20 Points',
        date: '20 Jul, 2020'
    },
    {
        image: NIKE_KEY,
        title: `Nike's promo code - Invite friends to fans club`,
        highlight: 'Get 10 Points',
        date: '12 Jul, 2020'
    }
]