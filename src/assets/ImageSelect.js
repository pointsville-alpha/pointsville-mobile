import React from 'react'

export const imageSelect = (key, size) => {
    switch (key) {
        case PITTSBURGH_KEY:
            return <Pittsburgh height={size} width={size} />
        case ARIZONA_KEY:
            return <Arizona height={size} width={size} />
        case BROOKLYN_KEY:
            return <Brooklyn height={size} width={size} />
        case CHICAGO_KEY:
            return <Chicago height={size} width={size} />
        case TORONTO_KEY:
            return <Toronto height={size} width={size} />
        case TEXAS_KEY:
            return <Texas height={size} width={size} />
        case TAMPA_BAY_KEY:
            return <TampaBay height={size} width={size} />
        case MCD_KEY:
            return <MCD height={size} width={size} />
        case MLB_KEY:
            return <MLBShop height={size} width={size} />
        case NIKE_KEY:
            return <Nike height={size} width={size} />
        case NEW_YORK_YANKEES_KEY:
            return <NewYorkYankees height={size} width={size} />
        case SAN_FRANSICO_GIANTS_KEY:
            return <SanFransicoGiants height={size} width={size} />
        case WASHINGTON_NATIONALS_KEY:
            return <WashingtonNationals height={size} width={size} />
        case BOSTAN_RED_SOX_KEY:
            return <BostanRedFox height={size} width={size} />
    }
}

export const PITTSBURGH_KEY = 'Pittsburgh'
export const ARIZONA_KEY = 'Cardinals'
export const BROOKLYN_KEY = 'Brooklyn'
export const CHICAGO_KEY = 'Chicago'
export const TORONTO_KEY = 'Toronto'
export const TEXAS_KEY = 'Texas'
export const TAMPA_BAY_KEY = 'Tampa Bay'
export const MCD_KEY = 'MCD'
export const MLB_KEY = 'MLBShop'
export const NIKE_KEY = 'Nike'
export const NEW_YORK_YANKEES_KEY = 'New York Yankees'
export const SAN_FRANSICO_GIANTS_KEY = 'San Francisco Giants'
export const WASHINGTON_NATIONALS_KEY = 'Washington Nationals'
export const BOSTAN_RED_SOX_KEY = 'Boston Red Sox'
export const SAMPLE_NEWS_1 = require('./images/news_1.png')
export const SAMPLE_NEWS_2 = require('./images/news_2.png')

import Pittsburgh from './images/pittsburgh.svg'
import Toronto from './images/toronto.svg'
import Chicago from './images/chicago.svg'
import Arizona from './images/arizona.svg'
import Brooklyn from './images/brooklyn.svg'
import Texas from './images/texas.svg'
import TampaBay from './images/tampa_bay.svg'
import MCD from './images/mcd.svg'
import MLBShop from './images/mlb.svg'
import Nike from './images/nike.svg'
import NewYorkYankees from './images/yankees.svg'
import SanFransicoGiants from './images/sanfrancisco_giants.svg'
import WashingtonNationals from './images/washington_nationals.svg'
import BostanRedFox from './images/bostan_red_sox.svg'