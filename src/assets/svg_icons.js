import AccountIcon from './icons/account_tab.svg'
import ARIcon from './icons/ar.svg'
import Availed from './icons/availed.svg'
import BackArrow from './icons/back_arrow.svg'
import Bell from './icons/bell.svg'
import CheckBoxEmpty from './icons/checkbox_empty.svg'
import CheckBoxSelected from './icons/checkbox_selected.svg'
import ChevronRight from './icons/chevron-right.svg'
import Close from './icons/close.svg'
import CoinIcon from './icons/coin.svg'
import Edit from './icons/edit.svg'
import EyeHidden from './icons/eye_hidden.svg'
import EyeVisible from './icons/eye_visible.svg'
import FacebookIcon from './icons/fb.svg'
import Filter from './icons/filter.svg'
import Flash from './icons/flash.svg'
import GoogleIcon from './icons/google.svg'
import Happy from './icons/happy.svg'
import HeaderClose from './icons/close_header.svg'
import HomeIcon from './icons/home_tab.svg'
import HuntIcon from './icons/hunt_tab.svg'
import InstagramIcon from './icons/instagram.svg'
import Laugh from './icons/laugh.svg'
import LinkedinIcon from './icons/linkedin.svg'
import LogoLarge from './icons/logo_large.svg'
import LogoSmall from './icons/logo_small.svg'
import LogoWatermark from './icons/logo_watermark.svg'
import MailIcon from './icons/mail.svg'
import MapSheet from './icons/mapsheet.svg'
import Minus from './icons/minus.svg'
import MoreIcon from './icons/more.svg'
import Mute from './icons/mute.svg'
import Neutral from './icons/neutral.svg'
import Plus from './icons/plus.svg'
import ProfileCamera from './icons/profile_camera.svg'
import Redeem from './icons/redeem.svg'
import ScanIcon from './icons/scan.svg'
import Scissor from './icons/scissor.svg'
import ScreenShot from './icons/screenshot.svg'
import TeamIcon from './icons/team_tab.svg'
import TwitterIcon from './icons/twitter.svg'
import Unhappy from './icons/unhappy.svg'
import WalletIcon from './icons/wallet_tab.svg'
import WalletQR from './icons/qr.svg'
import SoundOnIcon from './icons/sound-on.svg'
import More from './icons/more-new.svg'
import FlashOnIcon from './icons/flash-on.svg'
import FilterIcon from './icons/filter-new.svg'
import PointerIcon from './icons/pointer.svg'
import MicIcon from './icons/mic.svg'
import PiratesCoin from './icons/pirates_coin.svg'

export {
    AccountIcon,
    ARIcon,
    Availed,
    BackArrow,
    Bell,
    CheckBoxEmpty,
    CheckBoxSelected,
    ChevronRight,
    Close,
    CoinIcon,
    Edit,
    EyeHidden,
    EyeVisible,
    FacebookIcon,
    Filter,
    Flash,
    GoogleIcon,
    Happy,
    HeaderClose,
    HomeIcon,
    HuntIcon,
    InstagramIcon,
    Laugh,
    LinkedinIcon,
    LogoLarge,
    LogoSmall,
    LogoWatermark,
    MailIcon,
    MapSheet,
    Minus,
    MoreIcon,
    Mute,
    Neutral,
    Plus,
    ProfileCamera,
    Redeem,
    ScanIcon,
    Scissor,
    ScreenShot,
    TeamIcon,
    TwitterIcon,
    Unhappy,
    WalletIcon,
    WalletQR,
    SoundOnIcon,
    FlashOnIcon,
    More,
    FilterIcon,
    PointerIcon,
    MicIcon,
    PiratesCoin
}