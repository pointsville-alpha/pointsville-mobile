import {
    PixelRatio,
    Dimensions,
    Platform
} from 'react-native'

function normalizeDimension(size: number, actualDimension: number, referenceDimension: number) {
    const scaledSize = size * actualDimension / referenceDimension
    return Math.round(PixelRatio.roundToNearestPixel(scaledSize))
}

export const normalize = (size: number) =>
    normalizeDimension(size, Dimensions.get('window').width, 375)

export const normalizeVertical = (size: number) =>
    normalizeDimension(size, Dimensions.get('window').height, 812)

export function isIphoneX() {
    const dim = Dimensions.get('window');
    return (Platform.OS === 'ios' && (isIPhoneXSize(dim) || isIPhoneXrSize(dim)))
}

function isIPhoneXSize(dim: any) {
    return dim.height == 812 || dim.width == 812
}

function isIPhoneXrSize(dim: any) {
    return dim.height == 896 || dim.width == 896
}