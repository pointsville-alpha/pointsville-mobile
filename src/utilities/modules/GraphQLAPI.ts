import { gql } from "@apollo/client";

export const WHO_AM_I = gql`
mutation WhoAmI($givenName: String!, $phoneNumber: String!, $email: String!) {
    whoami(
        data: { givenName: $givenName, phoneNumber: $phoneNumber, email: $email }
    ) {
        id
        givenName
        email
    }
}`

export const HOME_DATA = gql`
query{
    orgs{
        name
        logoUrl
        rate
    }
    getAllGameSchedule {
        guid
        game_data
        orgcode
    }
}`

export const WALLET_DATA = gql`
query{
    getTransactionsByUser{
        id                
        username  
        userProfilePic 
        orgName   
        orgLogo       
        sponsorName  
        sponsorLogo  
        transactionDate
        points
    }
    
    getTransactionsGroupByOrg{
        orgName
        orgLogo
        orgRate
        price
        points
    }
    walletSummary{
        totalPoints
        totalAmount
        hunt
        promo
        received
    }    
}`

export const AVAIL_PROMO = gql`
mutation availPromo($promoCode: String!){
    availPromo(promoCode: $promoCode)
}`