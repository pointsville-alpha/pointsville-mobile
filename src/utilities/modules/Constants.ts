export const SIGN_IN_NAVIGATION_KEY = 'Sign In'
export const FORGOT_PASSWORD_NAVIGATION_KEY = 'Forgot Password'
export const FORGOT_PASSWORD_CHECK_EMAIL_NAVIGATION_KEY = 'ForgotPasswordCheckEmail'
export const FORGOT_PASSWORD_RESET_NAVIGATION_KEY = 'ForgotPasswordReset'
export const SIGN_UP_NAVIGATION_KEY = 'Sign Up'
export const INSTAGRAM_NAVIGATION_KEY = 'Instagram'
export const LINKEDIN_NAVIGATION_KEY = 'LinkedIn'
export const MAIN_NAVIGATION_KEY = 'Main'
export const SETTINGS_NAVIGATION_KEY = 'Settings'
export const GIVE_US_FEEDBACK_NAVIGATION_KEY = 'Give Us Feedback'
export const HOME_NAVIGATION_KEY = 'Home'
export const WALLET_NAVIGATION_KEY = 'Wallet'
export const HUNT_NAVIGAION_KEY = 'Hunt'
export const TEAM_NAVIGATION_KEY = 'Fan Shop'
export const ACCOUNT_NAVIGATION_KEY = 'Account'
export const TERMS_OF_USE_NAVIGATION_KEY = 'Terms Of Use'
export const PRIVACY_POLICY_NAVIGATION_KEY = 'Privacy Policy'
export const NOTIFICATION_NAVIGATION_KEY = 'Notification'
export const QRSCAN_NAVIGATION_KEY = 'QRScan'
export const TRANSFER_POINTS_NAVIGATION_KEY = 'TransferPoints'
export const REDEEM_POINTS_NAVIGATION_KEY = 'RedeemPoints'
export const PROMO_NAVIGATION_KEY = 'Promo'
export const EDIT_PROFILE_NAVIGATION_KEY = 'Edit Profile'
export const WIKITUDE_MENU_NAVIGATION_KEY = 'WikitudeMenu'
export const WIKITUDE_NAVIGATION_KEY = 'Wikitude'
export const NEWS_DETAIL_NAVIGATION_KEY = 'NewsDetail'

export const BACK_BUTTON_ICON_KEY = 'BACK_BUTTON'
export const EYE_HIDDEN_ICON_KEY = 'EYE_HIDDEN'
export const EYE_VISIBLE_ICON_KEY = 'EYE_VISIBLE'
export const CHECKBOX_EMPTY_ICON_KEY = 'CHECKBOX_EMPTY'

export const MAIL_SIGN_UP = 'MAIL_SIGN_UP'
export const GOOGLE_SIGN_UP = 'GOOGLE_SIGN_UP'
export const FACEBOOK_SIGN_UP = 'FACEBOOK_SIGN_UP'
export const MORE_SIGN_UP = 'MORE_SIGN_UP'
export const INSTAGRAM_SIGN_UP = 'INSTAGRAM_SIGN_UP'
export const TWITTER_SIGN_UP = 'TWITTER_SIGN_UP'
export const LINKEDIN_SIGN_UP = 'LINKEDIN_SIGN_UP'
export const SOCIAL_LOGIN_COLLAPSED_ITEMS = [
    MAIL_SIGN_UP,
    GOOGLE_SIGN_UP,
    FACEBOOK_SIGN_UP,
    MORE_SIGN_UP
]
export const SOCIAL_LOGIN_ALL_ITEMS = [
    MAIL_SIGN_UP,
    GOOGLE_SIGN_UP,
    FACEBOOK_SIGN_UP,
    INSTAGRAM_SIGN_UP,
    TWITTER_SIGN_UP,
    LINKEDIN_SIGN_UP
]

export const MESSAGES_SHARE_APP = 'Messages'
export const MESSENGER_SHARE_APP = 'Messenger'
export const WHATSAPP_SHARE_APP = 'WhatsApp'
export const TWITTER_SHARE_APP = 'Twitter'
export const FACEBOOK_SHARE_APP = 'Facebook'
export const INSTAGRAM_SHARE_APP = 'Instagram'

export const SHARE_APP_LIST = [
    MESSAGES_SHARE_APP,
    MESSENGER_SHARE_APP,
    WHATSAPP_SHARE_APP,
    TWITTER_SHARE_APP,
    FACEBOOK_SHARE_APP,
    INSTAGRAM_SHARE_APP
]

export const HELPLINE_NUMBER = '(626) 794-0000'

//permissions rationale messages
export const CAMERA_PERMISSION_RTIONALE = "We need permission to access your device camera"
export const CAMERA_PERMISSION_RTIONALE_DESC = "This functionality relies on camera. We require access to this permission to open the camera of your device."
export const LOCATION_PERMISSION_RTIONALE = "We need permission to access your current location"
export const LOCATION_PERMISSION_RTIONALE_DESC = "This functionality relies on your current location. We require access to this permission to get your current location."
export const STORAGE_PERMISSION_RTIONALE = "We need permission to access your external storage"
export const STORAGE_PERMISSION_RTIONALE_DESC = "We require access to this permission to store the images in your device."