export function hasKey<O>(obj: O, key: keyof any): key is keyof O {
    return key in obj
}

export enum Variant {
    small = 0.5,
    medium = 0.65,
    extraMedium = 0.75,
    large = 0.80,
    default = 1
}

export type Variants = keyof typeof Variant