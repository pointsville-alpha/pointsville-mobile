export const SIGN_IN_TITLE = 'Sign In'
export const FORGOT_PASSWORD_QUESTION = 'Forgot Password ?'
export const PASSWORD_TITLE = 'Password'
export const EMAIL_TITLE = 'Email'
export const SIGN_UP_WITH_TITLE = 'Sign up with'

export const FORGOT_PASSWORD_TITLE = 'Forgot Password'
export const FORGOT_PASSWORD_SCREEN_TITLE = `Don't worry, we have covered it for you!`
export const FORGOT_PASSWORD_DESCRIPTION = 'Enter your email address or phone number to reset your password.'
export const PLEASE_ENTER_AN_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE = 'Please enter an email address or phone number'
export const PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE = 'Please enter a valid email address or phone number'
export const NEXT_TITLE = 'Next'
export const OPEN_EMAIL_APP_TITLE = 'Open Email App'
export const CHECK_YOUR_EMAIL_EXCLAMATION = 'Check your email!'
export const CHECK_EMAIL_DESCRIPTION = 'To confirm your email address, \ntap the button in the email we sent to'
export const PLEASE_ENTER_NEW_PASSWORD_TITLE = 'Please enter new password'
export const PLEASE_RETYPE_NEW_PASSWORD_TITLE = 'Please retype new password'
export const CHANGE_PASSWORD_TTTLE = 'Change Password'
export const CODE_TITLE = 'Code'
export const NEW_PASSWORD_TITLE = 'New Password'
export const RETYPE_NEW_PASSWORD_TITLE = 'Retype New Password'
export const PASSWORDS_DO_NOT_MATCH_TITLE = 'Passwords do not match'
export const PASSWORD_CHANGED_TITLE = 'Password changed'
export const OK_TITLE = 'OK'
export const ALERT_TITLE = 'Alert'

export const SIGN_UP_TITLE = 'Sign Up'
export const FULL_NAME_TITLE = 'Full Name'
export const FIRST_NAME_TITLE = 'First Name'
export const LAST_NAME_TITLE = 'Last Name'
export const PHONE_NUMBER_TITLE = 'Phone Number'
export const BY_REGISTERING_YOU_AGREE_WITH_THE_TITLE = 'By registering, you agree with the '
export const TERMS_AMPERSAND_POLICY_TITLE = 'Terms & Policy'
export const PLEASE_ENTER_A_NAME_TITLE = 'Please enter a name'
export const PLEASE_ENTER_A_VALID_NAME_TITLE = 'Please enter a valid name'
export const PLEASE_ENTER_AN_EMAIL_ADDRESS = 'Please enter an email address'
export const PLEASE_ENTER_A_VALID_EMAIL_ADDRESS = 'Please enter a valid email address'
export const PLEASE_ENTER_A_VALID_PHONE_NUMBER = 'Please enter a valid phone number'
export const SIGNED_UP_SUCCESSFULLY_TITLE = 'Signed up successfully'

export const PLEASE_VERIFY_YOUR_EMAIL_ID = 'Please verify your email ID...'
export const VERIFICATION_CODE_DESC = 'We have sent a 4 digit verification code to '
export const TIME_UP = 'Time up!'
export const WOULD_YOU_LIKE_TO_RESEND_CODE = 'Would you like to resend a code to'
export const RESEND_CODE = "Resend Code"
export const VERIFY = "Verify"

export const HOME_TITLE = 'Home'
export const WALLET_TITLE = 'Wallet'
export const HUNT_TITLE = 'Hunt'
export const ACCOUNT_TITLE = 'Account'
export const TEAM_TITLE = 'Team'
export const DASHBOARD_TITLE = 'Dashboard'
export const MY_WALLET_TITLE = 'My Wallet'
export const WALLET_ID_TITLE = 'Wallet ID'

export const HAVE_A_SIGN_UP_CODE_QUESTION_EARN_POINTS_EXCLAMATION = 'Have a promo code? Earn Points!'
export const AVAIL_NOW_TITLE = 'AVAIL NOW'
export const GAMES_TITLE = 'Games'
export const TODAY_TITLE = 'Today'
export const UPCOMING_TITLE = 'Upcoming'
export const NEWS_TITLE = 'News'
export const VIEW_FULL_STORY_TITLE = 'View full story'
export const EARN_SIGNUP_POINTS_EXCLAMATION = 'Earn Points!'
export const PROMO_CODE_EXCLAMATION = 'Promo Code!'
export const ENTER_THE_CODE_FROM_THE_EMAIL_SENT_TITLE = 'Enter the code from the email sent'
export const EARN_SIGN_UP_POINTS_PLACEHOLDER = 'ex:signup50'
export const CLAIM_POINTS_TITLE = 'Claim Points'
export const ADD_TO_WALLET = 'Add to Wallet'
export const OFFERINGS_TITLE = 'Offerings'
export const PROMO_CODE_AVAILED = 'The promo code is avalied successfully.'
export const INVALID_PROMO_CODE = 'You have entered an invalid promo code.'

export const PUSH_NOTIFICATIONS_TITLE = 'Push Notifications'
export const SHARE_THE_POINTSVILLE_APP_TITLE = 'Share the PointsVille App'
export const SHARE_TITLE = 'Share'
export const GIVE_US_FEEDBACK_TITLE = 'Give Us Feedback'
export const LOGOUT_TITLE = 'Logout'
export const CANCEL_TITLE = 'Cancel'

export const WALLET_BALANCE_TITLE = 'Wallet Balance'
export const PROMO_TITLE = 'Promo'
export const RECEIVED_TITLE = 'Received'
export const MY_POINTS_TITLE = 'My Points'
export const TRANSACTIONS_TITLE = 'Transactions'
export const TRANSFER_TITLE = 'TRANSFER'
export const REDEEM_TITLE_UPPERCASE = 'REDEEM'
export const RECEIVE_TITLE = 'RECEIVE'

export const SETTINGS_TITLE = 'Settings'
export const TERMS_OF_USE_TITLE = 'Terms of Use'
export const PRIVACY_POLICY_TITLE = 'Privacy Policy'

export const ANYTHING_TO_BE_IMPROVED_QUESTION = 'Anything to be Improved?'
export const YOUR_FEEDBACK_OPTIONAL_TITLE = 'Your feedback (optional)'
export const HOW_GOOD_IS_POINTSVILLE_QUESTION = 'How Good is PointsVille'
export const RATE_YOUR_EXPERIENCE_TITLE = 'Rate your Experience'
export const SUBMIT_TITLE = 'Submit'
export const NEED_HELP_QUESTION = 'Need Help?'
export const CONNECT_WITH_US_TITLE = 'Connect with us'

export const NOTIFICATION_TITLE = 'Notification'

export const YOUR_FRIENDS_CAN_SCAN_DESCRIPTION = 'Your friends can scan this code to send and receive points.'
export const COPY_ID_TITLE = 'Copy ID'

export const POINTS_COLON = 'Points:'
export const VALUE_COLON = 'Value:'
export const PROCEED_TITLE = 'Proceed'
export const REDEEM_TITLE = 'Redeem'
export const PTS_TITLE = 'Pts'
export const ENTER_WALLET_ID_TITLE = 'Enter Wallet ID'
export const ENTER_WALLET_ID_DESCRIPTION = `Pirates’s Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's  dummy.`

export const NEW_PROMO_TITLE = 'New Promo'
export const AVAILED_TITLE = 'Availed'

export const HUNT_NOW_EXCLAMATION = 'Hunt Now!'
export const HUNT_NOW_SAFETY_MESSAGE = [
    'You are entering ',
    'to hunt PointsVille points. ',
    'is most important and we ensure you to be careful and watch your steps. '
]
export const HUNT_NOW_SAFETY_MESSAGE_BOLD = [
    'MAP/AR mode ',
    'Safety '
]

export const SEE_MORE_TITLE = 'See More'

export const SEARCH_LOCATION = "Search Location"