import { normalize } from './Measurement'

export enum FONT {
    black = 'Graphik-Black',
    black_italic = 'Graphik-BlackItalic',
    bold = 'Graphik-Bold',
    bold_italic = 'Graphik-BoldItalic',
    extra_light = 'Graphik-Extralight',
    light = 'Graphik-Light',
    light_italic = 'Graphik-LightItalic',
    medium = 'Graphik-Medium',
    medium_italic = 'Graphik-MediumItalic',
    regular = 'Graphik-Regular',
    regular_italic = 'Graphik-RegularItalic',
    semiBold = 'Graphik-Semibold',
    semiBold_italic = 'Graphik-SemiboldItalic',
    thin = 'Graphik-Thin',
    thin_italic = 'Graphik-ThinItalic',
    super = 'Graphik-Super',
    super_italic = 'Graphik-SuperItalic',
}

export enum FONTSIZE {
    tiny = normalize(9),
    small_tiny = normalize(10),
    small_medium = normalize(11),
    small = normalize(12),
    regular = normalize(14),
    regular_medium = normalize(15),
    medium = normalize(16),
    extra_medium = normalize(18),
    large = normalize(20),
    extra_large = normalize(22),
    very_large = normalize(24),
}

export enum FONTALIGNMENT {
    auto = 'auto',
    left = 'left',
    right = 'right',
    center = 'center',
    justify = 'justify',
}