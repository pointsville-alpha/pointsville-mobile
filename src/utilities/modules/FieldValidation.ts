export function isEmpty(value?: string) {
    return !(value && value.trim().length > 0)
}

export function validatePassword(input: string) {
    let password = input.trim()
    if (password.length < 8)
        return {
            success: false,
            error: 'Password must be atleast 8 characters long'
        }
    else if (!/\d/.test(password))
        return {
            success: false,
            error: 'Password must contain atleast one numerical digit'
        }
    return {
        success: true,
        error: null
    }
}

export function validateName(name: string) {
    return name.trim().length > 1
}

export function validateEmail(email: string) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    return reg.test(email.trim()) === true
}

export function validatePhoneNumber(input: string) {
    let phone = input.trim()
    return /^\+[1-9]{1}[0-9]{3,14}/.test(phone)
}