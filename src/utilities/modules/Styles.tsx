import React from 'react'
import { StyleSheet } from 'react-native'
import {
    Colors,
    normalizeVertical,
    normalize,
    FONT,
    FONTSIZE
} from '..'
import { BackButton } from '../../components'

export const InputStyle = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: normalizeVertical(58),
        alignItems: 'center',
        borderBottomWidth: 1
    },
    field: {
        flex: 1,
        backgroundColor: 'transparent',
        letterSpacing: 0,
        textAlignVertical: 'auto'
    }
})

export const defaultHeaderOptions = {
    headerLeft: () => <BackButton />,
    headerStyle: {
        backgroundColor: Colors.BACKGROUND,
        elevation: 0,
        shadowOpacity: 0,
        height: normalizeVertical(102)
    },
    headerLeftContainerStyle: {
        marginHorizontal: normalize(14)
    },
    headerTitleStyle: {
        color: Colors.WHITE,
        fontSize: FONTSIZE.extra_medium,
        fontFamily: FONT.medium,
        lineHeight: FONTSIZE.large
    },
    headerRightContainerStyle: {
        marginHorizontal: normalize(14)
    },
    headerTitleAlign: 'center'
}