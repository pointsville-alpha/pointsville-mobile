import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import { imageSelect } from '../../../assets/ImageSelect'
import { Availed } from '../../../assets/svg_icons'
import { Label } from '../../../components'
import Coin from '../../../components/atomic/Coin'
import {
    Colors,
    normalize
} from '../../../utilities'

interface IPromoItemProps {
    image: string
    title: string
    date: string
    validity?: string
    code?: string
    points?: string
}

export default function PromoItem(props: IPromoItemProps) {
    return <View style={styles.container}>
        <View style={styles.image}
            children={imageSelect(props.image)} />
        <View style={styles.textContainer}>
            <Label children={props.title} />
            <View style={styles.lowerTextContainer}>
                <Label children={props.date}
                    color='WHITE_ALPHA_50'
                    fontSize='small_medium' />
                {props.validity &&
                    <Label children={' - ' + props.validity}
                        style={styles.validity}
                        color='WHITE_ALPHA_50'
                        fontSize='tiny' />}
            </View>
        </View>
        {
            props.code &&
            <View style={styles.codeContainer}>
                <Label children={props.code}
                    fontSize='small'
                    fontFamily='medium' />
            </View>
        }
        {
            props.points &&
            <View>
                <Availed style={styles.availed} />
                <View style={styles.pointsContainer} >
                    <Coin />
                    <Label children={'+' + props.points}
                        style={styles.points}
                        color='BRIGHT_RED'
                        fontFamily='medium' />
                </View>
            </View>
        }
    </View>
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: normalize(60),
        alignItems: 'center',
        flexDirection: 'row',
        paddingStart: normalize(8),
        paddingEnd: normalize(6),
        backgroundColor: Colors.WHITE_ALPHA_5,
        borderRadius: normalize(6),
        overflow: 'hidden'
    },
    image: {
        height: normalize(40),
        width: normalize(40),
        marginEnd: normalize(6)
    },
    textContainer: {
        flex: 1,
        justifyContent: 'space-between',
        marginEnd: normalize(8.5)
    },
    lowerTextContainer: {
        marginTop: normalize(4),
        flexDirection: 'row'
    },
    codeContainer: {
        width: normalize(96),
        height: normalize(29),
        borderRadius: normalize(3),
        borderWidth: 1,
        borderStyle: 'dashed',
        borderColor: Colors.WHITE_ALPHA_30,
        backgroundColor: Colors.WHITE_ALPHA_5,
        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: normalize(8.5)
    },
    pointsContainer: {
        position: 'absolute',
        flexDirection: 'row',
        top: normalize(16),
        end: normalize(15),
        alignItems: 'center'
    },
    points: {
        marginStart: normalize(4),
    },
    availed: {
        bottom: normalize(16)
    },
    validity: {
        flex: 1
    }
})