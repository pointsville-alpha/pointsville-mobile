import React from 'react'
import {
    SafeAreaView,
    StyleSheet,
    View
} from 'react-native'
import {
    FlatList,
    TouchableOpacity
} from 'react-native-gesture-handler'
import {
    Gradient,
    Label,
    LighterBackground,
    ScreenContainer
} from '../../components'
import {
    AVAILED_PROMOS,
    NEW_PROMOS
} from '../../placeholderdata/Promo'
import {
    AVAILED_TITLE,
    NEW_PROMO_TITLE,
    normalize,
    normalizeVertical
} from '../../utilities'
import InfoSection from '../transfer_points/components/InfoSection'
import PromoItem from './components/PromoItem'

interface Props {
    navigation: any
    route: any
}

interface Istate {
    showNew: boolean
    newList: any[]
    availedList: any[]
}

export default class Promo extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            showNew: true,
            newList: [],
            availedList: []
        }
    }

    componentDidMount() {
        this.setState({ newList: NEW_PROMOS, availedList: AVAILED_PROMOS })
    }

    render() {
        return <ScreenContainer style={styles.container}>
            <LighterBackground style={styles.lighterBackground} />
            <SafeAreaView style={styles.infoSection}>
                <InfoSection team={this.props.route.params.team}
                    points={'201'}
                    total={'$ 301.50'}
                    price={this.props.route.params.price}
                    image={this.props.route.params.image} />
            </SafeAreaView>
            <View style={styles.main}>
                <View style={styles.header}>
                    {this.state.newList.length > 0 &&
                        <TouchableOpacity
                            onPress={() => this.setState({ showNew: true })}
                            activeOpacity={1}
                            style={styles.option}>
                            <Label children={NEW_PROMO_TITLE}
                                fontFamily='medium'
                                fontSize='small' />
                            {(this.state.showNew || this.state.availedList.length == 0)
                                && <Gradient shadowStyle={styles.shadow}
                                    style={styles.selection} />}
                        </TouchableOpacity>}
                    {this.state.availedList.length > 0 &&
                        <TouchableOpacity
                            onPress={() => this.setState({ showNew: false })}
                            activeOpacity={1}
                            style={styles.option}>
                            <Label children={AVAILED_TITLE}
                                fontFamily='medium'
                                fontSize='small' />
                            {(!this.state.showNew || this.state.availedList.length == 0)
                                && <Gradient shadowStyle={styles.shadow}
                                    style={styles.selection} />}
                        </TouchableOpacity>}
                </View>
                <FlatList showsVerticalScrollIndicator={false}
                    data={this.state.showNew ? NEW_PROMOS : AVAILED_PROMOS}
                    renderItem={({ item }: any) =>
                        <PromoItem image={item.image}
                            code={item.code}
                            points={item.points}
                            validity={item.validity}
                            date={item.date}
                            title={item.title} />
                    }
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    keyExtractor={(_, index) => 'Item_' + index} />
            </View>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    lighterBackground: {
        position: 'absolute'
    },
    container: {
        paddingHorizontal: normalize(14)
    },
    infoSection: {
        marginHorizontal: normalize(16)
    },
    main: {
        flex: 1,
        justifyContent: 'center',
        marginTop: normalizeVertical(92)
    },
    header: {
        flexDirection: 'row',
        marginBottom: normalize(15)
    },
    option: {
        marginEnd: normalize(20)
    },
    selection: {
        marginTop: normalize(5),
        width: '100%',
        height: 2,
        borderRadius: 1
    },
    shadow: {
        shadowOpacity: 0
    },
    separator: {
        height: normalize(10)
    }
})