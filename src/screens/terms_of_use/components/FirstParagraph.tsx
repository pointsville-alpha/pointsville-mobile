import React from 'react'
import {
    Text,
    StyleSheet
} from 'react-native'
import PrivacyLabel from '../../privacy_policy/components/PrivacyLabel'
import { PRIVACY_POLICY_NAVIGATION_KEY, PRIVACY_POLICY_TITLE } from '../../../utilities'
import { FONT } from '../../../utilities/modules/Fonts'
import { useNavigation } from '@react-navigation/native'

export default function FirstParagraph() {
    const navigation = useNavigation()
    return <PrivacyLabel>
        <Text>
            {TERMS_OF_USE_FIRST_PARAGRAPH[0]}
            <Text onPress={() => navigation.navigate(PRIVACY_POLICY_NAVIGATION_KEY)}
                children={PRIVACY_POLICY_TITLE}
                style={styles.privacyPolicy} />
            {TERMS_OF_USE_FIRST_PARAGRAPH[1]}
            <Text onPress={() => navigation.navigate(PRIVACY_POLICY_NAVIGATION_KEY)}
                children={PRIVACY_POLICY_TITLE}
                style={styles.privacyPolicy} />
            {TERMS_OF_USE_FIRST_PARAGRAPH[2]}
        </Text>
    </PrivacyLabel>
}

const styles = StyleSheet.create({
    privacyPolicy: {
        fontFamily: FONT.bold
    }
})

const TERMS_OF_USE_FIRST_PARAGRAPH = [
    'Please read the following terms and conditions carefully. By using and accessing the Site, you acknowledge that you have read these Terms of Use (“Terms”) and agree to abide by them and to the terms and conditions of PointsVille’s ',
    ' (“Policy”) hereby incorporated by reference, which details the ways in which we and other parties might use personal information that belongs to you. If you do not agree to these Terms or those in PointsVille’s ',
    ', or you do not meet any eligibility requirements established by PointsVille International (“PointsVille”, “we”, “us” or “our”) for use of and access to the Site (defined below), you may not access, view, obtain goods or services from, or otherwise use the Site. Use of the Site by individuals under the age of 18 is not except under the supervision of a parent, legal guardian, or other responsible adult. Children under the age of 13 are not allowed to visit or otherwise use the Site except in the case where the Child Privacy Policy Addendum explicitly applies.'
]