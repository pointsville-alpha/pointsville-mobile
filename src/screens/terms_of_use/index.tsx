import React from 'react'
import { StyleSheet } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import {
    Label,
    ScreenContainer
} from '../../components'
import CloseButton from '../../components/atomic/CloseButton'
import { normalize } from '../../utilities'
import PrivacyLabel from '../privacy_policy/components/PrivacyLabel'
import FirstParagraph from './components/FirstParagraph'

interface Props {
    navigation: any
}

export default class TermsOfUse extends React.Component<Props> {

    constructor(props: Props) {
        super(props)
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerLeft: () => <CloseButton />
        })
    }

    render() {
        return <ScreenContainer>
            <ScrollView contentContainerStyle={styles.container}>
                <PrivacyLabel children={TERMS_EFFECTIVE_DATE} />
                <FirstParagraph />
                <PrivacyLabel children={PARTNER_SERVICES_TITLE} isTitle />
                <PrivacyLabel children={PARTNER_SERVICES_DESCRIPTION} />
                <PrivacyLabel children={SITE_DEFINED_TITLE} isTitle />
                <PrivacyLabel children={SITE_DEFINED_DESCRIPTION} />
                <PrivacyLabel children={MODIFICATION_OF_THESE_TERMS_TITLE} isTitle />
                <PrivacyLabel children={MODIFICATION_OF_THESE_TERMS_DESCRIPTION} />
                <Label children={SITE_USE_TITLE}
                    style={styles.siteUse}
                    color='WHITE_ALPHA_50'
                    fontSize='medium'
                    fontFamily='bold' />
                <PrivacyLabel children={RESTRICTIONS_ON_USE_TITLE} isTitle />
                <PrivacyLabel children={RESTRICTIONS_ON_USE_DESCRIPTION} />
            </ScrollView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(16),
        paddingBottom: normalize(30)
    },
    siteUse: {
        top: normalize(30),
        marginBottom: normalize(10)
    }
})

const TERMS_EFFECTIVE_DATE = '\nEffective as of: April 15, 2020\n'
const PARTNER_SERVICES_TITLE = 'PARTNER SERVICES'
const PARTNER_SERVICES_DESCRIPTION = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
const SITE_DEFINED_TITLE = '“SITE” DEFINED'
const SITE_DEFINED_DESCRIPTION = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
const MODIFICATION_OF_THESE_TERMS_TITLE = 'MODIFICATION OF THESE TERMS'
const MODIFICATION_OF_THESE_TERMS_DESCRIPTION = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
const SITE_USE_TITLE = 'SITE USE'
const RESTRICTIONS_ON_USE_TITLE = 'Restrictions on Use'
const RESTRICTIONS_ON_USE_DESCRIPTION = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`