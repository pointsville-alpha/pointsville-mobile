import React from 'react'
import {
    StyleSheet,
    View,
    Text
} from 'react-native'
import { imageSelect } from '../../../assets/ImageSelect'
import { Label } from '../../../components'
import {
    normalize,
    Colors
} from '../../../utilities'

interface INotificationItemProps {
    image: string
    title: string
    highlight: string
    date: string
}

export default function NotificationItem(props: INotificationItemProps) {
    return <View style={styles.container}>
        <View style={styles.main}>
            <View style={styles.image}
                children={imageSelect(props.image, normalize(24))} />
            <Label style={styles.text}
                numberOfLines={6}
                color='WHITE_ALPHA_80'>
                {props.title}
                <Text children={' ' + props.highlight}
                    style={styles.highlight} />
            </Label>
        </View>
        <Label children={props.date}
            style={styles.date}
            color='WHITE_ALPHA_50'
            fontSize='small_medium' />
    </View>
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: normalize(12)
    },
    main: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        height: normalize(30),
        width: normalize(30),
        borderRadius: normalize(15),
        backgroundColor: Colors.WHITE_ALPHA_5,
        marginEnd: normalize(8),
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        marginTop: normalize(2),
        flex: 1
    },
    highlight: {
        fontFamily: 'Graphik-semiBold'
    },
    date: {
        marginStart: normalize(38)
    }
})