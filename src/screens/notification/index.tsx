import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import { ScreenContainer } from '../../components'
import {
    normalize,
    NOTIFICATION_NAVIGATION_KEY,
    Colors
} from '../../utilities'
import CloseButton from '../../components/atomic/CloseButton'
import { FlatList } from 'react-native-gesture-handler'
import { NOTIFICATIONS } from '../../placeholderdata/Notification'
import NotificationItem from './components/NotificationItem'

interface Props {
    navigation: any
}

export default class Notification extends React.Component<Props> {

    constructor(props: Props) {
        super(props)
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerTitle: NOTIFICATION_NAVIGATION_KEY,
            headerLeft: () => <CloseButton />
        })
    }

    render() {
        return <ScreenContainer style={styles.container}>
            <FlatList showsVerticalScrollIndicator={false}
                data={NOTIFICATIONS}
                renderItem={({ item }) =>
                    <NotificationItem highlight={item.highlight}
                        image={item.image}
                        date={item.date}
                        title={item.title} />
                }
                ItemSeparatorComponent={() => <View style={styles.separator} />}
                keyExtractor={(_, index) => 'Item_' + index} />
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(14)
    },
    separator: {
        height: 0.5,
        backgroundColor: Colors.WHITE_ALPHA_10,
        marginStart: normalize(38),
        width: '100%'
    }
})