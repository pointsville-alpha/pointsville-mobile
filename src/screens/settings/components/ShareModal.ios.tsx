import React from 'react'
import {
    Modal,
    View,
    StyleSheet, Platform
} from 'react-native'
import Label from '../../../components/atomic/Label'
import { TouchableWithoutFeedback } from 'react-native-gesture-handler'
import ShareCancelButton from './ShareCancelButton'
import ShareAppList from './ShareAppList'
import { normalize } from '../../../utilities/modules/Measurement'
import {
    SHARE_THE_POINTSVILLE_APP_TITLE,
    Colors
} from '../../../utilities'

interface IShareModalProps {
    visible: boolean
    dismiss: () => void
}

const Contents = (props: IShareModalProps) => {
    return <View style={styles.container}>
        <TouchableWithoutFeedback style={styles.innerContainer}
            onPressOut={props.dismiss}>
            <TouchableWithoutFeedback style={styles.subContainer}
                onPress={() => { }}>
                <Label children={SHARE_THE_POINTSVILLE_APP_TITLE}
                    fontFamily='medium'
                    fontSize='medium'
                    style={styles.shareTheApp} />
                <ShareAppList />
                <ShareCancelButton dismiss={props.dismiss} />
            </TouchableWithoutFeedback>
        </TouchableWithoutFeedback>
    </View>
}

export default function ShareModal(props: IShareModalProps) {
    return Platform.OS === 'ios' ?
        <Modal animationType='fade'
            transparent={true}
            visible={props.visible}>
            {<Contents dismiss={props.dismiss}
                visible={props.visible} />}
        </Modal> :
        <View>
            {<Contents dismiss={props.dismiss}
                visible={props.visible} />}
        </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.BLACK_ALPHA_60
    },
    innerContainer: {
        height: '100%',
        width: '100%',
        justifyContent: 'flex-end',
    },
    subContainer: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: Colors.BACKGROUND,
        borderTopStartRadius: normalize(20),
        borderTopEndRadius: normalize(20),
        paddingTop: normalize(30)
    },
    shareTheApp: {
        textAlign: 'center',
        marginBottom: normalize(20)
    }
})