import React from 'react'
import {
    FlatList,
    StyleSheet,
    View
} from 'react-native'
import ShareAppItem from './ShareAppItem'
import { SHARE_APP_LIST } from '../../../utilities/modules/Constants'
import { normalize } from '../../../utilities/modules/Measurement'

export default function ShareAppList() {
    return <FlatList
        data={SHARE_APP_LIST}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={styles.list}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        renderItem={({ item }) => <ShareAppItem title={item} />}
        keyExtractor={(_, index) => 'App_' + index.toString()} />
}

const styles = StyleSheet.create({
    list: {
        paddingHorizontal: normalize(16)
    },
    separator: {
        width: normalize(28)
    }
})