import React from 'react'
import {
    View,
    StyleSheet,
    Image
} from 'react-native'
import { Colors } from '../../../utilities/modules/Colors'
import Label from '../../../components/atomic/Label'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { normalize } from '../../../utilities/modules/Measurement'
import {
    MESSAGES_SHARE_APP,
    INSTAGRAM_SHARE_APP,
    WHATSAPP_SHARE_APP,
    TWITTER_SHARE_APP,
    FACEBOOK_SHARE_APP,
    MESSENGER_SHARE_APP
} from '../../../utilities'

const instagramIcon = require('../../../assets/icons/ic_instagram.png')
const twitterIcon = require('../../../assets/icons/ic_twitter.png')
const messagesIcon = require('../../../assets/icons/ic_message.png')
const whatsappIcon = require('../../../assets/icons/ic_whatsapp.png')
const messengerIcon = require('../../../assets/icons/ic_messanger.png')
const facebookIcon = require('../../../assets/icons/ic_facebook.png')

interface IShareAppItemProps {
    title: string
}

const iconSelect = (title: string) => {
    switch (title) {
        case MESSAGES_SHARE_APP: return messagesIcon
        case INSTAGRAM_SHARE_APP: return instagramIcon
        case WHATSAPP_SHARE_APP: return whatsappIcon
        case TWITTER_SHARE_APP: return twitterIcon
        case MESSENGER_SHARE_APP: return messengerIcon
        case FACEBOOK_SHARE_APP: return facebookIcon
        default: return null
    }
}

const ShareAppItem = ({ title }: IShareAppItemProps) =>
    <View style={styles.container}>
        <TouchableOpacity style={styles.imageContainer}>
            <Image source={iconSelect(title)}
                resizeMode='contain' />
        </TouchableOpacity>
        <Label children={title}
            color='WHITE_ALPHA_40'
            style={styles.text} />
    </View>

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    imageContainer: {
        height: normalize(57),
        width: normalize(57),
        borderRadius: normalize(29),
        backgroundColor: Colors.WHITE_ALPHA_3,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: normalize(10)
    },
    text: {
        textAlign: 'center',
        lineHeight: normalize(20),
        marginBottom: normalize(32)
    }
})

export default ShareAppItem