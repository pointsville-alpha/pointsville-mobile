import React from 'react'
import {
    StyleSheet
} from 'react-native'
import {
    normalize,
    Colors
} from '../../../utilities'
import Label from '../../../components/atomic/Label'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface ISettingItemProps {
    title: string
    component?: any
    onPress?: () => void
    isLast?: boolean
}

export default function SettingItem(props: ISettingItemProps) {
    return <TouchableOpacity onPress={props.onPress}
        style={[styles.container, props.isLast && styles.lastContainer]}
        activeOpacity={props.onPress ? 0.5 : 1}>
        <Label children={props.title}
            style={styles.title}
            color={props.isLast ? 'BRIGHT_RED' : 'WHITE_ALPHA_50'}
            fontSize='medium' />
        {props.component}
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: normalize(64),
        marginHorizontal: normalize(14),
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomColor: Colors.WHITE_ALPHA_10,
        borderBottomWidth: 0.5
    },
    lastContainer: {
        borderBottomColor: 'transparent'
    },
    title: {
        lineHeight: normalize(24)
    }
})