import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import Label from '../../../components/atomic/Label'
import ShareCancelButton from './ShareCancelButton'
import ShareAppList from './ShareAppList'
import { normalize } from '../../../utilities/modules/Measurement'
import {
    SHARE_THE_POINTSVILLE_APP_TITLE,
    Colors
} from '../../../utilities'

interface IShareModalProps {
    visible: boolean,
    dismiss: () => void,
}

const ShareModal = (props: IShareModalProps) => {
    if (props.visible)
        return <View style={styles.container}>
            <View style={styles.subContainer} >
                <Label children={SHARE_THE_POINTSVILLE_APP_TITLE}
                    fontFamily='medium'
                    fontSize='medium'
                    style={styles.shareTheApp} />
                <ShareAppList />
                <ShareCancelButton dismiss={props.dismiss} />
            </View>
        </View>
    else return null
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundColor: Colors.BLACK_ALPHA_60,
        justifyContent: 'flex-end'
    },
    subContainer: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: Colors.BACKGROUND,
        borderTopStartRadius: normalize(20),
        borderTopEndRadius: normalize(20),
        paddingTop: normalize(30)
    },
    shareTheApp: {
        textAlign: 'center',
        marginBottom: normalize(20)
    }
})

export default ShareModal