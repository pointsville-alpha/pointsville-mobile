import React, { useState } from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {
    normalize,
    Colors
} from '../../../utilities'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function SettingSwitch() {
    const [isOn, toggle] = useState(false)
    return <TouchableOpacity onPress={() => toggle(!isOn)}
        activeOpacity={1}>
        <LinearGradient start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            angle={135}
            colors={isOn ?
                [Colors.GRADIENT_START, Colors.GRADIENT_END] :
                [Colors.GRAY, Colors.GRAY]}
            style={styles.background}>
            <View style={[styles.thumb, isOn && styles.isOn]} />
        </LinearGradient>
    </TouchableOpacity>
}


const styles = StyleSheet.create({
    background: {
        width: normalize(32),
        height: normalize(18),
        borderRadius: normalize(40),
        shadowColor: '#4157F2',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.3,
        shadowRadius: 13,
        padding: normalize(3),
        justifyContent: 'center'
    },
    thumb: {
        backgroundColor: Colors.WHITE,
        width: normalize(12),
        height: normalize(12),
        borderRadius: normalize(6)
    },
    isOn: {
        alignSelf: 'flex-end'
    }
})