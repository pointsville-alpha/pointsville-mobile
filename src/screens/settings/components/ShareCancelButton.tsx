import React from 'react'
import Label from '../../../components/atomic/Label'
import { StyleSheet } from 'react-native'
import { CANCEL_TITLE } from '../../../utilities/modules/Strings'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Colors } from '../../../utilities/modules/Colors'
import { FONTSIZE } from '../../../utilities/modules/Fonts'
import { normalize } from '../../../utilities/modules/Measurement'

interface IShareCancelButtonProps {
    dismiss: () => void
}

export default function ShareCancelButton(props: IShareCancelButtonProps) {
    return <TouchableOpacity onPress={props.dismiss}
        style={styles.container}>
        <Label children={CANCEL_TITLE}
            color='WHITE_ALPHA_50'
            fontFamily='medium' />
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    container: {
        height: normalize(60),
        width: normalize(343),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderTopWidth: 1,
        borderTopColor: Colors.WHITE_ALPHA_10
    },
    text: {
        lineHeight: FONTSIZE.extra_medium
    }
})