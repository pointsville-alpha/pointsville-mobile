import React from 'react'
import { CommonActions } from '@react-navigation/native'
import { ScreenContainer } from '../../components'
import {
    PUSH_NOTIFICATIONS_TITLE,
    SHARE_THE_POINTSVILLE_APP_TITLE,
    GIVE_US_FEEDBACK_TITLE,
    LOGOUT_TITLE,
    GIVE_US_FEEDBACK_NAVIGATION_KEY,
    SIGN_IN_NAVIGATION_KEY, normalize
} from '../../utilities'
import SettingItem from './components/SettingItem'
import SettingSwitch from './components/SettingSwitch'
import ShareModal from './components/ShareModal'
import { ChevronRight } from '../../assets/svg_icons'
import { StyleSheet } from 'react-native'
import { Auth } from 'aws-amplify'

interface Props {
    navigation: any
}

interface Istate {
    modal: boolean
}

export default class Settings extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            modal: false
        }
    }

    giveUsFeedback = () => this.props.navigation.navigate(GIVE_US_FEEDBACK_NAVIGATION_KEY)

    shareApp = () => this.setState({ modal: true })

    logout = () => {
        Auth.signOut();
        // this.props.navigation.dispatch(
        //     CommonActions.reset({
        //         index: 0,
        //         routes: [{ name: SIGN_IN_NAVIGATION_KEY }]
        //     })
        // )
    }

    render() {
        return <ScreenContainer>
            <SettingItem component={<SettingSwitch />}
                title={PUSH_NOTIFICATIONS_TITLE} />
            <SettingItem onPress={this.shareApp}
                title={SHARE_THE_POINTSVILLE_APP_TITLE} />
            <SettingItem onPress={this.giveUsFeedback}
                component={<ChevronRight style={styles.chevron} />}
                title={GIVE_US_FEEDBACK_TITLE} />
            <SettingItem onPress={this.logout}
                title={LOGOUT_TITLE}
                isLast />
            <ShareModal visible={this.state.modal}
                dismiss={() => this.setState({ modal: false })} />
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    chevron: {
        marginEnd: normalize(10)
    }
})