import React from 'react'
import {
    View,
    StyleSheet,
    KeyboardAvoidingView,
    ScrollView,
    Alert,
    Platform,
    TouchableOpacity
} from 'react-native'
import {
    BackButton,
    Button,
    Label,
    Logo,
    ScreenContainer,
} from '../../components'
import {
    normalize,
    SIGN_UP_TITLE,
    FULL_NAME_TITLE,
    EMAIL_TITLE,
    PHONE_NUMBER_TITLE,
    NEW_PASSWORD_TITLE,
    RETYPE_NEW_PASSWORD_TITLE,
    BY_REGISTERING_YOU_AGREE_WITH_THE_TITLE,
    TERMS_AMPERSAND_POLICY_TITLE,
    validateName,
    validateEmail,
    validatePhoneNumber,
    isEmpty,
    validatePassword,
    MAIN_NAVIGATION_KEY,
    PLEASE_ENTER_A_NAME_TITLE,
    PLEASE_ENTER_A_VALID_NAME_TITLE,
    PLEASE_ENTER_AN_EMAIL_ADDRESS,
    PLEASE_ENTER_A_VALID_EMAIL_ADDRESS,
    PLEASE_ENTER_A_VALID_PHONE_NUMBER,
    PASSWORDS_DO_NOT_MATCH_TITLE,
    OK_TITLE,
    ALERT_TITLE,
    SIGNED_UP_SUCCESSFULLY_TITLE,
    TERMS_OF_USE_NAVIGATION_KEY
} from '../../utilities'
import {
    InputField,
    PasswordField
} from '../../components'
import { CommonActions, useLinkProps } from '@react-navigation/native'
import {
    CheckBoxEmpty,
    CheckBoxSelected,
    BackArrow
} from '../../assets/svg_icons'
import { SafeAreaView } from 'react-native-safe-area-context'
import {SignUp} from 'aws-amplify-react-native'
import {Auth} from 'aws-amplify'
import Loader from '../../utilities/modules/Loader'
interface Props {
    navigation: any
}

interface Istate {
    tick: boolean
    fullName: string
    email: string
    phoneNumber: string
    newPassword: string
    retypePassword: string
    fullNameWarning: string
    emailWarning: string
    phoneNumberWarning: string
    newPasswordWarning: string
    retypePasswordWarning: string,
    loading:boolean
}

export default class CustomSignUp extends SignUp {

    constructor(props: any) {
        super(props)
        this.state = {
            tick: false,
            fullName: '',
            email: '',
            phoneNumber: '',
            newPassword: '',
            retypePassword: '',
            fullNameWarning: '',
            emailWarning: '',
            phoneNumberWarning: '',
            newPasswordWarning: '',
            retypePasswordWarning: '',
            loading:false
        }
    }

    validate = () => {
        let isValid = true
        if (isEmpty(this.state.fullName)) {
            isValid = false
            this.setState({ fullNameWarning: PLEASE_ENTER_A_NAME_TITLE })
        }
        if (!validateName(this.state.fullName)) {
            isValid = false
            this.setState({ fullNameWarning: PLEASE_ENTER_A_VALID_NAME_TITLE })
        }
        if (isEmpty(this.state.email)) {
            isValid = false
            this.setState({ emailWarning: PLEASE_ENTER_AN_EMAIL_ADDRESS })
        }
        if (!validateEmail(this.state.email)) {
            isValid = false
            this.setState({ emailWarning: PLEASE_ENTER_A_VALID_EMAIL_ADDRESS })
        }
        if (!validatePhoneNumber(this.state.phoneNumber)) {
            isValid = false
            this.setState({ phoneNumberWarning: PLEASE_ENTER_A_VALID_PHONE_NUMBER })
        }
        let passwordCheck = validatePassword(this.state.newPassword)
        if (passwordCheck.error) {
            isValid = false
            this.setState({ newPasswordWarning: passwordCheck.error })
        }
        passwordCheck = validatePassword(this.state.retypePassword)
        if (passwordCheck.error) {
            isValid = false
            this.setState({ retypePasswordWarning: passwordCheck.error })
        }
        if (this.state.newPassword != this.state.retypePassword) {
            isValid = false
            this.setState({ retypePasswordWarning: PASSWORDS_DO_NOT_MATCH_TITLE })
        }
        isValid && this.state.tick && this.signUp()
    }

    async signUp() {
        this.toggleLoader(true)
        await Auth.signUp({
            username:this.state.email,
            password: this.state.newPassword,
            attributes: {
                given_name:this.state.fullName,
                phone_number:this.state.phoneNumber
            }
        }).then(({user}) => {
            console.log('sign up user',user)
            this.toggleLoader(false)
            Alert.alert(
                ALERT_TITLE,
                SIGNED_UP_SUCCESSFULLY_TITLE,
                [
                    { text: OK_TITLE, onPress: () => 
                        Auth.resendSignUp(this.state.email).then(result=>{
                            this.props.onStateChange('confirmSignUp',this.state.email)
                        }).catch(e=>{
                            Alert.alert(ALERT_TITLE, e.message)    
                        })
                    }
                ]
            )
        }).catch(e=>{
            console.log('SignUp error',e)
            this.toggleLoader(false)
            Alert.alert(
                ALERT_TITLE,
                e.message,
            )
        });
        
    }

    navigateToHome() {
        // this.props.navigation.dispatch(
        //     CommonActions.reset({
        //         index: 0,
        //         routes: [{ name: MAIN_NAVIGATION_KEY }]
        //     })
        // )
    }

    handleFullNameChange = (fullName: string) =>
        this.setState({ fullName, fullNameWarning: '' },()=>{
            console.log('handleFullNameChange',this.state.fullName)
        })

    handleEmailChange = (email: string) =>
        this.setState({ email, emailWarning: '' })

    handlePhoneNumberChange = (phoneNumber: string) =>
        this.setState({ phoneNumber, phoneNumberWarning: '' })

    handleNewPasswordChange = (newPassword: string) =>
        this.setState({ newPassword, newPasswordWarning: '' })

    handleRetypePasswordChange = (retypePassword: string) =>
        this.setState({ retypePassword, retypePasswordWarning: '' })

    handleTickChange = () => {
        console.log('handleTickChange',this.state.tick)
        this.setState({ tick: !this.state.tick })
    }

    toggleLoader = (loading:boolean) =>{
        this.setState({
            loading
        })
    }

    showComponent(theme) {
        return <ScreenContainer>
            <Loader loading={this.state.loading}/>
            <KeyboardAvoidingView keyboardVerticalOffset={
                Platform.select({ android: normalize(-220), ios: normalize(24) })
            }
                behavior={Platform.OS === 'ios' ? 'padding' : 'position'}>
                <ScrollView showsVerticalScrollIndicator={false} >
                    <SafeAreaView>
                    <TouchableOpacity onPress={() => {this.props.onStateChange('signIn',{})}}
                        children={<BackArrow style={styles.backButton} />} />
                        <Logo style={styles.logo}
                            size='large' />
                    </SafeAreaView>
                    <View style={styles.mainContainer}>
                        <Label children={SIGN_UP_TITLE}
                            style={styles.title}
                            fontSize='extra_medium'
                            fontFamily='semiBold' />
                        <InputField value={this.state.fullName}
                            onChangeText={this.handleFullNameChange}
                            warning={this.state.fullNameWarning}
                            placeholder={FULL_NAME_TITLE + '*'} />
                        <InputField value={this.state.email}
                            onChangeText={this.handleEmailChange}
                            warning={this.state.emailWarning}
                            placeholder={EMAIL_TITLE + '*'} />
                        <InputField value={this.state.phoneNumber}
                            onChangeText={this.handlePhoneNumberChange}
                            warning={this.state.phoneNumberWarning}
                            placeholder={PHONE_NUMBER_TITLE} />
                        <PasswordField value={this.state.newPassword}
                            onChangeText={this.handleNewPasswordChange}
                            warning={this.state.newPasswordWarning}
                            placeholder={NEW_PASSWORD_TITLE + '*'} />
                        <PasswordField value={this.state.retypePassword}
                            onChangeText={this.handleRetypePasswordChange}
                            warning={this.state.retypePasswordWarning}
                            placeholder={RETYPE_NEW_PASSWORD_TITLE + '*'} />
                        <View style={styles.checkBoxContainer}>
                            <TouchableOpacity onPress={this.handleTickChange}
                                style={styles.checkBox}>
                                {this.state.tick ? <CheckBoxSelected /> : <CheckBoxEmpty />}
                            </TouchableOpacity>
                            <Label children={BY_REGISTERING_YOU_AGREE_WITH_THE_TITLE}
                                style={styles.lineHeight}
                                fontSize='small_medium'
                                color='GRAY' />
                            <TouchableOpacity
                                onPress={() => {} /*this.props.navigation.navigate(TERMS_OF_USE_NAVIGATION_KEY)*/} >
                                <Label children={TERMS_AMPERSAND_POLICY_TITLE}
                                    style={styles.termsText}
                                    numberOfLines={2}
                                    fontSize='small_medium' />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Button onPress={this.validate}
                        disabled={!this.state.tick}
                        style={styles.button}
                        title={SIGN_UP_TITLE} />
                </ScrollView>
            </KeyboardAvoidingView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    logo: {
        alignSelf: 'center',
        marginBottom: normalize(24),
        marginTop: -normalize(11)
    },
    backButton: {
        marginTop: normalize(11),
        marginStart: normalize(30)
    },
    mainContainer: {
        width: '84%',
        alignSelf: 'center'
    },
    title: {
        marginBottom: normalize(24)
    },
    checkBoxContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        marginTop: normalize(12),
        marginBottom: normalize(32)
    },
    checkBox: {
        marginEnd: normalize(8)
    },
    termsText: {
        textDecorationLine: 'underline',
        flex: 1,
        lineHeight: normalize(13)
    },
    button: {
        width: '84%'
    },
    lineHeight: {
        lineHeight: normalize(13)
    }
})