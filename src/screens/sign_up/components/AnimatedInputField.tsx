import React from 'react'
import {
    ReturnKeyTypeOptions,
    TextInput,
    View,
    KeyboardTypeOptions,
    StyleSheet
} from 'react-native'
import {
    InputStyle,
    normalize,
    Colors,
    FONTSIZE
} from '../../../utilities'
import Label from '../../../components/atomic/Label'
import { TextInputLayout } from '../../../components'

interface IAnimatedTextProps {
    value: string
    onChangeText?: (text: any) => void
    returnKeyType?: ReturnKeyTypeOptions
    keyboardType?: KeyboardTypeOptions
    placeholder?: string
    warning?: string
    onSubmit?: () => void
    reference?: (ref: any) => void
    disabled?: boolean
}

export default function (props: IAnimatedTextProps) {
    return <View style={styles.animatedContainer}>
        <TextInputLayout
            labelFontSize={normalize(16)}
            labelText={props.placeholder}
            hintColor={Colors.WHITE_ALPHA_50}
            focusColor={Colors.WHITE_ALPHA_50}>
            <TextInput
                editable={!props.disabled}
                ref={ref => ref && props.reference && props.reference(ref)}
                style={[
                    InputStyle.field, styles.text,
                    props.disabled && { color: Colors.GRAY },
                    { paddingEnd: props.warning && 24 }
                ]}
                placeholder={props.placeholder}
                value={props.value}
                returnKeyType={props.returnKeyType}
                underlineColorAndroid={'transparent'}
                autoCapitalize='none'
                onChangeText={props.onChangeText}
                onSubmitEditing={props.onSubmit}
                numberOfLines={1}
                keyboardType={props.keyboardType} />
        </TextInputLayout>
        {props.warning ?
            <Label children={props.warning}
                fontSize='small_medium'
                style={styles.warningText}
                color='BRIGHT_RED' /> : null}
    </View>
}

const styles = StyleSheet.create({
    animatedContainer: {
        flexDirection: 'row',
        height: normalize(65),
        width: normalize(315),
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Colors.WHITE_ALPHA_10,
        marginBottom: normalize(20)
    },
    warningText: {
        alignSelf: 'flex-end',
        bottom: '-25%',
        position: 'absolute'
    },
    text: {
        color: Colors.WHITE,
        fontSize: FONTSIZE.medium
    }
})