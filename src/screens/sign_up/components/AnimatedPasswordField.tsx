import React, { useState } from 'react'
import {
    View,
    TextInput,
    ReturnKeyTypeOptions,
    StyleSheet,
    KeyboardTypeOptions
} from 'react-native'
import {
    Colors,
    InputStyle,
    normalize,
    FONTSIZE
} from '../../../utilities'
import {
    EyeButton,
    Label,
    TextInputLayout
} from '../../../components'

interface IPasswordFieldProp {
    value: string
    onChangeText: (text: any) => void
    returnKeyType?: ReturnKeyTypeOptions
    keyboardType?: KeyboardTypeOptions
    placeholder?: string
    warning?: string
    onSubmit?: () => void
    reference?: (ref: any) => void
}

export default function AnimatedPasswordField(props: IPasswordFieldProp) {
    const [secure, toggle] = useState(true)
    return <View style={styles.animatedContainer}>
        <TextInputLayout
            hintColor={Colors.WHITE_ALPHA_50}
            focusColor={Colors.WHITE_ALPHA_50}>
            <TextInput
                ref={ref => ref && props.reference && props.reference(ref)}
                style={[InputStyle.field, styles.text]}
                value={props.value}
                secureTextEntry={secure}
                placeholder={props.placeholder}
                placeholderTextColor={Colors.WHITE_ALPHA_50}
                returnKeyType='next'
                autoCapitalize='none'
                keyboardType={props.keyboardType}
                autoCorrect={false}
                onChangeText={props.onChangeText}
                onSubmitEditing={props.onSubmit}
                underlineColorAndroid={'transparent'} />
        </TextInputLayout>
        <View style={styles.eyeButtonContainer}>
            <EyeButton isSecure={secure}
                onPress={() => toggle(!secure)} />
        </View>
        {props.warning && <Label children={props.warning}
            fontSize='small_medium'
            style={styles.warningText}
            color='BRIGHT_RED' />}
    </View>
}

const styles = StyleSheet.create({
    animatedContainer: {
        flexDirection: 'row',
        height: normalize(65),
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: Colors.WHITE_ALPHA_10,
        marginBottom: normalize(20)
    },
    warningText: {
        alignSelf: 'flex-end',
        bottom: '-25%',
        position: 'absolute'
    },
    text: {
        color: Colors.WHITE,
        fontSize: FONTSIZE.medium
    },
    eyeButtonContainer: {
        alignItems: 'center',
        top: '50%',
        position: 'absolute',
        end: 0
    }
})