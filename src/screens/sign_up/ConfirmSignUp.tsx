import React from 'react'
import { ConfirmSignUp } from 'aws-amplify-react-native'
import { Button, Label, ScreenContainer,CountDown } from '../../components'
import Loader from '../../utilities/modules/Loader'
import { Alert, KeyboardAvoidingView, Platform, TouchableOpacity } from 'react-native'
import { ALERT_TITLE, normalize, normalizeVertical, PLEASE_VERIFY_YOUR_EMAIL_ID, VERIFICATION_CODE_DESC, VERIFY,TIME_UP, WOULD_YOU_LIKE_TO_RESEND_CODE } from '../../utilities'
import OTPInputField from '../../components/atomic/OTPInputField'
import { StyleSheet, SafeAreaView } from 'react-native'
import { Auth } from 'aws-amplify'
import { Colors } from '../../utilities/modules/Colors';
import { BackArrow } from '../../assets/svg_icons'
export default class CustomConfirmSignUp extends ConfirmSignUp {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            code: '',
            timeout: 2, //sec
            restartCountDownToggle: true,
            isTimeout:false
        }
    }

    gotoSignIn = ()=>{
        this.props.onStateChange('signIn',{})
    }
    onTimeout = (flag)=>{
        this.setState({
            isTimeout:flag
        })
    }

    showComponent(theme) {
        return (
            <ScreenContainer>
                <Loader loading={this.state.loading} />
                <KeyboardAvoidingView style={styles.contentContainer}
                    keyboardVerticalOffset={Platform.OS === 'ios' ? normalize(105) : 0}
                    behavior={'padding'}>
                        <TouchableOpacity onPress={this.gotoSignIn}
                        children={<BackArrow/>} />
                    <Label children={this.state.isTimeout?TIME_UP:PLEASE_VERIFY_YOUR_EMAIL_ID}
                        style={styles.title}
                        fontFamily='semiBold'
                        numberOfLines={2}
                        fontSize='extra_medium' />
                    <Label
                        style={styles.description}
                        numberOfLines={3}
                        color='WHITE_ALPHA_50'
                        children={this.state.isTimeout? WOULD_YOU_LIKE_TO_RESEND_CODE:VERIFICATION_CODE_DESC} />
                    <Label
                        style={styles.description}
                        numberOfLines={1}
                        color='WHITE'
                        children={this.props.authData} />
                    <OTPInputField
                        style={styles.otpInputField}
                        value={this.state.code}
                        onValueChange={(value) => {
                            this.setState({
                                code: value
                            })
                        }} />
                    <CountDown
                    restartCountDown={this.state.restartCountDownToggle}
                    timeout={this.state.timeout}
                    onTimeout={this.onTimeout}
                    onResendClick={()=>{
                        
                        Auth.resendSignUp(this.props.authData).then(
                            result=>{
                                console.log("resendSignUp",result)
                                this.onTimeout(false)
                            }
                        ).catch(
                            e=>{
                                Alert.alert(ALERT_TITLE,e.message)
                            }
                        )
                    }}
                    />
                    <SafeAreaView style={styles.buttonContainer}>
                        <Button title={VERIFY}
                            onPress={() => {
                                //verify the otp
                                Auth.confirmSignUp(this.props.authData, this.state.code).then(this.gotoSignIn).catch(e => {
                                    Alert.alert(ALERT_TITLE, e.message)
                                })
                            }} />
                    </SafeAreaView>
                </KeyboardAvoidingView>
            </ScreenContainer>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flex: 1,
        width: '84%',
        alignSelf: 'center',
        paddingTop: normalizeVertical(42)
    },
    title: {
        marginBottom: normalize(31),
        marginTop:normalizeVertical(40)
    },
    spaceBetweenTextFields: {
        marginBottom: normalize(20)
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    otpInputField: {
        marginTop: normalizeVertical(30)
    },
    description: {
        lineHeight: normalize(20),
    },
})