import React from 'react'
import {
    StyleSheet,
    SafeAreaView
} from 'react-native'
import {
    Label,
    Button,
    ScreenContainer
} from '../../components'
import {
    FORGOT_PASSWORD_TITLE,
    normalize,
    OPEN_EMAIL_APP_TITLE,
    CHECK_EMAIL_DESCRIPTION,
    CHECK_YOUR_EMAIL_EXCLAMATION,
    FORGOT_PASSWORD_RESET_NAVIGATION_KEY, normalizeVertical
} from '../../utilities'
import ResetPassword from './ResetPassword'
import { openInbox } from "react-native-email-link";
interface Props {
    navigation: any
    route: any,
    email:string,
    onPasswordReset:()=>void
}

export default class CheckEmail extends React.Component<Props> {

    constructor(props: Props) {
        super(props)
        this.state = {
            resetPassword:false
        }
    }

    componentDidMount() {
        // this.props.navigation.setOptions({
        //     headerTitle: FORGOT_PASSWORD_TITLE,
        //     headerLeftContainerStyle: {
        //         marginStart: normalize(30)
        //     }
        // })
    }

    handleBackPress = () => {
        //this.props.navigation.pop()
    }

    next = () => {
        
        this.setState({
            resetPassword:true
        },()=>{
            openInbox().then(result=>{
                console.log("success",result)
            }).catch(e=>{
                console.log("openInbox",e)
            })    
        })
        //this.props.navigation.navigate(FORGOT_PASSWORD_RESET_NAVIGATION_KEY)
    }

    render() {
        if(this.state.resetPassword){
            return <ResetPassword {...this.props}/>
        }else{
            return <ScreenContainer style={styles.container}>
            <Label
                style={styles.title}
                fontFamily='semiBold'
                numberOfLines={1}
                fontSize='extra_medium'
                children={CHECK_YOUR_EMAIL_EXCLAMATION} />
            <Label
                style={styles.description}
                numberOfLines={3}
                color='WHITE_ALPHA_50'
                children={CHECK_EMAIL_DESCRIPTION} />
            <Label
                style={styles.description}
                fontFamily='medium'
                numberOfLines={3}
                color='WHITE_ALPHA_50'
                children={this.props.email} />
            <SafeAreaView style={styles.buttonContainer}>
                <Button title={OPEN_EMAIL_APP_TITLE}
                    onPress={this.next} />
            </SafeAreaView>
        </ScreenContainer>
        }
        
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: normalizeVertical(42)
    },
    title: {
        lineHeight: normalize(28),
        marginBottom: normalize(32),
    },
    description: {
        lineHeight: normalize(20)
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    }
})