import React from 'react'
import {
    StyleSheet,
    KeyboardAvoidingView,
    SafeAreaView,
    TouchableOpacity,
    View, Alert
} from 'react-native'
import {
    Label,
    Button,
    ScreenContainer
} from '../../components'
import {
    FORGOT_PASSWORD_TITLE,
    FORGOT_PASSWORD_DESCRIPTION,
    NEXT_TITLE,
    normalize,
    validateEmail,
    validatePhoneNumber,
    PLEASE_ENTER_AN_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE,
    PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE,
    FORGOT_PASSWORD_SCREEN_TITLE,
    FORGOT_PASSWORD_CHECK_EMAIL_NAVIGATION_KEY,
    EMAIL_TITLE, normalizeVertical
} from '../../utilities'

import {
    BackArrow
} from '../../assets/svg_icons'

import { InputField } from '../../components'

import { ForgotPassword, } from 'aws-amplify-react-native'
import { Auth } from 'aws-amplify'
import CheckEmail from './CheckEmail'
import Loader from '../../utilities/modules/Loader'

interface Props {
    navigation: any
}

interface Istate {
    email: string
    errorMessage: string,
    codeSent: boolean,
    loading: boolean
}

export default class CustomForgotPassword extends ForgotPassword<Props, Istate> {
    constructor(props: Props) {
        super(props);
        this.state = {
            email: '',
            errorMessage: '',
            codeSent: false,
            loading: false,
        }
    }

    componentDidMount() {
        // this.props.navigation.setOptions({
        //     headerTitle: FORGOT_PASSWORD_TITLE,
        //     headerLeftContainerStyle: {
        //         marginStart: normalize(30)
        //     }
        // })
        this.setState({
            codeSent: false
        })
    }

    handleBackPress = () => {
        //this.props.navigation.pop()
        this.setState({
            email: '',
            errorMessage: '',
            codeSent: false
        })
        this.props.onStateChange('signIn', {})
    }

    sendMail = async () => {
        // this.state.email.trim().length == 0 ?
        // this.setState({ errorMessage: PLEASE_ENTER_AN_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE })
        // : !(validateEmail(this.state.email) || validatePhoneNumber(this.state.email)) ?
        //     this.setState({ errorMessage: PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE })
        //     : this.props.navigation.navigate(
        //         FORGOT_PASSWORD_CHECK_EMAIL_NAVIGATION_KEY,
        //         { email: this.state.email }
        //   
        if (this.state.email.trim().length == 0) {
            this.setState({ errorMessage: PLEASE_ENTER_AN_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE })
        } else {
            this.setState({
                loading:true
            })
            
            await Auth.forgotPassword(this.state.email).then(result => {
                this.setState({
                    codeSent: true,
                    loading:false
                })
                console.log('forgotPassword', result)
                //this.props.onStateChange('signIn', {})
            }).catch(e => {
                console.log('forgotPassword error', e)
                this.setState({
                    codeSent: true,
                    loading:false
                })
            })
        }
    }

    onChangeText = (email: string) => this.setState({ email, errorMessage: '' })

    showComponent(theme) {
        console.log('this.state.codeSent', this.state.codeSent)
        return <ScreenContainer>
            <Loader loading={this.state.loading}/>
            <KeyboardAvoidingView style={styles.contentContainer}
            // keyboardVerticalOffset={normalize(85)}
            // behavior={'padding'}
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity onPress={this.handleBackPress}
                        children={<BackArrow />} />
                    <Label
                        style={{ textAlign: 'center' }}
                        fontFamily='semiBold'
                        numberOfLines={3}
                        fontSize='medium'
                        children={FORGOT_PASSWORD_TITLE} />
                    <BackArrow style={{ opacity: 0 }} />
                </View>
                {!this.state.codeSent ?
                    <>
                        <Label
                            style={styles.title}
                            fontFamily='semiBold'
                            numberOfLines={3}
                            fontSize='large'
                            children={FORGOT_PASSWORD_SCREEN_TITLE} />
                        <Label
                            style={styles.description}
                            numberOfLines={4}
                            color='WHITE_ALPHA_50'
                            children={FORGOT_PASSWORD_DESCRIPTION} />
                        <InputField value={this.state.email}
                            placeholder={EMAIL_TITLE}
                            onChangeText={this.onChangeText}
                            warning={this.state.errorMessage} />
                        <SafeAreaView style={styles.buttonContainer}>
                            <Button title={NEXT_TITLE}
                                onPress={this.sendMail} />
                        </SafeAreaView>
                    </>
                    :
                    <CheckEmail onPasswordReset={this.handleBackPress} email={this.state.email} />}

            </KeyboardAvoidingView>
        </ScreenContainer>


    }
}

const styles = StyleSheet.create({
    contentContainer: {
        flex: 1,
        width: '84%',
        alignSelf: 'center',
        paddingTop: normalize(24)
    },
    title: {
        lineHeight: normalize(28),
        marginBottom: normalize(20),
        marginTop: normalizeVertical(43)
    },
    description: {
        lineHeight: normalize(20),
        marginBottom: normalize(26)
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: normalize(16)
    }
})