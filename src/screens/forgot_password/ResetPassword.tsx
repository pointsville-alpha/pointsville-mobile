import React from 'react'
import {
    StyleSheet,
    KeyboardAvoidingView,
    SafeAreaView,
    Alert, Platform
} from 'react-native'
import {
    Label,
    Button,
    ScreenContainer, InputField
} from './../../components'
import {
    normalize,
    FORGOT_PASSWORD_TITLE,
    CHANGE_PASSWORD_TTTLE,
    PLEASE_ENTER_NEW_PASSWORD_TITLE,
    NEW_PASSWORD_TITLE,
    RETYPE_NEW_PASSWORD_TITLE,
    validatePassword,
    PASSWORDS_DO_NOT_MATCH_TITLE,
    PASSWORD_CHANGED_TITLE,
    OK_TITLE,
    ALERT_TITLE,
    isEmpty,
    PLEASE_RETYPE_NEW_PASSWORD_TITLE,
    CODE_TITLE, normalizeVertical
} from './../../utilities'
import { PasswordField } from '../../components'
import { Auth } from 'aws-amplify'
import Loader from '../../utilities/modules/Loader'

interface Props {
    navigation: any,
    email: string,
    onPasswordReset: () => void
}
interface Istate {
    newPassword: string
    retypePassword: string
    newPasswordWarning: string
    retypePasswordWarning: string,
    code: string,
    loading: boolean
}

export default class ResetPassword extends React.Component<Props, Istate> {
    constructor(props: Props) {
        super(props)
        this.state = {
            newPassword: '',
            retypePassword: '',
            newPasswordWarning: '',
            retypePasswordWarning: '',
            code: '',
            loading: false
        }
    }

    componentDidMount() {
        // this.props.navigation.setOptions({
        //     headerTitle: FORGOT_PASSWORD_TITLE,
        //     headerLeftContainerStyle: {
        //         marginStart: normalize(30)
        //     }
        // })
    }

    handleBackPress = () => {
        //this.props.navigation.goBack()
    }

    validatePasswords = () => {
        if (isEmpty(this.state.newPassword))
            this.setState({ newPasswordWarning: PLEASE_ENTER_NEW_PASSWORD_TITLE })
        if (isEmpty(this.state.retypePassword))
            this.setState({ retypePasswordWarning: PLEASE_RETYPE_NEW_PASSWORD_TITLE })
        if (isEmpty(this.state.newPasswordWarning) && isEmpty(this.state.retypePasswordWarning)) {
            if (this.state.newPassword != this.state.retypePassword)
                this.setState({ newPasswordWarning: PASSWORDS_DO_NOT_MATCH_TITLE })
            else {
                let passwordCheck = validatePassword(this.state.newPassword)
                if (passwordCheck.success) this.changePassword()
                else this.setState({ newPasswordWarning: passwordCheck.error! })
            }
        }
    }

    changePassword = () => {
        this.toggleLoader(true)
        Auth.forgotPasswordSubmit(this.props.email, this.state.code, this.state.newPassword).then(result => {
            this.toggleLoader(false)
            Alert.alert(
                ALERT_TITLE,
                PASSWORD_CHANGED_TITLE,
                [
                    {
                        text: OK_TITLE, onPress: () => {
                            //this.props.navigation.popToTop()
                            this.props.onPasswordReset()
                        }
                    }
                ]
            )
        }).catch(e => {
            this.toggleLoader(false)
            Alert.alert(
                ALERT_TITLE,
                e.message,
                [
                    {
                        text: OK_TITLE, onPress: () => {

                        }
                    }
                ]
            )
        })

    }
    handleCodeChange = (code: string) =>
        this.setState({ code })

    handleNewPasswordChange = (newPassword: string) =>
        this.setState({ newPassword, newPasswordWarning: '' })

    handleRetypePasswordChange = (retypePassword: string) =>
        this.setState({ retypePassword, retypePasswordWarning: '' })

    toggleLoader = (loading) => this.setState({
        loading
    })


    render() {
        return <ScreenContainer>
            <Loader loading={this.state.loading}/>
            <KeyboardAvoidingView style={styles.contentContainer}
                keyboardVerticalOffset={Platform.OS === 'ios' ? normalize(105) : 0}
                behavior={'padding'}>
                <Label children={PLEASE_ENTER_NEW_PASSWORD_TITLE}
                    style={styles.title}
                    fontFamily='semiBold'
                    numberOfLines={2}
                    fontSize='extra_medium' />
                <InputField placeholder={CODE_TITLE}
                    containerStyle={styles.spaceBetweenTextFields}
                    value={this.state.code}
                    onChangeText={this.handleCodeChange} />
                <PasswordField placeholder={NEW_PASSWORD_TITLE}
                    warning={this.state.newPasswordWarning}
                    containerStyle={styles.spaceBetweenTextFields}
                    value={this.state.newPassword}
                    onChangeText={this.handleNewPasswordChange} />
                <PasswordField placeholder={RETYPE_NEW_PASSWORD_TITLE}
                    warning={this.state.retypePasswordWarning}
                    value={this.state.retypePassword}
                    onChangeText={this.handleRetypePasswordChange} />
                <SafeAreaView style={styles.buttonContainer}>
                    <Button title={CHANGE_PASSWORD_TTTLE}
                        onPress={this.validatePasswords} />
                </SafeAreaView>
            </KeyboardAvoidingView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        flex: 1,
        paddingTop: normalizeVertical(42)
    },
    title: {
        marginBottom: normalize(31)
    },
    spaceBetweenTextFields: {
        marginBottom: normalize(20)
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    }
})