import React from 'react'
import {
    StyleSheet,
    SafeAreaView,
    View,
    Alert
} from 'react-native'
import { Redeem } from '../../assets/svg_icons'
import {
    Button,
    LighterBackground,
    ScreenContainer
} from '../../components'
import {
    normalize,
    OK_TITLE,
    REDEEM_TITLE
} from '../../utilities'
import InfoSection from '../transfer_points/components/InfoSection'
import PointsSection from '../transfer_points/components/PointsSection'

interface Props {
    navigation: any
    route: any
}

interface Istate {
    transferPoints: number
    walletID: string
}

export default class RedeemPoints extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            transferPoints: 50,
            walletID: ''
        }
    }

    redeem = () => {
        Alert.alert(
            'Success',
            'You have redeemed ' + this.state.transferPoints + ' points.',
            [
                { text: OK_TITLE, onPress: () => this.props.navigation.pop() }
            ],
            { cancelable: false }
        )
    }

    render() {
        return <ScreenContainer style={styles.container}>
            <LighterBackground style={styles.lighterBackground} />
            <SafeAreaView style={styles.main}>
                <InfoSection team={this.props.route.params.team}
                    points={this.props.route.params.points}
                    total={this.props.route.params.total}
                    price={this.props.route.params.price}
                    image={this.props.route.params.image} />
                <View style={styles.main}>
                    <Redeem style={styles.image} />
                    <PointsSection points={this.state.transferPoints}
                        setPoints={transferPoints => this.setState({ transferPoints })} />
                </View>
                <Button onPress={this.redeem}
                    title={REDEEM_TITLE} />
            </SafeAreaView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    lighterBackground: {
        position: 'absolute'
    },
    container: {
        paddingHorizontal: normalize(30)
    },
    main: {
        flex: 1,
        justifyContent: 'center'
    },
    image: {
        marginTop: normalize(28),
        marginBottom: normalize(40),
        alignSelf: 'center'
    }
})