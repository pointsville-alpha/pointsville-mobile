import React from 'react'
import {
    Image,
    KeyboardAvoidingView,
    StyleSheet,
    View
} from 'react-native'
import {
    ScrollView,
    TouchableOpacity
} from 'react-native-gesture-handler'
import { ProfileCamera } from '../../assets/svg_icons'
import {
    Label,
    ScreenContainer
} from '../../components'
import {
    Colors,
    EMAIL_TITLE,
    FIRST_NAME_TITLE,
    LAST_NAME_TITLE,
    normalize,
    PHONE_NUMBER_TITLE,
} from '../../utilities'
import AnimatedInputField from '../sign_up/components/AnimatedInputField'

interface Props {
    navigation: any
    route: any
}

interface Istate {
    firstName: string
    lastName: string
    phoneNumber: string
}

export default class EditProfile extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            firstName: this.props.route.params.name.split(' ')[0],
            lastName: this.props.route.params.name.split(' ')[1],
            phoneNumber: ''
        }
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerRight: () =>
                <TouchableOpacity
                    onPress={() => {
                        this.props.route.params.setData(this.state.firstName + ' ' + this.state.lastName)
                        this.props.navigation.pop()
                    }} >
                    <Label children='SAVE'
                        fontFamily='medium'
                        color='GRADIENT_END' />
                </TouchableOpacity>
        })
    }

    render() {
        return <ScreenContainer style={styles.container}>
            <KeyboardAvoidingView>
                <ScrollView>
                    <View style={styles.imageContainer} >
                        <Image source={require('../../assets/images/profile.png')}
                            style={styles.image} />
                        <View style={styles.camera}>
                            <TouchableOpacity children={<ProfileCamera />} />
                        </View>
                    </View>
                    <AnimatedInputField value={this.state.firstName}
                        onChangeText={firstName => this.setState({ firstName })}
                        placeholder={FIRST_NAME_TITLE} />
                    <AnimatedInputField value={this.state.lastName}
                        onChangeText={lastName => this.setState({ lastName })}
                        placeholder={LAST_NAME_TITLE} />
                    <AnimatedInputField value={this.state.phoneNumber}
                        onChangeText={phoneNumber => this.setState({ phoneNumber })}
                        placeholder={PHONE_NUMBER_TITLE} />
                    <AnimatedInputField value={this.props.route.params.email}
                        disabled
                        placeholder={EMAIL_TITLE} />
                </ScrollView>
            </KeyboardAvoidingView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(30),
        alignItems: 'center'
    },
    imageContainer: {
        marginVertical: normalize(25),
        width: normalize(100),
        height: normalize(100),
        alignSelf: 'center'
    },
    image: {
        width: normalize(100),
        height: normalize(100),
        borderRadius: normalize(50),
        backgroundColor: Colors.WHITE_ALPHA_20
    },
    camera: {
        position: 'absolute',
        top: 0,
        end: 0,
        width: normalize(28),
        height: normalize(28),
        borderRadius: normalize(14),
        backgroundColor: Colors.BACKGROUND,
        alignItems: 'flex-end',
        justifyContent: 'flex-start'
    }
})