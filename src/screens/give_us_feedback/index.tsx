import React from 'react'
import { ScrollView } from 'react-native-gesture-handler'
import { ScreenContainer } from '../../components'
import CloseButton from '../../components/atomic/CloseButton'
import { GIVE_US_FEEDBACK_TITLE } from '../../utilities'
import AnythingToImprove from './components/AnythingToImprove'
import Helpline from './components/Helpline'
import HowGood from './components/HowGood'

interface Props {
    navigation: any
}

interface Istate {
    text: string
    iconIndex: number
}

export default class GiveUsFeedback extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            iconIndex: -1,
            text: ''
        }
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerTitle: GIVE_US_FEEDBACK_TITLE,
            headerLeft: () => <CloseButton />
        })
    }

    setIconIndex = (iconIndex: number) => this.setState({
        iconIndex: iconIndex === this.state.iconIndex ? -1 : iconIndex
    })

    submit = () => { }

    render() {
        return <ScreenContainer>
            <ScrollView>
                <HowGood index={this.state.iconIndex}
                    onPress={this.setIconIndex} />
                <AnythingToImprove text={this.state.text}
                    onEdit={text => this.setState({ text })}
                    onSubmit={this.submit} />
            </ScrollView>
            <Helpline />
        </ScreenContainer>
    }
}