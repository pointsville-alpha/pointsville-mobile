import React from 'react'
import { StyleSheet } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {
    Happy,
    Laugh,
    Neutral,
    Unhappy
} from '../../../assets/svg_icons'
import {
    normalize,
    Colors
} from '../../../utilities'

interface IFeedbackIconProps {
    selectedIndex: number
    index: number
    onPress: () => void
    key?: string
}

const selectIcon = (index: number, selectedIndex: number) => {
    const color = selectColor(index, selectedIndex)
    switch (index) {
        case 0: return <Unhappy style={{ color }} />
        case 1: return <Neutral style={{ color }} />
        case 2: return <Happy style={{ color }} />
        case 3: return <Laugh style={{ color }} />
        default: return null
    }
}

const selectColor = (index: number, selectedIndex: number) =>
    selectedIndex === index ?
        index === 0 ? 'red' :
            index === 1 ? 'yellow' :
                index === 2 ? '#9F0' :
                    '#0F0' :
        Colors.WHITE_ALPHA_40

export default function FeedbackIcon(props: IFeedbackIconProps) {
    return <TouchableOpacity activeOpacity={1}
        onPress={props.onPress}
        style={styles.container}
        children={selectIcon(props.index, props.selectedIndex)}
    />
}

const styles = StyleSheet.create({
    container: {
        marginEnd: normalize(20)
    }
})