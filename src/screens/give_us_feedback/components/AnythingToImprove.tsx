import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import Label from '../../../components/atomic/Label'
import Button from '../../../components/atomic/Button'
import {
    TextInput,
    TouchableOpacity
} from 'react-native-gesture-handler'
import {
    Colors,
    FONTSIZE,
    normalize,
    ANYTHING_TO_BE_IMPROVED_QUESTION,
    SUBMIT_TITLE,
    YOUR_FEEDBACK_OPTIONAL_TITLE
} from '../../../utilities'

interface IAnythingToImproveProps {
    text: string
    onEdit: (text: string) => void
    onSubmit: () => void
}

export default function AnythingToImprove(props: IAnythingToImproveProps) {
    return <View style={styles.container}>
        <Label children={ANYTHING_TO_BE_IMPROVED_QUESTION}
            numberOfLines={2}
            fontSize='medium'
            style={styles.anythingToBeImproved}
            fontFamily='semiBold' />
        <TextInput style={styles.textInput} multiline scrollEnabled
            placeholder={YOUR_FEEDBACK_OPTIONAL_TITLE}
            placeholderTextColor={Colors.WHITE_ALPHA_50}
            value={props.text}
            onChangeText={props.onEdit}
        />
        <TouchableOpacity onPress={props.onSubmit}>
            <Button onPress={() => { }}
                title={SUBMIT_TITLE}
                style={styles.button} />
        </TouchableOpacity>
    </View>
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(16),
        paddingTop: normalize(21),
        backgroundColor: Colors.WHITE_ALPHA_5,
        marginBottom: normalize(16)
    },
    anythingToBeImproved: {
        lineHeight: normalize(28)
    },
    textInput: {
        marginTop: normalize(7),
        marginBottom: normalize(20),
        borderWidth: 1,
        borderRadius: normalize(6),
        borderColor: Colors.NEARLY_WHITE,
        height: normalize(175),
        textAlignVertical: 'top',
        padding: normalize(10),
        lineHeight: FONTSIZE.large,
        fontSize: FONTSIZE.regular,
        color: Colors.WHITE
    },
    button: {
        width: normalize(160),
        height: normalize(44),
        alignSelf: 'center',
        marginBottom: normalize(20)
    }
})