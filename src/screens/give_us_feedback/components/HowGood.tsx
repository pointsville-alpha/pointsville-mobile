import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import { Label } from '../../../components/index'
import FeedbackIcon from './FeedbackIcon'
import {
    normalize,
    RATE_YOUR_EXPERIENCE_TITLE,
    HOW_GOOD_IS_POINTSVILLE_QUESTION,
    FONTSIZE,
    Colors
} from '../../../utilities'

interface IHowGoodProps {
    index: number
    onPress: (index: number) => void
}

const HowGood = (props: IHowGoodProps) =>
    <View style={styles.container}>
        <Label children={HOW_GOOD_IS_POINTSVILLE_QUESTION}
            fontSize='medium'
            style={styles.howGood}
            fontFamily='semiBold' />
        <View style={styles.bottomHalf} >
            <View style={styles.horizontal}>
                {[0, 1, 2, 3].map(value =>
                    <FeedbackIcon index={value}
                        key={value + 'icon'}
                        selectedIndex={props.index ? props.index : 0}
                        onPress={() => props.onPress(value)} />
                )}
            </View>
            <Label children={RATE_YOUR_EXPERIENCE_TITLE}
                color='WHITE_ALPHA_50'
                textAlign='right'
                numberOfLines={3}
                style={styles.rateExperience} />
        </View>
    </View>

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(16),
        height: normalize(122),
        justifyContent: 'center',
        backgroundColor: Colors.WHITE_ALPHA_5,
        marginBottom: normalize(16)
    },
    bottomHalf: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: normalize(20),
        justifyContent: 'space-between'
    },
    horizontal: {
        flexDirection: 'row'
    },
    howGood: {
        lineHeight: normalize(28)
    },
    rateExperience: {
        lineHeight: FONTSIZE.large,
        flex: 1
    }
})

export default HowGood