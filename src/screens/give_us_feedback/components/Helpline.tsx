import React from 'react'
import {
    View,
    StyleSheet,
} from 'react-native'
import Label from '../../../components/atomic/Label'
import { SafeAreaView } from 'react-native-safe-area-context'
import {
    normalize,
    FONTSIZE,
    NEED_HELP_QUESTION,
    CONNECT_WITH_US_TITLE,
    HELPLINE_NUMBER
} from '../../../utilities'

export default function Helpline() {
    return <SafeAreaView style={styles.container}>
        <Label children={NEED_HELP_QUESTION}
            fontFamily='semiBold'
            fontSize='medium'
            style={styles.leftSideText} />
        <View style={styles.horizontal}>
            <Label children={CONNECT_WITH_US_TITLE}
                fontSize='medium'
                style={styles.leftSideText} />
            <Label children={HELPLINE_NUMBER}
                color='WHITE_ALPHA_50'
                style={styles.rightSideText} />
        </View>
    </SafeAreaView>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        bottom: normalize(6),
        width: '100%'
    },
    leftSideText: {
        lineHeight: FONTSIZE.very_large,
        paddingStart: normalize(16)
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    rightSideText: {
        lineHeight: FONTSIZE.large,
        paddingEnd: normalize(12)
    }
})