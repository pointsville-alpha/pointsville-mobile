import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import {
    Gradient,
    Label,
    ScreenContainer
} from '../../components'
import {
    normalize,
    Colors,
    HAVE_A_SIGN_UP_CODE_QUESTION_EARN_POINTS_EXCLAMATION,
    EARN_SIGNUP_POINTS_EXCLAMATION, CLAIM_POINTS_TITLE
} from '../../utilities'
import { Close } from '../../assets/svg_icons'
import {
    TouchableOpacity,
    ScrollView
} from 'react-native-gesture-handler'
import UploadButton from './components/UploadButton'
import OfferingsList from './components/OfferingsList'
import GamesList from './components/GamesList'
import NewsList from './components/NewsList'
import {
    HOME_TOKENS,
    TODAY_GAMES,
    UPCOMING_GAMES,
    NEWS
} from '../../placeholderdata/Home'
import Popup from './components/Popup'

import { useQuery } from '@apollo/client'
import { HOME_DATA } from '.././../utilities/modules/GraphQLAPI'
import Loader from '../../utilities/modules/Loader'
var parseString = require('react-native-xml2js').parseString;

interface Props {
    navigation: any
}

interface Istate {
    banner: boolean
    popup: boolean
}

export default function Home(props: Props) {

    const { loading, error, data } = useQuery(HOME_DATA);
    const [banner, setBanner] = useState(true)
    const [popup, setPopup] = useState(false)
    const [feeds, setFeeds] = useState([])
    const [feedsLoading, setFeedsLoading] = useState(false)
    const [games, setGames] = useState([
        {"gameData" : '{"gameDate":"1603291080000","match":"Final","gameUrl":"https://www.mlb.com/gameday/rays-vs-dodgers/2020/10/21/635896#game_state=preview,lock_state=preview,game_tab=,game=635896","TeamA": {"name":"Dodgers","img":"https://www.mlbstatic.com/team-logos/119.svg","price":"$1.75"},"TeamB": {"name":"Braves 3","img":"https://www.mlbstatic.com/team-logos/144.svg","price":"$1.50"}}' },
        {"gameData" : '{"gameDate":"1603377480000","match":"Final","gameUrl":"https://www.mlb.com/gameday/dodgers-vs-rays/2020/10/23/635923#game_state=preview,lock_state=preview,game_tab=,game=635923","TeamB": {"name":"Dodgers","img":"https://www.mlbstatic.com/team-logos/119.svg","price":"$1.90"},"TeamA": {"name":"Braves 3","img":"https://www.mlbstatic.com/team-logos/144.svg","price":"$1.40"}}'}
        ])

    useEffect(() => {
        const CORS_PROXY = "https://cors-anywhere.herokuapp.com/"
        const RSS_NEWS = "https://www.mlb.com/feeds/news/rss.xml"
        fetch(`${RSS_NEWS}`)
            .then((response) => response.text())
            .then((rss) => {
                parseString(rss, function (err, result) {
                    //console.log("result", JSON.stringify(result['rss']['channel'][0].item));
                    setFeeds(result['rss']['channel'][0].item);
                    // setFeedsLoading(false)
                });
                
            })
            .catch(e => {
                console.log("rss error", e);
            })

        return () => {
            //component will unmount
        }
    }, [data])

    if (loading) return <Loader visible={loading} />
    if (error) return <View></View>;

    return (
        <ScreenContainer>
            <ScrollView showsVerticalScrollIndicator={false} >
                {banner &&
                    <Gradient style={styles.banner}>
                        <Label children={HAVE_A_SIGN_UP_CODE_QUESTION_EARN_POINTS_EXCLAMATION}
                            color='WHITE_ALPHA_60'
                            fontSize='small' />
                        <View style={styles.bannerButtons}>
                            <UploadButton onPress={() => setPopup(true)} />
                            <TouchableOpacity onPress={() => setBanner(false)}
                                hitSlop={styles.hitSlop}
                                children={<Close />} />
                        </View>
                    </Gradient>}
                <OfferingsList tokens={data.orgs} />
                <GamesList 
                games={data.getAllGameSchedule}
                today={TODAY_GAMES}
                    upcoming={UPCOMING_GAMES} />
                <NewsList news={feeds} navigation={props.navigation}/>
            </ScrollView>
            <Popup title={EARN_SIGNUP_POINTS_EXCLAMATION}
                button={CLAIM_POINTS_TITLE}
                dismiss={() => setPopup(false)}
                visible={popup} />
        </ScreenContainer>
    )
}

const styles = StyleSheet.create({
    banner: {
        width: '100%',
        height: normalize(50),
        backgroundColor: Colors.BACKGROUND_DEEP,
        paddingHorizontal: normalize(14),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: normalize(1),
        borderRadius: 0
    },
    bannerButtons: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    hitSlop: {
        top: 10,
        bottom: 10,
        left: 10,
        right: 10
    }
})