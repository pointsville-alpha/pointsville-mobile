import React from 'react'
import {
    Image,
    StyleSheet,
    View
} from 'react-native'
import { IOfferingProps } from '../offerings/Offering'
import { normalize } from '../../../../utilities'
import { Label } from '../../../../components'
import { imageSelect } from '../../../../assets/ImageSelect'
import SvgUri from 'react-native-svg-uri'

interface ITeamTokenProps {
    team: IOfferingProps
}

export default function TeamToken(props: ITeamTokenProps) {
    return props.team ?
        <View style={styles.container}>
            <SvgUri style={styles.image} height={normalize(40)} width={normalize(40)} source={{uri:props.team.img}}/>
            <Label children={props.team.name}
                fontSize='small'
                fontFamily='medium' />
            <Label children={props.team.price}
                color='WHITE_ALPHA_60' />
        </View>
        : null
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    image: {
        width: normalize(40),
        height: normalize(40),
        marginBottom: normalize(5)
    }
})