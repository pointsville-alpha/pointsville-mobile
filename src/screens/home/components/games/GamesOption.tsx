import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import {
    normalize,
    Colors
} from '../../../../utilities'
import { Label } from '../../../../components'

interface IGamesOptionProps {
    title: string
    onPress: () => void
    isSelected: boolean
}

export default function GamesOption(props: IGamesOptionProps) {
    return <View style={styles.option}>
        <Label children={props.title}
            onPress={props.onPress}
            color={props.isSelected ? 'WHITE' : 'WHITE_ALPHA_50'}
            fontSize='small'
            fontFamily='medium' />
        {props.isSelected && <View style={styles.optionHighlight} />}
    </View>
}

const styles = StyleSheet.create({
    option: {
        marginEnd: normalize(20)
    },
    optionHighlight: {
        height: 2,
        borderRadius: 1,
        width: '100%',
        marginTop: normalize(5),
        backgroundColor: Colors.WHITE
    },
})