import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import {
    normalize,
    Colors,
    REDEEM_TITLE_UPPERCASE,
    FONTSIZE,
    REDEEM_POINTS_NAVIGATION_KEY
} from '../../../../utilities'
import { IOfferingProps } from '../offerings/Offering'
import TeamToken from './TeamToken'
import {
    Gradient,
    Label
} from '../../../../components'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

interface IGameTileProps {
    homeTeam: IOfferingProps
    awayTeam: IOfferingProps
    event: string
    date: string
    time: string
}

export default function GameTile(props: IGameTileProps) {
    const navigation = useNavigation()
    const redeem = () => navigation.navigate(REDEEM_POINTS_NAVIGATION_KEY,
        {
            image: props.homeTeam.img,
            team: props.homeTeam.name,
            points: 200,
            price: props.homeTeam.price,
            total: '$320'
        })

    return <View style={styles.container} >
        <TeamToken team={props.homeTeam} />
        <View style={styles.middleContainer}>
            <Label children={` - ${props.event} - `}
                style={styles.title}
                fontSize='small'
                fontFamily='medium' />
            <Label children={props.date}
                fontSize='small_medium'
                fontFamily='medium' />
            <Label children={props.time}
                fontSize='small_medium'
                fontFamily='medium' />
            <TouchableOpacity onPress={redeem}>
                <Gradient style={styles.redeem}>
                    <Label children={REDEEM_TITLE_UPPERCASE}
                        style={styles.redeemText}
                        fontFamily='medium'
                        fontSize='small_tiny' />
                </Gradient>
            </TouchableOpacity>
        </View>
        <TeamToken team={props.awayTeam} />
    </View>
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        aspectRatio: 2.5,
        borderRadius: normalize(6),
        backgroundColor: Colors.WHITE_ALPHA_5,
        shadowOpacity: 0.1,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: normalize(30),
        shadowColor: 'black',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    middleContainer: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        marginBottom: normalize(5)
    },
    redeem: {
        marginTop: normalize(10),
        width: normalize(62),
        height: normalize(24),
        borderRadius: normalize(20),
        alignItems: 'center',
        justifyContent: 'center'
    },
    redeemText: {
        lineHeight: FONTSIZE.small
    }
})