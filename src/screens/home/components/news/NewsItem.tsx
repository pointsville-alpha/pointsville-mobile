import React from 'react'
import {
    StyleSheet,
    View,
    Image
} from 'react-native'
import Label from '../../../../components/atomic/Label'
import {
    normalize,
    Colors,
    VIEW_FULL_STORY_TITLE
} from '../../../../utilities'
import { TouchableOpacity } from 'react-native-gesture-handler'

export interface INewsItemProps {
    image: any
    title: string
    description: string
    key?: string,
    onClick:()=>void
}

export default function NewsItem(props: INewsItemProps) {
    return <View style={styles.container}>
        <Image style={styles.image}
            source={props.image} />
        <View style={styles.textContainer}>
            <Label children={props.title}
                style={styles.title}
                fontSize='small'
                fontFamily='medium' />
            <Label children={props.description}
                style={styles.description}
                fontSize='small_medium'
                color='WHITE_ALPHA_70'
                numberOfLines={3} />
            <TouchableOpacity onPress={props.onClick}>
                <Label children={VIEW_FULL_STORY_TITLE}
                    fontSize='small_medium'
                    fontFamily='medium' />
            </TouchableOpacity>
        </View>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: normalize(8),
        marginBottom: normalize(5)
    },
    image: {
        marginEnd: normalize(6),
        height: normalize(84),
        width: normalize(84),
        borderRadius: normalize(4),
        backgroundColor: Colors.WHITE_ALPHA_5
    },
    textContainer: {
        width: normalize(257)
    },
    title: {
        marginBottom: normalize(6)
    },
    description: {
        marginBottom: normalize(11)
    }
})