import React, { Component, useState } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';
import WebView from 'react-native-webview';
import { Colors} from '../../../../utilities';
import Loader from '../../../../utilities/modules/Loader';


const NewsDetail = props => {
    
    const [loading, setLoading] = useState(false)

    return (

        <View style={styles.background}>
            <Loader loading={loading}/>
            <WebView
                javaScriptEnabled={true}
                domStorageEnabled={true}
                startInLoadingState={true}
                scalesPageToFit={true}
                onLoadStart={() => setLoading(true)}
                onLoad={() => { setLoading(false) }}
                source={{ uri: props.route.params.url }}
            />

        </View>
    )
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
    },
});

export default NewsDetail;