import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import {
    Colors,
    FONTSIZE,
    normalize,
    OFFERINGS_TITLE,
    SEE_MORE_TITLE
} from '../../../utilities'
import {
    FlatList,
    TouchableOpacity
} from 'react-native-gesture-handler'
import Offering, { IOfferingProps } from './offerings/Offering'
import { Label } from '../../../components'

interface IOfferingsListProps {
    tokens: IOfferingProps[]
}

export default function OfferingsList(props: IOfferingsListProps) {
    return <View>
        <Label children={OFFERINGS_TITLE}
            style={styles.title}
            fontSize='regular_medium'
            fontFamily='medium' />
        <FlatList horizontal
            style={styles.list}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
            keyExtractor={(_, index) => 'Token_' + index}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }: any) =>
                <Offering name={item.name}
                    price={item.rate}
                    image={item.logoUrl} />
            }
            ListFooterComponent={() =>
                <TouchableOpacity style={styles.footer}>
                    <Label children={SEE_MORE_TITLE} />
                </TouchableOpacity>}
            data={props.tokens.slice(0, 6)} />
    </View>
}

const styles = StyleSheet.create({
    list: {
        flex: 1,
        marginTop: normalize(13),
        paddingHorizontal: normalize(14)
    },
    separator: {
        width: normalize(8)
    },
    title: {
        marginStart: normalize(14),
        marginTop: normalize(20),
        lineHeight: FONTSIZE.extra_medium
    },
    footer: {
        marginStart: normalize(8),
        marginEnd: normalize(28),
        borderRadius: normalize(6),
        width: normalize(104),
        aspectRatio: 1.25,
        marginTop: normalize(20.8),
        backgroundColor: Colors.WHITE_ALPHA_5,
        alignItems: 'center',
        justifyContent: 'center'
    }
})