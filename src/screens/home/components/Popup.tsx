import { useMutation } from '@apollo/client'
import React, { useState } from 'react'
import {
    View,
    StyleSheet, Alert
} from 'react-native'
import {
    TextInput,
    TouchableOpacity
} from 'react-native-gesture-handler'
import {
    Close,
    LogoWatermark
} from '../../../assets/svg_icons'
import {
    Gradient,
    Label
} from '../../../components'
import {
    ENTER_THE_CODE_FROM_THE_EMAIL_SENT_TITLE,
    normalize,
    Colors,
    FONTSIZE,
    EARN_SIGN_UP_POINTS_PLACEHOLDER,
    FONT, ALERT_TITLE,
    INVALID_PROMO_CODE,
    PROMO_CODE_AVAILED,
    OK_TITLE
} from '../../../utilities'
import { AVAIL_PROMO } from '../../../utilities/modules/GraphQLAPI'

interface IPopupProps {
    visible: boolean
    dismiss: () => void
    title: string
    button: string
}

export default function Popup(props: IPopupProps) {
    const [text, setText] = useState('')
    const [availPromo] = useMutation(AVAIL_PROMO)

    return props.visible ?
        <View style={styles.background}>
            <Gradient style={styles.container}>
                <Label children={props.title}
                    style={styles.title} />
                <Label children={ENTER_THE_CODE_FROM_THE_EMAIL_SENT_TITLE}
                    fontSize='small_medium'
                    style={styles.description} />
                <TextInput value={text}
                    maxLength={8}
                    selectionColor={Colors.WHITE}
                    onChangeText={value => setText(value.toString().trim())}
                    placeholderTextColor={Colors.WHITE_ALPHA_50}
                    placeholder={EARN_SIGN_UP_POINTS_PLACEHOLDER}
                    style={styles.input} />
                <TouchableOpacity activeOpacity={text.length > 0 ? 0.6 : 1}
                    onPress={() => {
                        if(text.length > 0){
                            availPromo({variables:{promoCode:text}}).then(
                                result=>{
                                    console.log("result",result)
                                    if(result.data.availPromo){
                                        Alert.alert(ALERT_TITLE,PROMO_CODE_AVAILED,
                                            [
                                                {
                                                    text:OK_TITLE,
                                                    onPress:props.dismiss()
                                                }
                                            ])
                                    }else{
                                        Alert.alert(ALERT_TITLE,INVALID_PROMO_CODE)
                                        //props.dismiss()
                                    }
                                }
                            ).catch(
                                e=>{
                                    console.log("avail promo error",e)
                                    Alert.alert(ALERT_TITLE,e.message)
                                    //props.dismiss()
                                }
                            )
                        }
                    }}
                    style={[
                        styles.claimPointsContainer, text.length == 0 && styles.disabledButton]}>
                    <Label children={props.button}
                        style={styles.claimPoints}
                        color={text.length > 0 ? 'WHITE' : 'WHITE_ALPHA_20'}
                        fontFamily='semiBold' />
                </TouchableOpacity>
                <LogoWatermark style={styles.watermark} />
                <View style={styles.close}>
                    <TouchableOpacity onPress={props.dismiss}>
                        <Close />
                    </TouchableOpacity>
                </View>
            </Gradient>
        </View> : null
}

const styles = StyleSheet.create({
    background: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundColor: Colors.BLACK_ALPHA_60,
        justifyContent: 'center'
    },
    container: {
        opacity: 1,
        minWidth: normalize(287),
        minHeight: normalize(230),
        borderRadius: normalize(6),
        padding: normalize(20),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    title: {
        marginBottom: normalize(6)
    },
    description: {
        marginBottom: normalize(20)
    },
    input: {
        marginBottom: normalize(30),
        width: normalize(161),
        height: normalize(60),
        borderWidth: 1,
        borderStyle: 'dashed',
        borderRadius: normalize(6),
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: FONTSIZE.extra_medium,
        lineHeight: FONTSIZE.extra_large,
        color: Colors.WHITE,
        backgroundColor: Colors.WHITE_ALPHA_20,
        borderColor: Colors.WHITE_ALPHA_50,
        fontFamily: FONT.medium
    },
    claimPointsContainer: {
        width: normalize(224),
        height: normalize(44),
        borderRadius: normalize(6),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.BACKGROUND_ALPHA_50
    },
    claimPoints: {
        lineHeight: FONTSIZE.large
    },
    watermark: {
        position: 'absolute',
        bottom: 0,
        end: 0
    },
    close: {
        position: 'absolute',
        top: normalize(15),
        end: normalize(15)
    },
    disabledButton: {
        backgroundColor: Colors.WHITE_ALPHA_10
    }
})