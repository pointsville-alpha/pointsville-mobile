import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { StyleSheet } from 'react-native'
import {
    normalize,
    Colors,
    AVAIL_NOW_TITLE
} from '../../../utilities'
import Label from '../../../components/atomic/Label'

interface IUploadButtonProps {
    onPress: () => void
}

export default function UploadButton(props: IUploadButtonProps) {
    return <TouchableOpacity onPress={props.onPress}
        style={styles.container}>
        <Label children={AVAIL_NOW_TITLE}
            fontFamily='medium'
            fontSize='small_medium' />
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    container: {
        paddingTop: normalize(7.25),
        paddingBottom: normalize(6.25),
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: Colors.WHITE_ALPHA_50,
        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: normalize(15),
        width: normalize(92)
    }
})