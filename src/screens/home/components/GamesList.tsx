import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import {
    normalize,
    GAMES_TITLE,
    TODAY_TITLE,
    UPCOMING_TITLE,
    FONTSIZE
} from '../../../utilities'
import { FlatList } from 'react-native-gesture-handler'
import { Label } from '../../../components'
import GamesOption from './games/GamesOption'
import GameTile from './games/GameTile'
import moment from 'moment'

interface IGamesListProps {
    today: any[]
    upcoming: any[],
    games: any[]
}

export default function GamesList(props: IGamesListProps) {
    const [isToday, toggle] = useState(false)
    const [today, setTodayGames] = useState([])
    const [upcoming, setUpcomingGames] = useState([])
    
    const getGameObject = (gameData: any, date: any) => {
        return {
            date: moment(date).format('DD MMM'),
            time: moment(date).format('HH:MM'),
            home: gameData.TeamA,
            away: gameData.TeamB,
            event: gameData.match,
            gameUrl: gameData.gameUrl
        }
    }
    useEffect(() => {
        let todayArray = [];
        let upcomingArray = [];
        props.games.map(item => {
            let gameData = JSON.parse(item.game_data)
            console.log("", gameData)
            let date = new Date(Number(gameData.gameDate))
            if (moment(date).isSame(new Date(), 'day')) {
                todayArray.push(getGameObject(gameData, date))
            } else {
                upcomingArray.push(getGameObject(gameData, date))
            }

        })
        if (todayArray.length > 0) {
            toggle(true)
        }
        setTodayGames(todayArray)
        setUpcomingGames(upcomingArray)

        return (() => {
            //component will unmount
        })
    }, [props.games])


    return <View>
        <View style={styles.textContainer}>
            <Label children={GAMES_TITLE}
                fontFamily='medium'
                style={styles.title}
                fontSize='regular_medium' />
            {today.length > 0 && upcoming.length > 0 &&
                <View style={styles.optionsContainer}>
                    <GamesOption title={TODAY_TITLE}
                        onPress={() => toggle(!isToday)}
                        isSelected={isToday} />
                    <GamesOption title={UPCOMING_TITLE}
                        onPress={() => toggle(!isToday)}
                        isSelected={!isToday} />
                </View>}
        </View>
        <FlatList horizontal
            style={styles.list}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
            keyExtractor={(_, index) => 'Token_' + index}
            showsHorizontalScrollIndicator={false}
            renderItem={({ item }: any) =>
                <GameTile date={item.date}
                    time={item.time}
                    homeTeam={item.home}
                    awayTeam={item.away}
                    event={item.event} />
            }
            data={isToday ? today : upcoming} />
    </View>
}

const styles = StyleSheet.create({
    textContainer: {
        marginTop: normalize(30),
        marginHorizontal: normalize(14)
    },
    optionsContainer: {
        flexDirection: 'row',
        marginTop: normalize(9)
    },
    list: {
        marginTop: normalize(17),
        paddingHorizontal: normalize(14),
        height: normalize(120)
    },
    separator: {
        width: normalize(10)
    },
    title: {
        lineHeight: FONTSIZE.extra_medium
    }
})