import React, { useState } from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import {
    normalize,
    NEWS_TITLE,
    FONTSIZE, NEWS_DETAIL_NAVIGATION_KEY
} from '../../../utilities'
import { Label } from '../../../components'
import NewsItem from './news/NewsItem'
import NewsDetail from './news/NewsDetail'

interface INewsListProps {
    news: any[],
    navigation:any
}

export default function NewsList(props: INewsListProps) {

    return <View style={styles.container}>
        <Label children={NEWS_TITLE}
            fontSize='regular_medium'
            style={styles.title}
            fontFamily='medium' />
        {props.news.map((item, index) => {
            return <NewsItem key={index + '_news'}
                description={item.description}
                title={item.title}
                image={item.image}
                onClick={() => {
                    props.navigation.navigate(NEWS_DETAIL_NAVIGATION_KEY,{url:item.link[0]})
                }} />
        })}
    </View>
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginTop: normalize(30),
        paddingHorizontal: normalize(14)
    },
    separator: {
        height: normalize(5)
    },
    title: {
        lineHeight: FONTSIZE.extra_medium
    }
})