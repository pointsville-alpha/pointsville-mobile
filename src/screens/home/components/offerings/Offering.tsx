import { useNavigation } from '@react-navigation/native'
import React from 'react'
import {
    Image,
    StyleSheet,
    View
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { imageSelect } from '../../../../assets/ImageSelect'
import Label from '../../../../components/atomic/Label'
import {
    normalize,
    Colors,
    PROMO_NAVIGATION_KEY
} from '../../../../utilities'

export interface IOfferingProps {
    name: string
    price: string
    image: string
}

export default function Offering(props: IOfferingProps) {
    const navigation = useNavigation()
    return <TouchableOpacity style={styles.container}
        onPress={() => navigation.navigate(
            PROMO_NAVIGATION_KEY,
            {
                team: props.name,
                image: props.image,
                price: props.price
            }
        )}>
        <View style={styles.translucency} />
        <View style={styles.subContainer}>
            <Image style={styles.image} source={{ uri: props.image }} resizeMode={'contain'} />
            <Label children={props.name}
                fontFamily='medium'
                color='WHITE_ALPHA_60'
                fontSize='small' />
            <Label children={`$${props.price}`} />
        </View>
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    container: {
        width: normalize(104),
        aspectRatio: 1
    },
    translucency: {
        width: '100%',
        height: '80%',
        borderRadius: normalize(6),
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.WHITE_ALPHA_5
    },
    subContainer: {
        position: 'absolute',
        alignSelf: 'center',
        height: normalize(80),
        alignItems: 'center'
    },
    image: {
        height: normalize(50),
        width: normalize(50),
        marginBottom: normalize(5),
        justifyContent: 'center',
        alignItems: 'center'
    }
})