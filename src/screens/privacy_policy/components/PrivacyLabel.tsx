import React from 'react'
import { FlexStyle } from 'react-native'
import { Label } from '../../../components/index'
import { FONTSIZE } from '../../../utilities/modules/Fonts'
import { normalize } from '../../../utilities/modules/Measurement'

interface IPrivacyLabelProps {
    children: string,
    isTitle?: boolean,
    style?: FlexStyle
}

export default function PrivacyLabel(props: IPrivacyLabelProps) {
    return <Label children={props.children}
        style={{
            lineHeight: FONTSIZE.large,
            marginTop: props.isTitle ? normalize(30) : 0,
            marginBottom: props.isTitle ? normalize(10) : 0,
            ...props.style
        }}
        numberOfLines={1000}
        color='WHITE_ALPHA_50'
        fontFamily={props.isTitle ? 'semiBold' : 'regular'}
        fontSize={props.isTitle ? 'medium' : 'regular'} />
}