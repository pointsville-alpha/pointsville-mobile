import React from 'react'
import PrivacyLabel from './PrivacyLabel'
import {
    View,
    StyleSheet,
    Text,
    Platform
} from 'react-native'
import { FONTSIZE } from '../../../utilities/modules/Fonts'
import { Colors } from '../../../utilities/modules/Colors'
import { normalize } from '../../../utilities/modules/Measurement'

interface IListItemProps {
    text: string
}

export default function ListItem(props: IListItemProps) {
    return <View>
        <PrivacyLabel children={'   ' + props.text} />
        <Text children={'\u25CB'}
            maxFontSizeMultiplier={1.5}
            style={styles.circle} />
    </View>
}

const styles = StyleSheet.create({
    circle: {
        position: 'absolute',
        lineHeight: FONTSIZE.large,
        color: Colors.GRAY,
        fontSize: Platform.select({ ios: normalize(7), android: normalize(10) })
    }
})