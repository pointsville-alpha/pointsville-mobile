import React from 'react'
import { StyleSheet } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { ScreenContainer } from '../../components'
import PrivacyLabel from './components/PrivacyLabel'
import ListItem from './components/ListItem'
import { normalize } from '../../utilities'
import CloseButton from '../../components/atomic/CloseButton'

interface Props {
    navigation: any
}

export default class PrivacyPolicy extends React.Component<Props> {

    constructor(props: Props) {
        super(props)
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerLeft: () => <CloseButton />
        })
    }

    render() {
        return <ScreenContainer>
            <ScrollView contentContainerStyle={styles.container}>
                <PrivacyLabel children={PRIVACY_EFFECTIVE_DATE} />
                <PrivacyLabel children={PRIVACY_INTRODUCTION} />
                <PrivacyLabel children={INFORMATION_COLLECTION_AND_USE_TITLE} isTitle />
                <PrivacyLabel children={INFORMATION_COLLECTION_DESCRIPTION} />
                <PrivacyLabel children={PERSONALLY_IDENTIFIABLE_INFORMATION_TITLE} isTitle />
                <PrivacyLabel children={PERSONALLY_IDENTIFIABLE_INFORMATION_DESCRIPTION} />
                {PERSONALLY_IDENTIFIABLE_INFORMATION_COLLECTED.map(value => {
                    return <ListItem text={value} />
                })}
                <PrivacyLabel children={PERSONALLY_IDENTIFIABLE_INFORMATION_USES} />
            </ScrollView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(16),
        paddingBottom: normalize(30)
    }
})

const PRIVACY_EFFECTIVE_DATE = '\nEffective as of: April 15, 2020\n'
const PRIVACY_INTRODUCTION = 'POINTSVILLE VALUES THE PROTECTION OF INDIVIDUAL PRIVACY AND IS COMMITTED TO THE PRIVACY OF THOSE WHO VISIT AND USE OUR WEBSITES. THIS DOCUMENT SETS FORTH THE PRIVACY POLICY (“PRIVACY POLICY”) FOR POINTSVILLE (“WE”, “US” OR “OUR”) WEBSITES (COLLECTIVELY, “SITE”). FOR PURPOSES OF THIS PRIVACY POLICY ONLY, REFERENCES TO “POINTSVILLE”, “WE”, “US” AND “OUR” INCLUDE TYPEONENATION.ORG (“T1N”). WHERE APPLICABLE, THIS PRIVACY POLICY INCLUDES THE CHILD PRIVACY POLICY ADDENDUM. THIS PRIVACY POLICY GOVERNS THE MANNER IN WHICH JDRF COLLECTS, USES, MAINTAINS, DISCLOSES AND SECURES INFORMATION COLLECTED FROM USERS OF THE SITE. YOU SHOULD READ THIS PRIVACY POLICY. EXCEPT FOR THE LIMITED SITUATIONS MENTIONED BELOW, JDRF DOES NOT SHARE OR SELL DONOR INFORMATION COLLECTED ONLINE.'
const INFORMATION_COLLECTION_AND_USE_TITLE = 'I. INFORMATION COLLECTION AND USE'
const INFORMATION_COLLECTION_DESCRIPTION = 'Each time you visit the Site, we collect information about you based on your consent.'
const PERSONALLY_IDENTIFIABLE_INFORMATION_TITLE = 'A.  Personally Identifiable Information'
const PERSONALLY_IDENTIFIABLE_INFORMATION_DESCRIPTION = 'We collect personally identifiable information about you when you visit the Site. During your interactions with the Site, when you register with the Site or make a donation or agree to volunteer, we collect the following types of information:\n'
const PERSONALLY_IDENTIFIABLE_INFORMATION_USES = `We use this information, upon the following legal bases, to:

validate Site users based on our legitimate interest in sharing information about POINTVILLE and soliciting support,
to facilitate participation in various POINTVILLE services such as fundraising and community forums as offered through the Site based on our legitimate interest in further the mission of POINTVILLE,
Lorem Ipsum is simply dummy text of the printing and typesetting industry. 

Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 

It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 

It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.`
const PERSONALLY_IDENTIFIABLE_INFORMATION_COLLECTED = [
    'your name,',
    'date of birth,',
    'postal address,',
    'email address,',
    'telephone number,',
    'credit card information,',
    'your preferences for receiving additional information such as newsletters,',
    'IP Address,internet service provider,browser,',
    'operating system,',
    'device type,',
    ' screen size,',
    ' location (country, region, state, city),',
    ' language preference,',
    'webpages visited,',
    'time spent on the website,',
    'clicks,',
    'referring website.'
]