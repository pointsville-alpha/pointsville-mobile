import React, { useState } from 'react'
import { View } from 'react-native'
import { ScreenContainer } from '../../components'
import { ADD_TO_WALLET, PROMO_CODE_EXCLAMATION } from '../../utilities'
import WalletCard from './components/WalletCard'
import {
    CARD,
    MY_POINTS,
    TRANSACTIONS
} from '../../placeholderdata/Wallet'
import WalletList from './components/WalletList'
import Popup from '../home/components/Popup'
import { useQuery } from '@apollo/client'
import { TRANSACTION_BY_USER_DETAILS, WALLET_DATA } from '../../utilities/modules/GraphQLAPI'
import Loader from '../../utilities/modules/Loader'

interface Props {
    navigation: any
}

interface Istate {
    popup: boolean
}

export default function Wallet(props: Props) {

    const [popup, setPopup] = useState(false)
    const { loading, error, data } = useQuery(WALLET_DATA);

    if (loading) return <Loader visible={loading} />

    if (error) return <View />
    

    return <ScreenContainer>
        <WalletCard openPopup={() => setPopup(true)}
            info={data.walletSummary} />
        <WalletList myPoints={data.getTransactionsGroupByOrg}
            transactions={data.getTransactionsByUser} />
        <Popup title={PROMO_CODE_EXCLAMATION}
            button={ADD_TO_WALLET}
            dismiss={() => setPopup(false)}
            visible={popup} />
    </ScreenContainer>

}