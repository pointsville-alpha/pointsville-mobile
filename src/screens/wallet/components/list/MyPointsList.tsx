import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import {
    normalize,
    Colors,
    TRANSFER_TITLE,
    TRANSFER_POINTS_NAVIGATION_KEY,
    REDEEM_POINTS_NAVIGATION_KEY,
    REDEEM_TITLE_UPPERCASE
} from '../../../../utilities'
import PointsItem from './PointsItem'
import { SwipeListView } from 'react-native-swipe-list-view'
import { Label } from '../../../../components'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

interface IMyPointsListProps {
    data: any
}

export default function MyPointsList({ data }: IMyPointsListProps) {
    const navigation = useNavigation()

    const navigate = (item: any, key: string) => {
        navigation.navigate(key,
            {
                image: item.image,
                team: item.team,
                points: item.points,
                price: item.price,
                total: item.total
            })
    }

    return <SwipeListView showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        ListFooterComponent={() => <View style={styles.separator} />}
        renderItem={({ item }: any) =>
            <PointsItem team={item.orgName}
                points={item.points}
                left={`$${item.orgRate}`}
                right={`$${item.price}`}
                image={item.orgLogo} />
        }
        renderHiddenItem={(item, _) => (
            <View style={styles.hidden}>
                <TouchableOpacity style={styles.hiddenItem}
                    onPress={() => navigate(item.item, TRANSFER_POINTS_NAVIGATION_KEY)}>
                    <Label children={TRANSFER_TITLE}
                        fontFamily='semiBold'
                        fontSize='small' />
                </TouchableOpacity>
                <View style={[styles.hiddenItem, styles.redeem]}>
                    <TouchableOpacity style={[styles.hiddenItem, styles.redeem]}
                        onPress={() => navigate(item.item, REDEEM_POINTS_NAVIGATION_KEY)}>
                        <Label children={REDEEM_TITLE_UPPERCASE}
                            fontFamily='semiBold'
                            fontSize='small' />
                    </TouchableOpacity>
                </View>
            </View>
        )}
        disableRightSwipe
        leftOpenValue={0}
        rightOpenValue={-normalize(150)}
        data={data}
        keyExtractor={(_, index) => 'Item_' + index} />
}

const styles = StyleSheet.create({
    separator: {
        height: normalize(10)
    },
    hidden: {
        width: '82.5%',
        height: '100%',
        flexDirection: 'row',
        marginHorizontal: normalize(15),
        alignSelf: 'flex-end',
        backgroundColor: Colors.DULL_RED,
        justifyContent: 'flex-end',
        borderRadius: normalize(6)
    },
    hiddenItem: {
        width: normalize(75),
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.DULL_RED
    },
    redeem: {
        backgroundColor: Colors.DULL_GREEN,
        borderTopEndRadius: normalize(6),
        borderBottomEndRadius: normalize(6)
    }
})