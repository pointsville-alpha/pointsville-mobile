import React from 'react'
import {
    View,
    StyleSheet, Image
} from 'react-native'
import {
    normalize,
    Colors
} from '../../../../utilities'
import { Label } from '../../../../components'
import { imageSelect } from '../../../../assets/ImageSelect'
import Coin from '../../../../components/atomic/Coin'

interface IPointsItemProps {
    image: string
    team: string
    points: string
    left: string
    right: string
}

export default function PointsItem(props: IPointsItemProps) {
    const pointsPrefix = props.points.toString().charAt(0)
    const pointsColor = pointsPrefix === '-' ? 'BRIGHT_RED'
        : pointsPrefix === '+' ? 'BRIGHT_GREEN'
            : 'WHITE'
    return <View style={styles.background}>
        <View style={styles.container}>
            <Image source={{uri:props.image}} style={styles.image}/>
            <View style={styles.textContainer}>
                <View>
                    <Label children={props.team} />
                    <Label children={props.left}
                        style={styles.bottomText}
                        fontSize='small_medium'
                        color='WHITE_ALPHA_50' />
                </View>
                <View>
                    <View style={styles.pointsContainer}>
                        <Coin />
                        <Label children={props.points}
                            color={pointsColor}
                            style={styles.pointsText}
                            fontFamily='medium' />
                    </View>
                    <Label children={props.right}
                        style={styles.bottomText}
                        fontSize='small_medium'
                        color='WHITE_ALPHA_50' />
                </View>
            </View>
        </View>
    </View>
}

const styles = StyleSheet.create({
    background: {
        width: '92.5%',
        height: normalize(60),
        backgroundColor: Colors.BACKGROUND,
        borderRadius: normalize(6),
        alignSelf: 'center'
    },
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE_ALPHA_5,
        paddingStart: normalize(8),
        paddingEnd: normalize(16),
        borderRadius: normalize(6),
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        height: normalize(40),
        width: normalize(40),
        marginEnd: normalize(6),
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    bottomText: {
        marginTop: normalize(4)
    },
    pointsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    pointsText: {
        marginStart: normalize(4)
    }
})