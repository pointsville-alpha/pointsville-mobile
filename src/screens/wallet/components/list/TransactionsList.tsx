import React from 'react'
import {
    View,
    StyleSheet,
    FlatList,
    SectionList
} from 'react-native'
import { Colors, normalize } from '../../../../utilities'
import PointsItem from './PointsItem'
import moment from 'moment'
import { Text } from 'react-native-svg'
import { Label } from '../../../../components'
interface ITransactionsList {
    data: any
}

export default function TransactionsList({ data }: ITransactionsList) {

    const currentYear = moment(new Date()).format(" YYYY")

    const formatDate = (timestamp) =>{
        return moment(Number(timestamp)).format('MMM DD, YYYY - HH:MM')
    }
    const groupByDate = (data) => {
        let sectionData = {};
        
        data.forEach(element => {
            let dateString = moment(Number(element.transactionDate)).format('DD MMM YYYY').replace(currentYear,'')
            if (!sectionData[dateString]) {
                sectionData[dateString] = []
            }
            sectionData[dateString].push(element)

        });
        return sectionData;
    }

    const prepareSectionedData = () => {

        let sectionData = groupByDate(data)
        let keys = Object.keys(sectionData)

        return keys.map(key => {
            return { title: key, data: [...sectionData[key]] }
        })
        //return data;
    }

    const DATA = prepareSectionedData();

    return <SectionList showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        ListFooterComponent={() => <View style={styles.separator} />}
        renderItem={({ item }: any) =>
            <PointsItem team={item.orgName}
                points={item.points}
                left={formatDate(item.transactionDate)}
                right={item.id.substring(0, 8)}
                image={item.orgLogo} />
        }
        renderSectionHeader={({ section: { title } }) =>
            <View style={styles.headerContainer}>
                <View style={styles.headerline} />
                <Label
                    numberOfLines={1}
                    color='WHITE_ALPHA_50'
                    children={title}
                    fontSize={'small_tiny'}
                    style={styles.header} />
                <View style={styles.headerline} />
            </View>

        }
        sections={DATA}
        keyExtractor={(_, index) => 'Item_' + index} />
}

const styles = StyleSheet.create({
    separator: {
        height: normalize(10)
    },
    header: {
        alignSelf: 'center',
        padding: normalize(10)
    },
    headerContainer: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    headerline: {
        height: normalize(1),
        width: normalize(45),
        backgroundColor: Colors.WHITE_ALPHA_10,
        alignSelf:'center'
    }
})