import React from 'react'
import {
    View,
    StyleSheet,
    Text
} from 'react-native'
import {
    normalize,
    WALLET_BALANCE_TITLE,
    Colors,
    HUNT_TITLE,
    PROMO_TITLE,
    RECEIVED_TITLE,
    QRSCAN_NAVIGATION_KEY
} from '../../../utilities'
import {
    WalletQR,
    LogoWatermark,
    Scissor
} from '../../../assets/svg_icons'
import { Label } from '../../../components'
import Gradient from '../../../components/atomic/Gradient'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import Coin from '../../../components/atomic/Coin'

interface IWalletCardProps {
    info: any
    openPopup: () => void
}

export default function WalletCard(props: IWalletCardProps) {
    console.log("WalletCard",props)
    const navigation = useNavigation()
    return <Gradient shadowStyle={styles.shadow}
        style={styles.container}>
        <View style={styles.topContainer}>
            <Label children={WALLET_BALANCE_TITLE}
                color='WHITE_ALPHA_70' />
            <View style={styles.iconContainer}>
                <TouchableOpacity onPress={props.openPopup}>
                    <Scissor />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate(QRSCAN_NAVIGATION_KEY)}>
                    <WalletQR style={styles.qrIcon} />
                </TouchableOpacity>
            </View>
        </View>
        <View style={styles.mainPointsContainer}>
            <Coin size={normalize(18)} />
            <Text children={props.info.totalPoints}
                style={styles.mainPointsText}
                maxFontSizeMultiplier={1.5} />
        </View>
        <Label children={`$${props.info.totalAmount}`}
            color='WHITE_ALPHA_70'
            style={styles.amount} />
        <View style={styles.bottomContainer}>
            <View style={styles.categoryContainer}>
                <Label children={HUNT_TITLE}
                    style={styles.categoryTitle}
                    fontSize='small_medium'
                    color='WHITE_ALPHA_70' />
                <View style={styles.iconContainer}>
                    <Coin />
                    <Label children={props.info.hunt}
                        style={styles.categoryPoints}
                        fontSize='small' />
                </View>
            </View>
            <View style={styles.categoryContainer}>
                <Label children={PROMO_TITLE}
                    style={styles.categoryTitle}
                    fontSize='small_medium'
                    color='WHITE_ALPHA_70' />
                <View style={styles.iconContainer}>
                    <Coin />
                    <Label children={props.info.promo}
                        style={styles.categoryPoints}
                        fontSize='small' />
                </View>
            </View>
            <View style={styles.categoryContainer}>
                <Label children={RECEIVED_TITLE}
                    style={styles.categoryTitle}
                    fontSize='small_medium'
                    color='WHITE_ALPHA_70' />
                <View style={styles.iconContainer}>
                    <Coin />
                    <Label children={props.info.received}
                        style={styles.categoryPoints}
                        fontSize='small' />
                </View>
            </View>
        </View>
        <LogoWatermark style={styles.watermark} />
    </Gradient>
}

const styles = StyleSheet.create({
    container: {
        width: '92.5%',
        marginTop: normalize(9),
        height: normalize(170),
        paddingHorizontal: normalize(15),
        paddingVertical: normalize(12),
        alignItems: 'flex-start',
        alignSelf: 'center'
    },
    shadow: {
        shadowOpacity: 0
    },
    topContainer: {
        height: normalize(22),
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: normalize(5)
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    qrIcon: {
        marginStart: normalize(20)
    },
    mainPointsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: normalize(2)
    },
    mainPointsText: {
        fontSize: normalize(30),
        lineHeight: normalize(36),
        color: Colors.WHITE,
        fontFamily: 'Graphik-regular',
        marginStart: normalize(5)
    },
    amount: {
        marginStart: normalize(23)
    },
    bottomContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end'
    },
    categoryContainer: {
        marginEnd: normalize(30)
    },
    categoryTitle: {
        marginBottom: normalize(5)
    },
    categoryPoints: {
        marginStart: normalize(4)
    },
    watermark: {
        position: 'absolute',
        bottom: 0,
        end: 0
    }
})