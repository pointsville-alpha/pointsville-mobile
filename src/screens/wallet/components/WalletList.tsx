import React, { useEffect, useState } from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import {
    normalize,
    MY_POINTS_TITLE,
    TRANSACTIONS_TITLE
} from '../../../utilities'
import {
    Label,
    Gradient
} from '../../../components'
import { TouchableOpacity } from 'react-native-gesture-handler'
import MyPointsList from './list/MyPointsList'
import TransactionsList from './list/TransactionsList'

interface IWalletListProps {
    myPoints: any[]
    transactions: any[]
}

export default function WalletList(props: IWalletListProps) {

    const [showTransactions, toggle] = useState(false)

    useEffect(()=>{
        console.log("props",JSON.stringify(props))
        props.myPoints.length == 0 && toggle(true)
        return(()=>{

        })
    },[props])
    
    return <View style={styles.container} >
        <View style={styles.header}>
            {props.myPoints.length > 0 &&
                <TouchableOpacity onPress={() => toggle(false)}
                    activeOpacity={1}
                    style={styles.option}>
                    <Label children={MY_POINTS_TITLE}
                        color={!showTransactions ? 'WHITE' : 'WHITE_ALPHA_50'}
                        fontFamily='medium'
                        fontSize='small' />
                    {!showTransactions &&
                        <Gradient shadowStyle={styles.shadow}
                            style={styles.selection} />}
                </TouchableOpacity>}
            {props.transactions.length > 0 &&
                <TouchableOpacity onPress={() => toggle(true)}
                    activeOpacity={1}
                    style={styles.option}>
                    <Label children={TRANSACTIONS_TITLE}
                        color={showTransactions ? 'WHITE' : 'WHITE_ALPHA_50'}
                        fontFamily='medium'
                        fontSize='small' />
                    {showTransactions &&
                        <Gradient shadowStyle={styles.shadow}
                            style={styles.selection} />}
                </TouchableOpacity>}
        </View>
        {showTransactions ?
            <TransactionsList data={props.transactions} /> :
            <MyPointsList data={props.myPoints} />}
    </View>
}

const styles = StyleSheet.create({
    container: {
        marginTop: normalize(30),
        flex: 1
    },
    shadow: {
        shadowOpacity: 0
    },
    header: {
        flexDirection: 'row',
        marginBottom: normalize(15),
        marginHorizontal: normalize(14)
    },
    option: {
        marginEnd: normalize(20)
    },
    selection: {
        marginTop: normalize(5),
        width: '100%',
        height: 2,
        borderRadius: 1
    }
})