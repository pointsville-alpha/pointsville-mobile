import React from 'react'
import {
    StyleSheet,
    Image,
    View
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-navigation'
import {
    Button,
    Label,
    ScreenContainer
} from '../../components'
import {
    FONTSIZE,
    normalize,
    SHARE_TITLE,
    WALLET_ID_TITLE,
    YOUR_FRIENDS_CAN_SCAN_DESCRIPTION,
    Colors,
    COPY_ID_TITLE
} from '../../utilities'
import ShareModal from '../settings/components/ShareModal.ios'

interface Props {
    navigation: any
}

interface Istate {
    modal: boolean
}

const CODE = '3857 5294 1039 4853 33'

export default class QRScan extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            modal: false
        }
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerTitle: WALLET_ID_TITLE,
            headerLeftContainerStyle: {
                marginStart: normalize(30)
            }
        })
    }

    render() {
        return <ScreenContainer>
            <SafeAreaView style={styles.container}>
                <View style={styles.mainSection}>
                    <Image source={require('../../assets/images/qr_code.png')}
                        resizeMode='stretch'
                        style={styles.image} />
                    <Label children={CODE}
                        style={styles.code}
                        fontSize='extra_medium'
                        fontFamily='semiBold' />
                    <Label children={YOUR_FRIENDS_CAN_SCAN_DESCRIPTION}
                        numberOfLines={5}
                        style={styles.description}
                        color='WHITE_ALPHA_50' />
                </View>
                <TouchableOpacity style={styles.copyButton}>
                    <Label children={COPY_ID_TITLE}
                        style={styles.description}
                        fontSize='extra_medium' />
                </TouchableOpacity>
                <Button onPress={() => this.setState({ modal: true })}
                    title={SHARE_TITLE} />
            </SafeAreaView>
            <ShareModal visible={this.state.modal}
                dismiss={() => this.setState({ modal: false })} />
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(30),
        flex: 1
    },
    mainSection: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    image: {
        height: normalize(200),
        width: normalize(200),
        backgroundColor: 'white',
        marginBottom: normalize(40)
    },
    code: {
        marginBottom: normalize(10)
    },
    description: {
        lineHeight: FONTSIZE.large,
        textAlign: 'center'
    },
    copyButton: {
        width: '100%',
        height: normalize(54),
        borderWidth: 0.5,
        borderColor: Colors.WHITE_ALPHA_20,
        borderRadius: normalize(6),
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: normalize(16)
    }
})