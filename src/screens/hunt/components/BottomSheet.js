import React from 'react'
import { Modal, StyleSheet, Animated, Dimensions, PanResponder, View } from 'react-native'
import { Colors, normalize } from '../../../utilities'

export default class BottomSheet extends React.PureComponent<Props> {

    constructor(props) {
        super(props)

        this.state = {
            panY: new Animated.Value(Dimensions.get('screen').height)
        };
        this._resetPositionAnim = Animated.timing(this.state.panY, {
            toValue: 0,
            duration: 300,
        }, { useNativeDriver: true })
        this._closeAnim = Animated.timing(this.state.panY, {
            toValue: Dimensions.get('screen').height,
            duration: 500,
        }, { useNativeDriver: true })
        this._panResponders = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onMoveShouldSetPanResponder: () => false,
            onPanResponderMove: Animated.event([
                null, { dy: this.state.panY }
            ], { useNativeDriver: true }),
            onPanResponderRelease: (e, gs) => {
                if (gs.dy > 0 && gs.vy > 2) {
                    return this._closeAnim.start(() => this.props.onDismiss());
                }
                return this._resetPositionAnim.start();
            },
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            prevProps.visible !== this.props.visible
            && this.props.visible
        ) {
            this._resetPositionAnim.start();
        }
        if (!this.props.visible) {
            this._handleDismiss()
        }
    }
    _handleDismiss() {
        this._closeAnim.start(() => this.props.onDismiss());
    }

    render() {
        const top = this.state.panY.interpolate({
            inputRange: [-1, 0, 1],
            outputRange: [0, 0, 1],
        }, { useNativeDriver: true });
        return (
            <View style={styles.overlay}>
                <Animated.View style={[styles.container, { top }]}>
                    {this.props.children}
                </Animated.View>
            </View>
        )
    }



}

const styles = StyleSheet.create({
    overlay: {
        // backgroundColor: Colors.BACKGROUND_ALPHA_50,
        flex: 1,
        justifyContent: 'flex-end',
        height: '100%',
        width: '100%',
        position: 'absolute',

    },
    container: {
        backgroundColor: Colors.BACKGROUND,
        paddingTop: normalize(12),
        borderTopRightRadius: normalize(12),
        borderTopLeftRadius: normalize(12),
    },
});