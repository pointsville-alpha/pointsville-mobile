import React, { useState } from 'react'
import { StyleSheet, TextInput, View, TouchableOpacity, ScrollView } from 'react-native';
import { PointerIcon } from '../../../assets/svg_icons';
import { Label } from '../../../components';
import { MAP_AUTOCOMPLETE_SAMPLE_DATA } from '../../../placeholderdata/Hunt';
import { Colors, InputStyle, normalize, SEARCH_LOCATION } from '../../../utilities';

const AutocompletePlaces = (props: any)=> {

    var searhTimeout:any;
    const [placeText, setPlaceText] = useState('')
    const [autoCompleteSuggessions, setAutoCompleteSuggessions] = useState([])
    
    const loadAutoCompletePlaces = (text: string) => {
        setPlaceText(text)
        // if (text) {
        //     setAutoCompleteSuggessions(MAP_AUTOCOMPLETE_SAMPLE_DATA)
        // } else {
        //     setAutoCompleteSuggessions([])
        // }
        if(searhTimeout)
        {
            clearTimeout(searhTimeout)
        }
        searhTimeout = setTimeout(()=>{
            fetchPlaces();
        },2000)
        

    }

    const fetchPlaces = async ()=>{
        var response = await fetch(`https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${placeText.trim().replace(" ","+")}&types=address&key=AIzaSyALqFP-Byo_LPoJlviyTk2OmITJxKRFMM4`, {
            method: 'GET',
            headers: {
              Accept: 'application/json',
            },
          })
          response
          .json()
          .then(res => setAutoCompleteSuggessions(res.predictions))
          .catch(e=>{
              console.log("Places API error",e)
          });
    }

    return (
        <>
            <View style={styles.inputFieldContainer}>
                <PointerIcon />
                <TextInput
                    numberOfLines={1}
                    style={[InputStyle.field, styles.field]}
                    value={placeText}
                    onChangeText={loadAutoCompletePlaces}
                    placeholder={SEARCH_LOCATION}
                    placeholderTextColor={Colors.WHITE_ALPHA_50}
                    autoCorrect={false}
                    autoFocus={false}
                    autoCapitalize='none'
                    underlineColorAndroid='transparent' />
                {/* <MicIcon /> */}
            </View>
            <View style={styles.autoCompleteSuggessions}>
                <ScrollView>
                    {autoCompleteSuggessions.map((item,index) => {
                        return (
                            <TouchableOpacity
                                key={`${index}`}
                                onPress={() => {
                                    setPlaceText(item.description)
                                    setAutoCompleteSuggessions([])
                                }}>
                                <Label children={item.description}
                                    fontSize={'regular'}
                                    fontFamily={'medium'}
                                    style={{ padding: normalize(10), margin: normalize(1), backgroundColor: Colors.BACKGROUND_ALPHA_40 }} />
                            </TouchableOpacity>
                        )
                    })}
                </ScrollView>
            </View>
            <View
            style={{flex:1,width:0}}/>
        </>
    )
}

export default AutocompletePlaces;

const styles = StyleSheet.create({
    inputFieldContainer: {
        paddingHorizontal: normalize(10),
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: normalize(16),
        borderRadius: normalize(6),
        height: normalize(43),
        borderWidth: normalize(1),
        borderColor: Colors.WHITE_ALPHA_50
    },
    field: {
        height: normalize(43),
        marginStart: normalize(10),
        color: Colors.WHITE,
        fontSize: normalize(14)
    },
    autoCompleteSuggessions: {
        alignSelf: 'stretch',
        backgroundColor: Colors.WHITE_ALPHA_5,
        borderRadius: normalize(6),
        marginTop: normalize(5),
        maxHeight: normalize(150),
    }
})