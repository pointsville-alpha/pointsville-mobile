import React, { useEffect, useRef, useState, useLayoutEffect } from 'react'
import { SafeAreaView, StyleSheet, Alert, Platform, View, Image, TouchableOpacity, Modal } from 'react-native'
import { WikitudeView } from 'react-native-wikitude-sdk'
import RNFetchBlob from 'rn-fetch-blob'
import Overlay from './Overlay'
import Geolocation from '@react-native-community/geolocation';
import {
    MAP_BOTTOM_BANNER_TITLE,
    MAP_BOTTOM_POINT_ADDRESS,
    MAP_BOTTOM_POINT_TYPE,
    LOCAL_POIS_DATA
} from '../../../placeholderdata/Hunt'
import Map from './Map'
import BottomSheet from './BottomSheet'
import { Colors, normalize, normalizeVertical } from '../../../utilities'
import { Label } from '../../../components'
import ExitHunt from './ExitHunt'

export default function WikitudeComponent({ route, navigation }: any) {
    const wikitudeView = useRef(null)
    const [isFocus, setFocus] = useState(false)
    const [isMapView, setMapView] = useState(true)
    const [isBottomSheeVisible, setBottomSheetVisible] = useState(false)
    const [latitude, setLatitude] = useState(19.9799004)
    const [longitude, setLongitude] = useState(73.7453953)
    const [poisData, setPoisData] = useState([])
    const [selectedCoin, setSelectedCoin] = useState('')
    const [exitPopup, showExitPop] = useState(false)
    const [collectedPoints, collectPoints] = useState(0)
    let screeshotPath = ''

    console.log('URL WIKITUDE: ', route.params.url)

    useEffect(() => {
        const subscribeFocus = navigation.addListener('blur', () => {
            console.log('Is blur')
            if (wikitudeView.current) {
                wikitudeView.current.stopRendering()
            }
            setFocus(false)
        })
        navigation.addListener('focus', () => {
            console.log('Is focus')
            setFocus(true)
            if (wikitudeView.current) {
                wikitudeView.current.resumeRendering()
            }
        })
        let pois = Array();
        Object.keys(LOCAL_POIS_DATA).map(key => {
            pois.push(...LOCAL_POIS_DATA[key])
        })
        setPoisData(pois)
        Geolocation.getCurrentPosition(
            position => {

                console.log("GEO initialPosition", JSON.stringify(position));
                setLatitude(position.coords.latitude);
                setLongitude(position.coords.longitude);

            },
            error => console.log('Error', JSON.stringify(error)),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
        Geolocation.watchPosition(position => {
            const lastPosition = JSON.stringify(position);
            console.log("GEO lastPosition", lastPosition);
            setLatitude(position.coords.latitude);
            setLongitude(position.coords.longitude);
        });

        return subscribeFocus
    }, [navigation])

    useLayoutEffect(() => {
        //loadPoisDatToWikitude(poisData)
        return;
    })
    const loadPoisDatToWikitude = (pois: any) => {
        if (wikitudeView.current) {
            //wikitudeView.current.callJavascript('World.reloadPlaces()')
            //wikitudeView.current.callJavascript(`World.requestDataFromServer(${latitude},${longitude})`)
            wikitudeView.current.injectLocation(latitude, longitude);
        }
    }
    const config = {
        licenseKey:
            'WBUvfVlvG1j21fc6QtG0Qa7iLErVT4pspLrSHoaO3yLZUaEtlsOVbI5vgsAWhIjyMAcNTyhUs06UH1OjyN+kPH2pXm3YncLOs5l+lzgkqClC0jllUsqJ8ZM93a/69Gnr7K8AMNg4Z0E1iPUVSEKekbi0RIlMSwWEqIeDUlVT7WBTYWx0ZWRfXysFHgRnc1eEfRfLWtpAztcMhEMM5EIxcy5Rtq6mUBduL60RsLWuudpIB1FVHLHSmI5fdFINAnWvHJIYc3jNwXB9mU4EEVn/b1NgnmRHSAaWWYgw8B/HOlG2nt4PCBOnk6rrEWFmWc+98dt2VhRJvhP3BQV+MTnmyZQB9s2VCdP8yTJ8THkuUjAAhV6F/2mGlvuYWgEaTVQcj+7fx0KIAo9Iux7csJGC674MLSeBsG5lwnXglt5VYmze8HlK10jWW4VjVsQwPTJr0d79qpXu1abGQIdnvkQL+sjYXlbkfLfVMedKGyY+mHyAEs42raClwW5qYsE7YNak0kBdchdNRj6H8TeAIKRyroHXdF6C++2svN1chzqOXh1jkdqM0n7td53EgeMjN/6lJ2+tGLR9DoecSoA4vgi8TBN9C9DRWBbKMHJnSmSghnBOKMjIr84H86Ibw1T9jHTRVhCHjExws9HKGC6yc7gSNY+nZE93xuqsNdEvob7ztO2RrAXn2GwXVL22WU5wAiW4wOwfEGKUam9hAb0rMOImCIsuvMbmC4IccFOY/HBMXGHGX/5+aZAxYfaK/SgdOZAxlsOHXknwfyE9v5Pxo5KW4nyY5ucqP8/4K0rIccBETGeZlNEKZTxz2PgrHNBbV6+ptNFUoNEnfKMi8z2+tjZY6u/IiYS/eiZ1W3jTf0WzjIw=',
        url: route.params.url,
        isPOI: true,
    }

    const onFinishLoading = (event: any) => {
        console.log('On Finish Loading')
        loadPoisDatToWikitude(poisData)
    }

    const onJsonReceived = (object: any) => {
        console.log("onJsonReceived", object)
        switch (object.action) {
            case 'coin-selected':
                collectPoints(collectedPoints + Number(object.data.title))
                break
        }
    }

    const onFailLoading = (event: any) => console.log(event)

    const onScreenCaptured = (event: any) => {
        console.log("onScreenCaptured", event)
        if (!event.image) return

        //store screenshot

        const dirs = RNFetchBlob.fs.dirs;

        screeshotPath = `${dirs.DCIMDir}/PointsVilles/PointVilles_${new Date().getMilliseconds()}.png`;

        RNFetchBlob.fs.writeFile(screeshotPath, event.image, 'base64')
            .then((res) => {
                console.log("File : ", res)
                displayFileStoredAlert();
            });

    }

    const onToggleFlash = (toggle: any) => {
        if (wikitudeView.current) {
            //turning flash on/off
            wikitudeView.current.callJavascript('World.updateFlashlight(' + toggle + ')')
            return;
        }
    }
    const captureScreen = () => {
        if (wikitudeView.current) {
            //capture camera screen
            wikitudeView.current.captureScreen(true);
            return;
        }
    }
    const displayFileStoredAlert = () =>
        Alert.alert(
            "Screenshot Taken Successfully!",
            "To view the screenshot please click on Preview.",
            [
                {
                    text: "Close",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "Preview", onPress: () => previewScreenShot() }
            ],
            { cancelable: false }
        );

    const previewScreenShot = () => {

        if (Platform.OS === 'android') {
            RNFetchBlob.android.actionViewIntent(screeshotPath, 'image/png');
        } else {
            RNFetchBlob.ios.previewDocument(screeshotPath);
        }

    }

    const onMute = (mute: boolean) => {
        if (wikitudeView.current) {
            wikitudeView.current.callJavascript('World.muteAudio(' + mute + ')')
        }
    }

    const onFilter = (team: any) => {
        let pois = Array()
        pois.push(...LOCAL_POIS_DATA[team])
        setPoisData(pois)
    }
    const onViewSwitch = (toggle: boolean) => {
        if (!toggle) {
            setBottomSheetVisible(false)
        }
        setMapView(toggle)
    }
    const onPoiClick = (poi) => {
        console.log("onPoiClick", poi)
        setBottomSheetVisible(true)
        setSelectedCoin(poi)
    }

    const onClose = () => {

        setBottomSheetVisible(false)

        if (!isMapView) {
            showExitPop(true)
        } else {
            navigation.goBack()
        }
    }


    return (
        <SafeAreaView style={styles.container}>
            {isFocus && <>
                {!isMapView ?
                    <WikitudeView
                        ref={wikitudeView}
                        licenseKey={config.licenseKey}
                        url={config.url}
                        style={styles.AR}
                        onFailLoading={onFailLoading}
                        onJsonReceived={onJsonReceived}
                        onFinishLoading={onFinishLoading}
                        onScreenCaptured={onScreenCaptured}
                        isPOI={config.isPOI}
                    /> : <Map coordinates={{
                        latitude: 40.712529296874735,
                        longitude: -74.00541918352246,
                        latitudeDelta: 0.01,
                        longitudeDelta: 0.01
                    }} onMarkerPress={onPoiClick} />}

                <Overlay
                    filterData={LOCAL_POIS_DATA}
                    points={collectedPoints}
                    onToggleFlash={onToggleFlash}
                    captureScreen={captureScreen}
                    onMute={onMute}
                    onFilter={onFilter}
                    switchView={onViewSwitch}
                    mapView={isMapView}
                    bottomSheet={isBottomSheeVisible}
                    selectedCoin={selectedCoin}
                    onBottomSheetDismiss={() => setBottomSheetVisible(false)}
                    onClose={onClose} />
                <BottomSheet visible={isBottomSheeVisible} onDismiss={() => setBottomSheetVisible(false)}>
                    <TouchableOpacity style={{ borderRadius: normalize(10) }}
                        onPress={() => setBottomSheetVisible(false)}>
                        <View style={styles.bottomSheetCloseButton} />
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', marginHorizontal: normalize(16), marginBottom: normalize(16) }}>
                        <Image source={require("../../../assets/images/news_1.png")} style={{ height: normalize(70), width: normalize(70), borderRadius: normalize(8) }} />
                        <View style={{ marginStart: normalize(16) }}>
                            <Label style={{ marginVertical: normalize(3) }} children={MAP_BOTTOM_BANNER_TITLE}
                                fontFamily='semiBold'
                                fontSize='medium' />
                            <Label style={{ marginVertical: normalize(3) }} children={MAP_BOTTOM_POINT_TYPE}
                                fontFamily='semiBold'
                                fontSize='regular'
                                color={"GRAY"} />
                            <Label style={{ marginVertical: normalize(3) }} children={MAP_BOTTOM_POINT_ADDRESS}
                                fontFamily='regular'
                                fontSize='tiny'
                                color={"GRAY"} />
                        </View>
                    </View>
                </BottomSheet>
                <ExitHunt
                    points={collectedPoints}
                    visibile={exitPopup}
                    huntAgain={() => {
                        showExitPop(false)
                    }}
                    exitHunt={() => {
                        navigation.goBack()
                    }} />
            </>}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    AR: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    bottomSheet: {
        flex: 1
    },
    bottomSheetCloseButton: {
        marginBottom: normalize(16),
        alignSelf: 'center',
        height: normalize(5),
        width: normalize(50),
        backgroundColor: Colors.WHITE_ALPHA_20,
        borderRadius: normalize(10)
    }
})