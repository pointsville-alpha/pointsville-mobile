import React from 'react'
import { Modal, View } from 'react-native'
import { Button, Label } from '../../../components'
import { EXIT_POPUP_TITLE1, EXIT_POPUP_TITLE2, EXIT_POPUP_TITLE3, EXIT_POPUP_DESC, EXIT_POPUP_BUTTON1, EXIT_POPUP_BUTTON2 } from '../../../placeholderdata/Hunt'
import { Colors, normalize, normalizeVertical } from '../../../utilities'

function ExitHunt(props: any) {

    return (
        <Modal
            visible={props.visibile}
            animated
            animationType="fade"
            transparent>
            <View style={{ flex: 1, backgroundColor: Colors.BLACK_ALPHA_50, justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ backgroundColor: Colors.BACKGROUND, borderRadius: normalize(6) }}>
                    <Label style={{ marginTop: normalizeVertical(40), alignSelf: 'center' }} children={EXIT_POPUP_TITLE1}
                        fontFamily='regular'
                        fontSize='regular'
                        color="GRAY" />
                    <Label style={{ marginTop: normalizeVertical(5), alignSelf: 'center' }} children={`${props.points} ${EXIT_POPUP_TITLE2}`}
                        fontFamily='medium'
                        fontSize='extra_medium'
                        color={'WHITE'} />
                    <Label style={{ marginTop: normalizeVertical(5), alignSelf: 'center' }} children={EXIT_POPUP_TITLE3}
                        fontFamily='regular'
                        fontSize='regular'
                        color="GRAY" />
                    <Label style={{ marginTop: normalizeVertical(25), alignSelf: 'center', textAlign: 'center', marginHorizontal: normalize(55) }} children={EXIT_POPUP_DESC}
                        numberOfLines={2}
                        fontFamily='regular'
                        fontSize='regular'
                        color="GRAY" />
                    <Button
                        style={{ marginTop: normalizeVertical(25), alignSelf: 'center', height: normalizeVertical(44), width: normalize(160) }}
                        title={EXIT_POPUP_BUTTON1}
                        onPress={props.huntAgain} />
                    <Label style={{ marginBottom: normalizeVertical(25), alignSelf: 'center', textAlign: 'center' }} children={EXIT_POPUP_BUTTON2}
                        numberOfLines={1}
                        fontFamily='regular'
                        fontSize='regular'
                        color="GRAY"
                        onPress={props.exitHunt} />
                </View>
            </View>
        </Modal>
    )
}

export default ExitHunt