import React, { useEffect, useState } from "react";
import { Image } from 'react-native'
import MapView, { Marker } from "react-native-maps";
import { MAP_STYLE, LOCAL_POIS_COORDINATES } from '../../../placeholderdata/Hunt'
import { CoinIcon } from '../../../assets/svg_icons'
import { normalize } from "../../../utilities";

const Map = ({ coordinates, onMarkerPress }) => {
    
    const [region, setRegion] = useState(coordinates)
    /*
    {
        latitude: 51.5079145,
        longitude: -0.0899163,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      }
    */
    useEffect(() => {
        console.log('coordinates',coordinates)
        
    }, [region])
    return (
        coordinates.latitude ?
            <MapView
                style={{ flex: 1 }}
                region={region}
                customMapStyle={MAP_STYLE}
                onRegionChangeComplete={region => {
                    console.log({latitude:region.latitude,longitude:region.longitude})
                    setRegion(region)
                    }}>
                {Object.keys(LOCAL_POIS_COORDINATES).map(team => {
                    return LOCAL_POIS_COORDINATES[team].map(coordinate => {
                        return (
                            <Marker
                                identifier={coordinates.id}
                                title={coordinate.title}
                                coordinate={coordinate}
                                onPress={(event) => onMarkerPress(coordinate.id)}>
                                <CoinIcon height={normalize(25)} width={normalize(25)} />
                            </Marker>
                        )
                    })
                })}
            </MapView> : null
    );
};

export default Map;