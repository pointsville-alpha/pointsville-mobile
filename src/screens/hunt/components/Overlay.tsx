import { useNavigation } from '@react-navigation/native'
import React, { useEffect, useState } from 'react'
import {
    View,
    StyleSheet,
    Image, FlatList, Text, TextInput, ScrollView
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {
    Flash,
    HeaderClose,
    MapSheet,
    Mute,
    ScreenShot,
    More,
    WalletIcon, SoundOnIcon, FlashOnIcon, FilterIcon, ARIcon, PointerIcon, MicIcon,PiratesCoin
} from '../../../assets/svg_icons'
import { Label } from '../../../components'
import Coin from '../../../components/atomic/Coin'
import {
    Colors,
    FONTSIZE,
    normalize,
    InputStyle, SEARCH_LOCATION
} from '../../../utilities'
import { BOTTOM_ADD_TO_WALLET, BOTTOM_BANNER_TITLE, BOTTOM_POINT_PRICE, icon1, MAP_AUTOCOMPLETE_SAMPLE_DATA } from '../../../placeholderdata/Hunt'
import AutocompletePlaces from './AutocompletePlaces'

export default function Overlay(props: any) {
    const navigation = useNavigation()
    const [isFlashOn, setFlash] = useState(false)
    const [isMute, setMute] = useState(false)
    const [isMoreVisible, setMoreVisibility] = useState(false)
    const [isFilterVisible, setFilterVisibility] = useState(false)
    const [placeText, setPlaceText] = useState('')
    const [teams, setTeams] = useState([])
    const [autoCompleteSuggessions, setAutoCompleteSuggessions] = useState([])
    const toggleFlash = () => {
        props.onToggleFlash(!isFlashOn)
        setFlash(!isFlashOn)
    }
    const toggleMute = () => {
        props.onMute(!isMute)
        setMute(!isMute)
    }

    const toggleMore = () => {
        setMoreVisibility(!isMoreVisible)
    }

    const toggleFilter = () => {
        setFilterVisibility(!isFilterVisible)
    }
    const toggleMapView = () => {
        props.switchView(!props.mapView)
    }

    const loadAutoCompletePlaces = (text:string)=>{
        /**
         * call google places api here
         */
        setPlaceText(text)
        if(text){
            setAutoCompleteSuggessions(MAP_AUTOCOMPLETE_SAMPLE_DATA)
        }else{
            setAutoCompleteSuggessions([])
        }
        
    }

    useEffect(() => {
        setTeams(Object.keys(props.filterData))
    }, [props.filterData])

    return <View style={[styles.container]} pointerEvents={'box-none'}>
        <View style={styles.topSection}>
            <TouchableOpacity onPress={props.onClose}
                children={<HeaderClose />}
                style={styles.closeButton} />
            {!props.mapView && <View style={styles.pointsContainer}>
                <Coin />
                <Label children={props.points}
                    color='BACKGROUND_DEEP'
                    style={styles.pointsCount}
                    fontFamily='semiBold'
                    fontSize='small' />
                <Label children={' pts'}
                    color='BACKGROUND_DEEP'
                    style={styles.pointsText}
                    fontSize='small' />
            </View>}
            <TouchableOpacity children={props.mapView ? <ARIcon /> : <MapSheet />}
                style={[styles.pointImage, styles.transperentWhiteBackground]}
                onPress={toggleMapView} />
        </View>
        {!props.mapView ?
            <>
            <View
            style={{flex:1,width:0}}/>
                <View style={styles.mainSection}>
                    <View>
                        {isMoreVisible &&
                            <View>
                                <TouchableOpacity children={!isMute ? <Mute /> : <SoundOnIcon />}
                                    style={[styles.mainButtons, styles.moreButton]}
                                    onPress={toggleMute} />
                                <TouchableOpacity children={isFlashOn ? <FlashOnIcon /> : <Flash />}
                                    style={[styles.mainButtons, styles.moreButton]}
                                    onPress={toggleFlash} />
                                <TouchableOpacity children={<ScreenShot />}
                                    style={[styles.mainButtons, styles.moreButton]}
                                    onPress={props.captureScreen} />
                            </View>
                        }
                        <TouchableOpacity children={isMoreVisible ? <HeaderClose /> : <More />}
                            style={[styles.mainButtons, styles.moreButton]}
                            onPress={toggleMore} />
                    </View>
                </View>
                
                <View style={styles.bottomView}>
                    {!isFilterVisible && <PiratesCoin height={normalize(42)} width={normalize(42)}/>}
                    {isFilterVisible ?
                        <View style={styles.pointDetail}>

                            <FlatList
                                data={teams}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                overScrollMode={'never'}
                                renderItem={({ item }) => {
                                    console.log("item", item)
                                    return (
                                        <View style={styles.flatListItem}>
                                            <TouchableOpacity children={<PiratesCoin height={normalize(42)} width={normalize(42)}/>/*<Image style={styles.pointImage} source={require("../../../assets/icons/pirates_coin.svg")} />*/}

                                                onPress={() => { props.onFilter(item) }} />
                                        </View>
                                    )
                                }}
                                keyExtractor={(item) => item} />

                        </View> :
                        <View style={styles.pointDetail}>
                            <Label style={{ marginBottom: normalize(3) }} children={BOTTOM_BANNER_TITLE}
                                fontFamily='semiBold'
                                fontSize='medium' />

                            <View style={styles.pointPrice}>
                                <Label style={{ marginEnd: normalize(5) }} children={BOTTOM_POINT_PRICE}
                                    fontFamily='semiBold'
                                    fontSize='regular' />

                                <Label children={BOTTOM_ADD_TO_WALLET}
                                    fontFamily='regular'
                                    fontSize='regular' />
                            </View>
                        </View>
                    }

                    <TouchableOpacity children={isFilterVisible ? <HeaderClose /> : <FilterIcon />}
                        style={[styles.pointImage, isFilterVisible ? styles.transperentWhiteBackground : {}]}
                        onPress={toggleFilter} />
                </View>
            </> :
            <AutocompletePlaces/>
        }
    </View>
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        paddingHorizontal: normalize(16),
        position: 'absolute',
        justifyContent:'space-between',
        alignItems:'flex-end'
    },
    topSection: {
        alignSelf:'stretch',
        marginTop: normalize(51),
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    closeButton: {
        //backgroundColor: Colors.BACKGROUND_ALPHA_40,
        width: normalize(32),
        height: normalize(32),
        borderRadius: normalize(16),
        alignItems: 'center',
        justifyContent: 'center'
    },
    pointsContainer: {
        paddingStart: normalize(12),
        paddingEnd: normalize(14.5),
        height: normalize(32),
        borderRadius: normalize(16),
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Colors.FOG_ALPHA_70
    },
    pointsCount: {
        lineHeight: FONTSIZE.regular,
        marginStart: normalize(4.5)
    },
    pointsText: {
        lineHeight: FONTSIZE.regular
    },
    mainSection: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginBottom: normalize(26)
    },
    mainButtons: {
        marginTop: normalize(20),
        width: normalize(42),
        height: normalize(42),
        borderRadius: normalize(25.5),
        backgroundColor: Colors.BACKGROUND_ALPHA_40,
        alignItems: 'center',
        justifyContent: 'center'
    },
    transperentWhiteBackground: {
        backgroundColor: Colors.WHITE_ALPHA_30,
        shadowColor: Colors.BLACK_ALPHA_50,
        shadowRadius: normalize(4)
    },
    moreButton: {
        backgroundColor: Colors.WHITE_ALPHA_30,
        shadowColor: Colors.BLACK_ALPHA_50,
        shadowRadius: normalize(4)
    },
    bottomView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: Colors.BACKGROUND_ALPHA_50,
        shadowColor: Colors.BLACK_ALPHA_50,
        shadowRadius: normalize(4),
        height: normalize(90),
        marginBottom: normalize(30),
        borderRadius: normalize(10),
        padding: normalize(20),
    },
    pointDetail: {
        flex: 1,
        marginHorizontal: normalize(8),
    },
    pointPrice: {
        flexDirection: 'row',
    },
    pointImage: {
        height: normalize(42),
        width: normalize(42),
        borderRadius: normalize(25.5),
        alignItems: 'center',
        justifyContent: 'center'
    },
    flatListItem: {
        justifyContent: 'center',
        marginHorizontal: normalize(10)
    },
    inputFieldContainer: {
        paddingHorizontal: normalize(10),
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: normalize(16),
        borderRadius: normalize(6),
        height: normalize(43),
        borderWidth: normalize(1),
        borderColor: Colors.WHITE_ALPHA_50
    },
    field: {
        height: normalize(43),
        marginStart: normalize(10),
        color: Colors.WHITE,
        fontSize: normalize(14)
    },
    autoCompleteSuggessions:{
        alignSelf:'stretch',
        backgroundColor:Colors.WHITE_ALPHA_5,
        borderRadius:normalize(6),
        marginTop:normalize(5),
        maxHeight:normalize(200),
    }
})