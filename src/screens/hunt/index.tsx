import React from 'react'
import { StyleSheet, Alert, Platform } from 'react-native'
import { request, PERMISSIONS, requestMultiple, openSettings } from 'react-native-permissions';
import { greaterOrEq } from 'react-native-reanimated';
import {
    Button,
    Label,
    ScreenContainer
} from '../../components'
import {
    CAMERA_PERMISSION_RTIONALE,
    CAMERA_PERMISSION_RTIONALE_DESC,
    HUNT_NOW_EXCLAMATION,
    HUNT_NOW_SAFETY_MESSAGE,
    HUNT_NOW_SAFETY_MESSAGE_BOLD,
    LOCATION_PERMISSION_RTIONALE,
    LOCATION_PERMISSION_RTIONALE_DESC,
    normalize,
    STORAGE_PERMISSION_RTIONALE,
    STORAGE_PERMISSION_RTIONALE_DESC,
    WIKITUDE_NAVIGATION_KEY
} from '../../utilities'

interface Props {
    navigation: any
}

export default class Hunt extends React.Component<Props> {

    constructor(props: Props) {
        super(props)
    }

    validatePermissions = () => {
        const permissions = Platform.OS === 'ios' ?
            [PERMISSIONS.IOS.CAMERA,
            PERMISSIONS.IOS.LOCATION_WHEN_IN_USE] :
            [PERMISSIONS.ANDROID.CAMERA,
            PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
            PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
            PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE];
        requestMultiple(permissions).then(result => {
            console.log("result", result);
            let isGranted = false;
            permissions.some(element => {
                isGranted = result[element] === 'granted';
                if (!isGranted && result[element] === 'blocked') {
                    switch (element) {
                        case PERMISSIONS.ANDROID.CAMERA:
                        case PERMISSIONS.IOS.CAMERA:
                            this.showAlert(
                                CAMERA_PERMISSION_RTIONALE,
                                CAMERA_PERMISSION_RTIONALE_DESC)
                            return true;
                        case PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION:
                        case PERMISSIONS.IOS.LOCATION_WHEN_IN_USE:
                            this.showAlert(
                                LOCATION_PERMISSION_RTIONALE,
                                LOCATION_PERMISSION_RTIONALE_DESC)
                            return true;
                        case PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE:
                        case PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE:
                            this.showAlert(
                                STORAGE_PERMISSION_RTIONALE,
                                STORAGE_PERMISSION_RTIONALE_DESC)
                            return true;
                    }
                } else if (!isGranted) {
                    this.validatePermissions();
                    return true;
                }
                console.log("not returned", "here")
            });

            if (isGranted) {
                this.openWikitude('/assets/ARchitectExamples/08_PointOfInterest_4_SelectingPois/index')
            }
        })
    }
    openWikitude = (url: string) => {
    
        if (Platform.OS === 'android') {
            url = url.replace('/assets/', '')
        }
        this.props.navigation.navigate(WIKITUDE_NAVIGATION_KEY, { url: url })
    }

    showAlert = (title: string, desc: string) => {
        Alert.alert(
            title,
            desc,
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "Settings", onPress: () => openSettings().then(result => {
                        console.log("setting opn success")
                    }).catch((e) => {
                        console.log(e)
                    })
                }
            ],
            { cancelable: false }
        );
    }

    render() {
        return <ScreenContainer style={styles.container}>
            <Label color='WHITE_ALPHA_70'
                style={styles.text}
                numberOfLines={6}
                fontSize='small'>
                {
                    HUNT_NOW_SAFETY_MESSAGE[0] +
                    HUNT_NOW_SAFETY_MESSAGE_BOLD[0] +
                    HUNT_NOW_SAFETY_MESSAGE[1] +
                    HUNT_NOW_SAFETY_MESSAGE_BOLD[1] +
                    HUNT_NOW_SAFETY_MESSAGE[2]
                }
            </Label>
            <Button onPress={this.validatePermissions}
                style={styles.button}
                title={HUNT_NOW_EXCLAMATION} />
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: normalize(30),
        paddingHorizontal: normalize(20)
    },
    button: {
        width: normalize(160),
        height: normalize(44)
    },
    text: {
        lineHeight: normalize(17),
        textAlign: 'center',
        marginBottom: normalize(20)
    }
})