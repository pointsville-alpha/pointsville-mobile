import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import {
    ScreenContainer,
    Label
} from '../../components'
import {
    normalize,
    OFFERINGS_TITLE
} from '../../utilities'
import { FlatList } from 'react-native-gesture-handler'
import { TEAM_SCREEN_TOKENS } from '../../placeholderdata/Team'
import Offering from './components/Offering'

interface Props {
    navigation: any
}

export default class Team extends React.Component<Props> {

    constructor(props: Props) {
        super(props)
    }

    render() {
        return <ScreenContainer style={styles.container}>
            <View style={styles.topSection}>
                <Label children={OFFERINGS_TITLE}
                    fontSize='small'
                    fontFamily='medium' />
            </View>
            <FlatList data={TEAM_SCREEN_TOKENS}
                columnWrapperStyle={styles.list}
                renderItem={({ item }: any) =>
                    <Offering team={item.team}
                        price={item.price}
                        image={item.image} />}
                keyExtractor={(_, index) => 'Token_' + index}
                showsVerticalScrollIndicator={false}
                numColumns={3} />
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: normalize(14)
    },
    topSection: {
        width: '100%',
        flexDirection: 'row',
        marginTop: normalize(9),
        marginBottom: normalize(22),
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    list: {
        justifyContent: 'space-between'
    }
})