import React, { useState } from 'react'
import {
    StyleSheet,
    View,
    Image
} from 'react-native'
import Label from '../../../components/atomic/Label'
import {
    normalize,
    Colors,
    RECEIVE_TITLE
} from '../../../utilities'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Coin from '../../../components/atomic/Coin'

export interface ITokenProps {
    image: string
    team: string
    price: string
    percentage?: string
    growth?: boolean
    deduction?: string
}

export default function PointsToken(props: ITokenProps) {
    const [pressed, toggle] = useState(false)
    function receive() {

    }

    return <TouchableOpacity activeOpacity={1}
        onPress={() => toggle(!pressed)}
        style={styles.container}>
        <View style={styles.translucency} />
        <View style={styles.subContainer}>
            <Image source={{ uri: props.image }}
                style={styles.image} />
            <Label children={props.team}
                fontFamily='medium'
                fontSize='small' />
            <Label children={props.price}
                fontSize='small' />
            {pressed ?
                <View style={styles.lowerContainer}>
                    <View style={styles.line} />
                    <TouchableOpacity onPress={receive}
                        style={styles.receiveContainer}>
                        <Coin />
                        <Label children={RECEIVE_TITLE}
                            style={styles.receiveText}
                            fontSize='small'
                            fontFamily='semiBold'
                            color='DULL_GREEN' />
                    </TouchableOpacity>
                </View> :
                <View style={styles.lowerContainer}>
                    {props.percentage &&
                        <Label children={props.percentage}
                            color='WHITE_ALPHA_50'
                            fontSize='small_medium' />}
                    {props.deduction &&
                        <Label children={`- ${props.deduction}`}
                            color='WHITE_ALPHA_50'
                            style={styles.bottomText}
                            fontSize='small_medium' />}
                </View>}
        </View>
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    container: {
        width: normalize(110),
        height: normalize(147),
        marginBottom: normalize(20)
    },
    translucency: {
        width: '100%',
        height: '81.6%',
        borderRadius: normalize(6),
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.WHITE_ALPHA_5
    },
    subContainer: {
        position: 'absolute',
        alignSelf: 'center',
        height: '100%',
        alignItems: 'center'
    },
    image: {
        height: normalize(50),
        width: normalize(80),
        marginBottom: normalize(5)
    },
    lowerContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    bottomText: {
        marginTop: normalize(5),
        marginBottom: normalize(13)
    },
    line: {
        width: normalize(90),
        height: 0.5,
        backgroundColor: Colors.WHITE_ALPHA_10
    },
    receiveContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        height: normalize(45)
    },
    receiveText: {
        marginStart: normalize(5)
    }
})