import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import Label from '../../../components/atomic/Label'
import {
    normalize,
    Colors,
    PROMO_NAVIGATION_KEY
} from '../../../utilities'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'
import { imageSelect } from '../../../assets/ImageSelect'

export interface IOfferingProps {
    image: string
    team: string
    price: string
}

export default function Offering(props: IOfferingProps) {
    const navigation = useNavigation()
    return <TouchableOpacity onPress={() => navigation.navigate(
        PROMO_NAVIGATION_KEY,
        {
            team: props.team,
            image: props.image,
            price: props.price
        }
    )}
        style={styles.container}>
        <View style={styles.translucency} />
        <View style={styles.subContainer}>
            <View style={styles.image}>
                {imageSelect(props.image, normalize(50))}
            </View>
            <Label children={props.team}
                style={styles.team}
                fontFamily='medium'
                fontSize='small' />
            <Label children={props.price}
                color='WHITE_ALPHA_50'
                fontSize='small' />
        </View>
    </TouchableOpacity>
}

const styles = StyleSheet.create({
    container: {
        width: normalize(110),
        height: normalize(107),
        marginBottom: normalize(20)
    },
    translucency: {
        width: '100%',
        height: normalize(80),
        borderRadius: normalize(6),
        position: 'absolute',
        bottom: 0,
        backgroundColor: Colors.WHITE_ALPHA_5
    },
    subContainer: {
        position: 'absolute',
        alignSelf: 'center',
        height: '100%',
        alignItems: 'center'
    },
    image: {
        height: normalize(50),
        width: normalize(80),
        marginBottom: normalize(7),
        justifyContent: 'center',
        alignItems: 'center'
    },
    team: {
        marginBottom: normalize(6)
    }
})