import React from 'react'
import {
    View,
    StyleSheet,
    Image
} from 'react-native'
import {
    Label,
    LighterBackground,
    ScreenContainer
} from '../../components'
import {
    normalize,
    Colors,
    SETTINGS_TITLE,
    SETTINGS_NAVIGATION_KEY,
    TERMS_OF_USE_TITLE,
    PRIVACY_POLICY_TITLE,
    TERMS_OF_USE_NAVIGATION_KEY,
    PRIVACY_POLICY_NAVIGATION_KEY,
    EDIT_PROFILE_NAVIGATION_KEY,
    defaultHeaderOptions
} from '../../utilities'
import { Edit } from '../../assets/svg_icons'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface Props {
    navigation: any
    route: any
}

interface Istate {
    name: string
    email: string
}

export default class Account extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            name: 'John Doe',
            email: 'john.doe@mail.com'
        }
    }

    componentDidMount() {
        this.props.navigation.setOptions({
            headerStyle: {
                ...defaultHeaderOptions.headerStyle,
                backgroundColor: Colors.WHITE_ALPHA_5
            }
        })
    }

    editProfile = () => {
        this.props.navigation.navigate(
            EDIT_PROFILE_NAVIGATION_KEY,
            {
                name: this.state.name,
                email: this.state.email,
                setData: (name: string) => this.setState({ name })
            }
        )
    }

    render() {
        return <ScreenContainer>
            <LighterBackground header
                style={styles.lighterBackground} />
            <View style={styles.account}>
                <Image source={require('../../assets/images/profile.png')}
                    style={styles.image} />
                <View style={styles.detailsContainer}>
                    <Label children={this.state.name}
                        fontFamily='semiBold'
                        fontSize='extra_medium' />
                    <Label children={this.state.email}
                        style={styles.email}
                        color='WHITE_ALPHA_70'
                        fontSize='small' />
                </View>
                <TouchableOpacity style={styles.edit}
                    onPress={this.editProfile} >
                    <Edit />
                </TouchableOpacity>
            </View>
            <View style={styles.navigationOptionsList}>
                <TouchableOpacity style={styles.navigationOption}
                    onPress={() => this.props.navigation.navigate(SETTINGS_NAVIGATION_KEY)}>
                    <Label children={SETTINGS_TITLE} />
                </TouchableOpacity>
                <View style={styles.line} />
                <TouchableOpacity style={styles.navigationOption}
                    onPress={() => this.props.navigation.navigate(TERMS_OF_USE_NAVIGATION_KEY)}>
                    <Label children={TERMS_OF_USE_TITLE} />
                </TouchableOpacity>
                <View style={styles.line} />
                <TouchableOpacity style={styles.navigationOption}
                    onPress={() => this.props.navigation.navigate(PRIVACY_POLICY_NAVIGATION_KEY)}>
                    <Label children={PRIVACY_POLICY_TITLE} />
                </TouchableOpacity>
            </View>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    lighterBackground: {
        marginTop: -normalize(100)
    },
    account: {
        position: 'absolute',
        marginTop: normalize(12),
        paddingHorizontal: normalize(14),
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        height: normalize(100),
        width: normalize(100),
        borderRadius: normalize(50),
        backgroundColor: Colors.WHITE_ALPHA_50,
        marginEnd: normalize(20)
    },
    detailsContainer: {
        flex: 1,
        justifyContent: 'space-evenly'
    },
    email: {
        marginTop: normalize(7),
        marginBottom: normalize(11)
    },
    edit: {
        flex: 1,
        marginTop: normalize(25)
    },
    navigationOptionsList: {
        marginHorizontal: normalize(14),
        marginVertical: normalize(30)
    },
    line: {
        backgroundColor: Colors.WHITE_ALPHA_10,
        height: 0.5,
        width: '100%',
    },
    navigationOption: {
        height: normalize(54),
        justifyContent: 'center'
    }
})