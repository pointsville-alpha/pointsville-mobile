import React from 'react'
import {
    StyleSheet,
    View,
    ScrollView,
    Keyboard, Alert
} from 'react-native'
import {
    Button,
    InputField,
    Label,
    Logo,
    PasswordField,
    ScreenContainer
} from '../../components'
import {
    SIGN_IN_TITLE,
    FORGOT_PASSWORD_QUESTION,
    PASSWORD_TITLE,
    EMAIL_TITLE,
    normalize,
    FORGOT_PASSWORD_NAVIGATION_KEY,
    MAIN_NAVIGATION_KEY,
    PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE,
    PLEASE_ENTER_AN_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE, ALERT_TITLE
} from '../../utilities'
import {
    validateEmail,
    validatePassword,
    isEmpty
} from '../../utilities/modules/FieldValidation'
import SocialLogin from './components/SocialLogin'
import { SignIn, } from 'aws-amplify-react-native'
import { Auth } from 'aws-amplify'
import Loader from '../../utilities/modules/Loader'
interface Props {
    navigation: any
}

interface ISignInState {
    username: string
    password: string
    usernameWarning: string
    passwordWarning: string
    isKeypadShown: boolean
    isProcessingRequest: boolean
}

export default class CustomSignIn extends SignIn {

    public keyboardDidShowListener: any
    public keyboardDidHideListener: any

    constructor(props: Props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            usernameWarning: '',
            passwordWarning: '',
            isKeypadShown: false,
            isProcessingRequest: false,
            loading: false
        }
        this._keyboardDidShow = this._keyboardDidShow.bind(this)
        this._keyboardDidHide = this._keyboardDidHide.bind(this)
    }


    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow,
        )
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide,
        )
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove()
        this.keyboardDidHideListener.remove()
    }

    _keyboardDidShow() {
        this.setState({ isKeypadShown: true })
    }

    _keyboardDidHide() {
        this.setState({ isKeypadShown: false })
    }

    handleUsernameChange = (username: string) =>
        this.setState({ username, usernameWarning: '' })

    handlePasswordChange = (password: string) =>
        this.setState({ password, passwordWarning: '' })

    validate = () => {
        let isValid = false
        if (isEmpty(this.state.username))
            this.setState({ usernameWarning: PLEASE_ENTER_AN_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE })
        else if (!validateEmail(this.state.username))
            this.setState({ usernameWarning: PLEASE_ENTER_A_VALID_EMAIL_ADDRESS_OR_PHONE_NUMBER_TITLE })
        else isValid = true
        let passwordStatus = validatePassword(this.state.password)
        if (passwordStatus.success)
            isValid = isValid && true
        else {
            isValid = isValid && false
            this.setState({ passwordWarning: passwordStatus.error! })
        }
        return isValid
    }

    signIn = async () => {
        if (this.validate()) {
            //this.props.navigation.replace(MAIN_NAVIGATION_KEY)
            this.toggleLoader(true)
            await Auth.signIn(this.state.username, this.state.password).then(result => {

                Auth.currentAuthenticatedUser().then(user => {
                    console.log("authenticated user", result)                    
                    this.props.whoAmI({
                        "givenName": user.attributes.given_name,
                        "phoneNumber": user.attributes.phone_number,
                        "email": user.attributes.email,
                    })
                    this.toggleLoader(false)
                    
                }).catch(e => {
                    Alert.alert(ALERT_TITLE, e.message)
                    this.toggleLoader(false)
                })
            }).catch(e => {
                console.log('error', e)
                this.toggleLoader(false)
                if(e.code === 'UserNotConfirmedException'){
                   Auth.resendSignUp(this.state.username).then(result=>{
                        this.props.onStateChange('confirmSignUp',this.state.username)
                    }).catch(e=>{
                        Alert.alert(ALERT_TITLE, e.message)    
                    })
                }else {
                    Alert.alert(ALERT_TITLE, e.message)
                }
                
            })
        }
    }

    onPressForgotPassword = () => {
        //this.props.navigation.navigate(FORGOT_PASSWORD_NAVIGATION_KEY)
        this.props.onStateChange('forgotPassword', {})
    }

    toggleLoader = (loading) =>{
        this.setState({
            loading
        })
    }


    showComponent(theme) {
        return <ScreenContainer>
            <Loader loading={this.state.loading} />
            <ScrollView showsVerticalScrollIndicator={false}
                style={styles.scrollViewStyle} >
                <View style={[
                    styles.logoContainer,
                    this.state.isKeypadShown && styles.keyBoardShown
                ]}>
                    <Logo size='large' />
                </View>
                <View style={styles.mainContainer}>
                    <Label children={SIGN_IN_TITLE}
                        style={styles.signInLabel}
                        fontSize='extra_medium'
                        fontFamily='semiBold' />
                    <InputField warning={this.state.usernameWarning}
                        containerStyle={styles.emailField}
                        placeholder={EMAIL_TITLE}
                        value={this.state.username}
                        onChangeText={this.handleUsernameChange} />
                    <PasswordField warning={this.state.passwordWarning}
                        containerStyle={styles.passwordField}
                        placeholder={PASSWORD_TITLE}
                        value={this.state.password}
                        onChangeText={this.handlePasswordChange} />
                    <Label children={FORGOT_PASSWORD_QUESTION}
                        style={styles.forgotPasswordLabel}
                        fontSize='regular'
                        fontFamily='regular'
                        onPress={this.onPressForgotPassword} />
                </View>
                <Button onPress={this.signIn}
                    style={styles.button}
                    title={SIGN_IN_TITLE} />
                <SocialLogin changeAuthState={(state: any) => {
                    this.props.onStateChange(state, {})
                }} />
            </ScrollView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    scrollViewStyle: {
        width: '100%'
    },
    mainContainer: {
        width: '84%',
        alignSelf: 'center'
    },
    logoContainer: {
        marginTop: normalize(86),
        marginBottom: normalize(54),
        alignItems: 'center',
        justifyContent: 'center'
    },
    keyBoardShown: {
        marginTop: normalize(44),
        marginBottom: normalize(30)
    },
    signInLabel: {
        marginBottom: normalize(26)
    },
    emailField: {
        height: normalize(78)
    },
    passwordField: {
        height: normalize(89)
    },
    forgotPasswordLabel: {
        marginBottom: normalize(41),
        alignSelf: 'flex-end'
    },
    button: {
        marginBottom: normalize(50),
        width: '84%'
    }
})