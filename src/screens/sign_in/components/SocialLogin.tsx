import React, { useState } from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
import {
    normalize,
    Colors,
    SIGN_UP_WITH_TITLE,
    SOCIAL_LOGIN_COLLAPSED_ITEMS,
    SOCIAL_LOGIN_ALL_ITEMS
} from '../../../utilities'
import { Label } from '../../../components'
import { FlatList } from 'react-native'
import SocialIcon from './SocialIcon'

export default function SocialLogin(props) {
    const [expanded, expand] = useState(false)
    return <View style={styles.container}>
        <View style={styles.titleSection}>
            <View style={styles.line} />
            <Label children={SIGN_UP_WITH_TITLE}
                color='WHITE_ALPHA_50'
                style={styles.title}
            />
            <View style={styles.line} />
        </View>
        <FlatList horizontal
            showsHorizontalScrollIndicator={false}
            data={expanded ? SOCIAL_LOGIN_ALL_ITEMS : SOCIAL_LOGIN_COLLAPSED_ITEMS}
            keyExtractor={(item) => item}
            renderItem={({ item }) => <SocialIcon item={item} expand={() => expand(true)} changeAuthState={props.changeAuthState} />}
            ItemSeparatorComponent={() => <View style={styles.iconSeparator} />}
        />
    </View>
}

const styles = StyleSheet.create({
    container: {
        width: normalize(292),
        minHeight: normalize(97),
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: normalize(20)
    },
    titleSection: {
        width: normalize(200),
        height: normalize(38),
        marginBottom: normalize(15),
        flexDirection: 'row',
        alignItems: 'center'
    },
    line: {
        flex: 1,
        backgroundColor: Colors.WHITE_ALPHA_10,
        height: 1
    },
    title: {
        padding: normalize(9)
    },
    iconSeparator: {
        width: normalize(12)
    }
})