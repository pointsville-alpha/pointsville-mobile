import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import {
    Colors,
    normalize,
    MORE_SIGN_UP,
    MAIL_SIGN_UP,
    GOOGLE_SIGN_UP,
    FACEBOOK_SIGN_UP,
    INSTAGRAM_SIGN_UP,
    TWITTER_SIGN_UP,
    LINKEDIN_SIGN_UP
} from '../../../utilities'

import {
    MailIcon,
    GoogleIcon,
    FacebookIcon,
    MoreIcon,
    InstagramIcon,
    TwitterIcon,
    LinkedinIcon
} from '../../../assets/svg_icons'
//import { useNavigation } from '@react-navigation/native'
import loginHandler from '../social/LoginHandler'

interface ISocialIconProps {
    expand: () => void
    item: string,
    changeAuthState: ()=> void
}

function icon(item: string) {
    switch (item) {
        case MAIL_SIGN_UP: return <MailIcon />
        case GOOGLE_SIGN_UP: return <GoogleIcon />
        case FACEBOOK_SIGN_UP: return <FacebookIcon />
        case MORE_SIGN_UP: return <MoreIcon />
        case INSTAGRAM_SIGN_UP: return <InstagramIcon />
        case TWITTER_SIGN_UP: return <TwitterIcon />
        case LINKEDIN_SIGN_UP: return <LinkedinIcon />
        default: return null
    }
}

export default function SocialIcon({ expand, item, changeAuthState }: ISocialIconProps) {
    //const navigation = useNavigation()

    
    const onPress = () =>
        item === MORE_SIGN_UP ? expand() :
            loginHandler(item, changeAuthState/*, navigation*/)
    return <TouchableOpacity onPress={onPress}
        style={styles.container}
        children={icon(item)} />
}

const styles = StyleSheet.create({
    container: {
        width: normalize(64),
        height: normalize(44),
        borderRadius: normalize(22),
        backgroundColor: Colors.WHITE_ALPHA_5,
        alignItems: 'center',
        justifyContent: 'center'
    }
})