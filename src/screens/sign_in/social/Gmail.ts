import { Platform } from 'react-native'
import SocialLogin from './SocialLogin'
import {
    GoogleSignin,
    statusCodes
} from '@react-native-community/google-signin'

const GMAIL_IOS_CLIENT_ID = '351004925339-3c62d8c3e973ree2n0tarqag9ih8056o.apps.googleusercontent.com'

const ANDROID_WEB_CLIENT_ID = '351004925339-01jlamcnprj47v2uk3b2440cnnlr2ea8.apps.googleusercontent.com'

export default class Gmail extends SocialLogin {

    constructor(callback: Function) {
        super(callback)
        this.init()
    }

    init(): void {
        let configure = {}
        if (Platform.OS === 'ios') {
            configure = { iosClientId: GMAIL_IOS_CLIENT_ID }
        }
        else {
            configure = { webClientId: ANDROID_WEB_CLIENT_ID }
        }
        try {
            console.log('** Configuring Gmail Login **')
            GoogleSignin.configure(configure)
        }
        catch (err) {
            console.log("Google error: ", err.code, err.message)
            this.showErrorMessage("Google error: " + err.message)
        }
    }

    login(): void {
        this.signIn()
    }

    signIn = async () => {
        try {
            console.log('** Gmail SignIn **')
            await GoogleSignin.hasPlayServices()
            const userInfo = await GoogleSignin.signIn()
            let resultData = {
                name: userInfo.user.name,
                email: userInfo.user.email,
                photoURL: userInfo.user.photo,
            }
            this.callBack(resultData, true)
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('User Cancelled the Login Flow')
                this.callBack(undefined, false)
            } else if (error.code === statusCodes.IN_PROGRESS) {
                this.showErrorMessage('Signing In')
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                this.showErrorMessage('Play Services Not Available or Outdated')
            } else {
                this.showErrorMessage(error.code)
            }
        }
    }

    logout(): void {
        this.signOut()
    }

    signOut = async () => {
        try {
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
            this.callBack(undefined, false)
        } catch (error) {
            console.error('** SignOut ** error::', error)
            this.showErrorMessage(error)
            this.callBack(undefined, false)
        }
    };
}