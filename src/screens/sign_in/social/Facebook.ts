import {
    LoginManager,
    AccessToken,
    GraphRequestManager,
    GraphRequest
} from 'react-native-fbsdk'
import SocialLogin from './SocialLogin'

export default class Facebook extends SocialLogin {

    constructor(callback: Function) {
        super(callback)
    }

    init(): void {

    }

    login(): void {
        LoginManager.logInWithPermissions(['public_profile', 'email']).then((result: any) => {
            if (result.isCancelled) {
                console.log('** Facebook Login Canceled **')
                this.callBack(undefined, false)
            } else {
                AccessToken.getCurrentAccessToken().then((data: any) => {
                    const { accessToken } = data
                    this.fetchUserInfo(accessToken)
                })
            }
        },
            (error: any) => {
                this.showErrorMessage('FB Login fail with error: ' + error)
            }
        )
    }

    logout(): void {
        LoginManager.logOut()
        this.callBack(undefined, false)
    }

    fetchUserInfo = (accessToken: string) => {
        const FB_FIELDS = 'email,gender,name,first_name,last_name,picture.type(large)'
        const config = {
            accessToken: accessToken.toString(),
            parameters: { fields: { string: FB_FIELDS } }
        }
        const infoRequest = new GraphRequest('/me', config, this._responseInfoCallback)
        new GraphRequestManager().addRequest(infoRequest).start()
    }

    _responseInfoCallback = (error: any, result: any) => {
        if (error) {
            this.showErrorMessage('Error fetching data: ' + error.toString());
        } else {
            console.log('** FB Response result::', result)
            let resultData = {
                name: result.name,
                email: result.email,
                photoURL: result.picture.data.url,
            }
            this.callBack(resultData, true)
        }
    }
}