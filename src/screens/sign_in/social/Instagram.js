import React from 'react'
import {
    View,
    StyleSheet
} from 'react-native'
// import InstagramLogin from 'react-native-instagram-login'
import { Colors, MAIN_NAVIGATION_KEY } from '../../../utilities'
import { withNavigation } from 'react-navigation'

export const INSTAGRAM_APP_ID = '1152132335126482'
export const INSTAGRAM_APP_SECRET = 'b6612e3b85d5181794b1db27a81ef85b'
export const INSTAGRAM_REDIRECT_URL = 'https://jdrf.org/instagram/auth/'

class InstagramSignIn extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            token: '',
            userId: '',
            userName: '',
        }
    }

    componentDidMount() {
        this.handleInstagramPressed()
    }

    saveToken = async (data) => {
        this.setState(
            { token: data.access_token, userId: data.user_id },
            () => this.props.navigation.replace(MAIN_NAVIGATION_KEY)
        )
    }

    handleInstagramPressed() {
        // this.instagramLogin.show()
    }

    handleClose() {
        // this.props.navigation.goBack()
    }

    render() {
        return (
            <View style={styles.container}>
                {/* <InstagramLogin
                    ref={ref => (this.instagramLogin = ref)}
                    appId={INSTAGRAM_APP_ID}
                    appSecret={INSTAGRAM_APP_SECRET}
                    redirectUrl={INSTAGRAM_REDIRECT_URL}
                    scopes={['basic']}
                    onLoginSuccess={this.saveToken}
                    onLoginFailure={(data) => console.log(data)}
                    onClose={() => this.handleClose()}
                /> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.WHITE
    }
})

export default withNavigation(InstagramSignIn)