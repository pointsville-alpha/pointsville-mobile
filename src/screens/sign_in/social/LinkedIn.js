import React from 'react'
import {
    Alert,
    View,
    StyleSheet
} from 'react-native'
import { Colors } from '../../../utilities'
// import LinkedInModal from 'react-native-linkedin'

export const LINKEDIN_CLIENT_ID = '81jpsbe0fcgj6w'
export const LINKEDIN_CLIENT_SECRET = 'nqd91TXj1t1QzjOZ'

export const LINKEDIN_REDIRECT_URL = 'https://www.linkedin.com/oauth2/accessToken'
export const LINKEDIN_FETCH_USER_INFO = 'https://api.linkedin.com/v2/me'

export default class LinkedinSignIn extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            token: '',
            userDetails: {}
        }
        linkedRef = undefined
    }

    componentDidMount() {
        this.handleLinkedInPress()
    }

    getUserDetails = async (token) => {
        const response = await fetch(LINKEDIN_FETCH_USER_INFO, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + token,
            },
        })
        const payload = await response.json()
        console.log("Payload", payload)
        this.setState({ userDetails: payload }, () => Alert.alert('Token and Payload Saved'))
    }

    handleLinkedInPress() {
        this.linkedRef.open()
    }

    handleClose() {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <View style={styles.container}>
                {/* <LinkedInModal
                    ref={ref => this.linkedRef = ref}
                    clientID={LINKEDIN_CLIENT_ID}
                    clientSecret={LINKEDIN_CLIENT_SECRET}
                    redirectUri={LINKEDIN_REDIRECT_URL}
                    onError={error => Alert.alert(error.message ?? 'Linked-In sign-in error')}
                    onClose={() => this.handleClose()}
                    onSuccess={tokenData => {
                        this.setState(
                            { token: tokenData.access_token }
                            , () => this.getUserDetails(tokenData.access_token)
                        )
                    }}
                /> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.WHITE
    }
})