import { Auth } from 'aws-amplify'
import React from 'react'
import { Alert } from 'react-native'
import {
    GOOGLE_SIGN_UP,
    FACEBOOK_SIGN_UP,
    MAIN_NAVIGATION_KEY,
    MAIL_SIGN_UP,
    SIGN_UP_NAVIGATION_KEY,
    INSTAGRAM_SIGN_UP,
    INSTAGRAM_NAVIGATION_KEY,
    LINKEDIN_SIGN_UP,
    LINKEDIN_NAVIGATION_KEY
} from '../../../utilities'
import Facebook from '../social/Facebook'
import Gmail from '../social/Gmail'

const loginHandler = (key: string, changeAuthState:()=>void/*, navigation: any*/) => {
    switch (key) {
        case MAIL_SIGN_UP:
            //navigation.navigate(SIGN_UP_NAVIGATION_KEY)
            changeAuthState('signUp')
            break
        case GOOGLE_SIGN_UP:
            //loginGoogle(navigation)
            Auth.federatedSignIn({provider: 'Google'}).then(
                result=>{
                    console.log("Google SignIn Response",JSON.stringify(result))
                }
            ).catch(
                e=>{
                    console.log("Google SignIn Error",JSON.stringify(e))
                }
            )
            break
        case FACEBOOK_SIGN_UP:
            //loginFacebook(navigation)
            Auth.federatedSignIn({provider: 'Facebook'})
            break
        case INSTAGRAM_SIGN_UP:
            //navigation.navigate(INSTAGRAM_NAVIGATION_KEY)
            break
        // case LINKEDIN_SIGN_UP:
        //     navigation.navigate(LINKEDIN_NAVIGATION_KEY)
    }
}

const loginFacebook = (navigation: any) => {
    let social = new Facebook((result: any) => {
        if (result) {
            result.email ?
                navigation.replace(MAIN_NAVIGATION_KEY)
                : Alert.alert(result)
        }
    })
    social?.login()
}

const loginGoogle = (navigation: any) => {
    let social = new Gmail((result: any) => {
        if (result) {
            result.email ?
                navigation.replace(MAIN_NAVIGATION_KEY)
                : Alert.alert(result)
        }
    })
    social?.login()
}

export default loginHandler