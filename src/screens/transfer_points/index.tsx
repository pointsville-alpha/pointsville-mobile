import React from 'react'
import {
    Alert,
    StyleSheet,
    View
} from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { SafeAreaView } from 'react-navigation'
import {
    Button,
    Label,
    LighterBackground,
    ScreenContainer
} from '../../components'
import {
    ENTER_WALLET_ID_DESCRIPTION,
    normalize,
    OK_TITLE,
    PROCEED_TITLE
} from '../../utilities'
import InfoSection from './components/InfoSection'
import InputWalletID from './components/InputWalletID'
import PointsSection from './components/PointsSection'

interface Props {
    navigation: any
    route: any
}

interface Istate {
    transferPoints: number
    walletID: string
}

export default class TransferPoints extends React.Component<Props, Istate> {

    constructor(props: Props) {
        super(props)
        this.state = {
            transferPoints: 50,
            walletID: ''
        }
    }

    transfer = () => {
        Alert.alert(
            'Success',
            'You have transferred ' + this.state.transferPoints + ' points.',
            [
                { text: OK_TITLE, onPress: () => this.props.navigation.pop() }
            ],
            { cancelable: false }
        )
    }

    render() {
        return <ScreenContainer>
            <LighterBackground style={styles.lighterBackground} />
            <SafeAreaView style={styles.container}>
                <ScrollView contentContainerStyle={styles.padding}
                    scrollEnabled={false}
                    showsVerticalScrollIndicator={false}>
                    <InfoSection team={this.props.route.params.team}
                        points={this.props.route.params.points}
                        total={this.props.route.params.total}
                        price={this.props.route.params.price}
                        image={this.props.route.params.image} />
                    <View style={styles.container}>
                        <PointsSection points={this.state.transferPoints}
                            setPoints={transferPoints => this.setState({ transferPoints })} />
                        <InputWalletID text={this.state.walletID}
                            onEdit={walletID => this.setState({ walletID })} />
                        <Label children={ENTER_WALLET_ID_DESCRIPTION}
                            numberOfLines={6}
                            style={styles.text}
                            textAlign='center'
                            color='WHITE_ALPHA_50'
                            fontSize='small' />
                    </View>
                    <Button onPress={this.transfer}
                        title={PROCEED_TITLE} />
                </ScrollView>
            </SafeAreaView>
        </ScreenContainer>
    }
}

const styles = StyleSheet.create({
    lighterBackground: {
        position: 'absolute'
    },
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    padding: {
        flex: 1,
        width: '84%',
        alignSelf: 'center'
    },
    text: {
        marginBottom: normalize(83)
    }
})