import React from 'react'
import {
    StyleSheet,
    Text,
    View
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import {
    Minus,
    Plus
} from '../../../assets/svg_icons'
import { Label } from '../../../components'
import {
    Colors,
    normalize,
    PTS_TITLE
} from '../../../utilities'

interface IPointsSectionProps {
    points: number
    setPoints: (points: number) => void
}

export default function PointsSection(props: IPointsSectionProps) {
    const reducePoints = (points: number) => points > 10 ? points - 10 : 10
    const increasePoints = (points: number) => points < 200 ? points + 10 : 200
    return <View style={styles.container}>
        <View style={styles.pointsContainer}>
            <TouchableOpacity
                onPress={() => props.setPoints(reducePoints(props.points))}>
                <Minus />
            </TouchableOpacity>
            <Text children={props.points}
                maxFontSizeMultiplier={1.5}
                style={styles.points} />
            <TouchableOpacity
                onPress={() => props.setPoints(increasePoints(props.points))}>
                <Plus />
            </TouchableOpacity>
        </View>
        <Label children={PTS_TITLE}
            color='WHITE_ALPHA_50'
            fontSize='small' />
    </View>
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginBottom: normalize(47)
    },
    pointsContainer: {
        width: '100%',
        flexDirection: 'row',
        paddingHorizontal: normalize(35),
        justifyContent: 'space-between',
        alignItems: 'flex-end'
    },
    points: {
        fontSize: normalize(28),
        lineHeight: normalize(34),
        color: Colors.WHITE,
        letterSpacing: 1,
        fontFamily: 'Graphik-semiBold',
        fontWeight: 'bold'
    }
})