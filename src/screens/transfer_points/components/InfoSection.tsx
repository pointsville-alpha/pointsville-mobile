import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import SvgUri from 'react-native-svg-uri'
import { imageSelect } from '../../../assets/ImageSelect'
import {
    BackButton,
    Label
} from '../../../components'
import Coin from '../../../components/atomic/Coin'
import {
    Colors,
    normalize,
    POINTS_COLON,
    VALUE_COLON
} from '../../../utilities'

interface Props {
    image: string
    points: string
    team: string
    price: string
    total: string
}

export default function InfoSection(props: Props) {
    return <View style={styles.container}>
        <SvgUri style={styles.image} height={normalize(50)} width={normalize(100)} source={{uri:props.image}}/>
        <Label children={props.team}
            style={styles.team}
            fontSize='medium'
            fontFamily='medium' />
        <Label children={props.price}
            fontSize='small' />
        <View style={styles.infoRow}>
            <View style={styles.pointsRow}>
                <Label children={POINTS_COLON}
                    color='WHITE_ALPHA_50'
                    fontSize='small_medium' />
                <Coin style={styles.pointsIcon} />
                <Label children={props.points}
                    fontFamily='medium' />
            </View>
            <View style={styles.separator} />
            <View style={[styles.pointsRow, styles.valueRow]}>
                <Label children={VALUE_COLON}
                    color='WHITE_ALPHA_50'
                    fontSize='small_medium' />
                <Label children={props.total}
                    style={styles.valueText}
                    fontFamily='medium' />
            </View>
        </View>
        <View style={styles.backbutton} >
            <BackButton />
        </View>
    </View>
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '100%',
        padding: normalize(15)
    },
    image: {
        height: normalize(50),
        width: normalize(100),
        marginBottom: normalize(11),
        justifyContent: 'center',
        alignItems: 'center'
    },
    team: {
        marginBottom: normalize(10)
    },
    infoRow: {
        marginTop: normalize(16),
        flexDirection: 'row',
        alignItems: 'center'
    },
    pointsRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    pointsIcon: {
        marginStart: normalize(6),
        marginEnd: normalize(4)
    },
    separator: {
        height: normalize(16),
        width: 1,
        backgroundColor: Colors.WHITE_ALPHA_10,
        marginHorizontal: normalize(15)
    },
    valueText: {
        marginStart: normalize(4)
    },
    valueRow: {
        justifyContent: 'flex-start'
    },
    backbutton: {
        position: 'absolute',
        top: normalize(16),
        start: 0
    }
})