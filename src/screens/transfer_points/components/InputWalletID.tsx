import React from 'react'
import {
    StyleSheet,
    View
} from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import { ScanIcon } from '../../../assets/svg_icons'
import {
    Colors,
    ENTER_WALLET_ID_TITLE,
    FONTSIZE,
    normalize
} from '../../../utilities'

interface IInputWalletIDProps {
    text: string
    onEdit: (text: string) => void
}

export default function InputWalletID(props: IInputWalletIDProps) {
    return <View style={styles.container}>
        <TextInput value={props.text}
            onChangeText={props.onEdit}
            placeholder={ENTER_WALLET_ID_TITLE}
            placeholderTextColor={Colors.WHITE_ALPHA_50}
            style={styles.input} />
        <ScanIcon />
    </View>
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        justifyContent: 'space-between',
        height: normalize(54),
        borderRadius: normalize(6),
        borderColor: Colors.WHITE_ALPHA_30,
        borderWidth: 0.5,
        flexDirection: 'row',
        marginBottom: normalize(20),
        alignItems: 'center',
        paddingHorizontal: normalize(15)
    },
    input: {
        alignItems: 'center',
        color: Colors.WHITE,
        fontSize: FONTSIZE.medium,
        lineHeight: FONTSIZE.large,
        marginEnd: normalize(8),
        flex: 1
    }
})