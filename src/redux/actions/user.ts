import { SETPROFILE } from './types'

export const setProfile = (user: Object) => ({
  type: SETPROFILE,
  payload: {
    user
  }
})