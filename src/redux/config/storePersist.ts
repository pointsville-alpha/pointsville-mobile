import AsyncStorage from '@react-native-community/async-storage'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

interface Persist {
  key: string
  storage: any
  stateReconciler: any
}

// Need to include transform inside config
const persistConfig: Persist = {
  key: '_root',
  stateReconciler: autoMergeLevel2,
  storage: AsyncStorage
}

export default persistConfig