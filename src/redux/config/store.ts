import { createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import rootReducer from '../reducers'
import persistConfig from './storePersist'

const persistedReducer = persistReducer(persistConfig, rootReducer)
export default () => {
  const store = createStore(persistedReducer)
  const persistor = persistStore(store)
  return { store, persistor }
}