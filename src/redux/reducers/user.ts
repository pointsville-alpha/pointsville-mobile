import { SETPROFILE } from '../actions/types'
import { user } from '../states/user'

export const userReducer = (state: any = user, action: any) => {
  switch (action.type) {
    case SETPROFILE:
      return {
        user: {
          ...state.user,
          ...action.payload
        }
      }
    default:
      return state
  }
}