import React, { useEffect, useRef, useState, useLayoutEffect } from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity, Text } from 'react-native';
import _ from 'lodash';
import { WikitudeView } from 'react-native-wikitude-sdk';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Share from "react-native-share";
import Geolocation from '@react-native-community/geolocation';


const styles = StyleSheet.create({
  AR: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
});
const LOCAL_POIS_DATA = [
  {"id":"1","longitude":"20.0339","latitude":"19.9849","description":"This is the description of POI#1","name":"POI#1"},
  {"id":"2","longitude":"20.0549","latitude":"19.9929","description":"This is the description of POI#2","name":"POI#2"},
  {"id":"3","longitude":"19.9309","latitude":"20.0569","description":"This is the description of POI#3","name":"POI#3"},
  {"id":"4","longitude":"19.9849","latitude":"20.0259","description":"This is the description of POI#4","name":"POI#4"},
  {"id":"5","longitude":"19.9649","latitude":"19.9209","description":"This is the description of POI#5","name":"POI#5"},
  {"id":"6","longitude":"19.9129","latitude":"19.8859","description":"This is the description of POI#6","name":"POI#6"},
  {"id":"7","longitude":"20.0209","latitude":"20.0689","description":"This is the description of POI#7","name":"POI#7"},
  {"id":"8","longitude":"19.8809","latitude":"19.9589","description":"This is the description of POI#8","name":"POI#8"},
  {"id":"9","longitude":"19.8889","latitude":"19.8819","description":"This is the description of POI#9","name":"POI#9"},
  {"id":"10","longitude":"20.0529","latitude":"19.9889","description":"This is the description of POI#10","name":"POI#10"}];
const Wikitude = ({ route, navigation }) => {
  const wikitudeView = useRef(null);
  const [isFocus, setFocus] = useState(false);
  const [latitude, setLatitude] = useState(19.9799004);
  const [longitude, setLongitude] = useState(73.7453953);

  console.log('URL WIKITUDE: ', route.params.url);

  useEffect(() => {
    const subscribeFocus = navigation.addListener('blur', () => {
      console.log('Is blur');
      if (wikitudeView.current) {
        console.log("wikitudeView Ref BLUR", 'injectLocation' in wikitudeView.current);
        //wikitudeView.current.stopRendering();
      }
      setFocus(false);
    });
    navigation.addListener('focus', () => {
      console.log('Is focus', wikitudeView.current);
      setFocus(true);
      if (wikitudeView.current) {
        console.log("wikitudeView Ref FoCUS", 'injectLocation' in wikitudeView.current);
        //console.log("wikitudeView Ref", wikitudeView.current);
        //wikitudeView.current.resumeRendering();
      }
    });
    navigation.setOptions({
      headerRight: () => (
        <Icon name="camera" size={40} onPress={() => takeScreenshot()} />
      ),
    });

    Geolocation.getCurrentPosition(
      position => {
        
        console.log("GEO initialPosition", JSON.stringify(position));
        setLatitude(position.coords.latitude);
        setLongitude(position.coords.longitude);
      },
      error => console.log('Error', JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
    Geolocation.watchPosition(position => {
      const lastPosition = JSON.stringify(position);
      console.log("GEO lastPosition", lastPosition);
      setLatitude(position.coords.latitude);
      setLongitude(position.coords.longitude);
    });
    return subscribeFocus;
  }, [navigation]);

  useLayoutEffect(() => {
    console.log("componentDidUpdateFunction");
    if (wikitudeView.current) {
      wikitudeView.current.injectLocation(latitude, longitude);
      setTimeout(() => {
        wikitudeView.current.callJavascript('World.loadPoisFromJsonData(' + JSON.stringify(LOCAL_POIS_DATA) +')')  
      }, 1000);
      
      return;
    }
  })

  const config = {
    licenseKey:
      'ACbHymO+o2lXRuA/ctX/qCGHQR0PCVvmF8vRFxmYe3Zne40oOjc2NJj1fAW6TL3RE7vUF7PHXdgzDC1XoOVa7hbTEeWpx7CCKg+/GkZUAktGiIzB7Z0/eL7qIlUE2+nplWo6SnhFWeIrKewk1ykNPhaLHavJR0PtCHI7lmfFlcBTYWx0ZWRfX8LMJBINokov1F8kSAhv2UeES1Yuqs8B4efNbpXZY4qttY+Q62Bgu1F8dJPUgjM9bCWyibgoQNizxW2uxJFXLiPliPJCUDDILYEHUa+LdfGWgAS2SmOOdDLSv/9FZ17ZikSyzMBc4UXo+P4VzqaHGIe/RJIvs+xRAhUyKDbdaB4pOgQaH4Srra2myP6Dv3SDxvU2N4XMT2zWY7d2SdmTLvf2oeYjdxf6Q8MZEEjc1Z081z3V/pJu2HFjcSeEz8RBh2jqy2OIk81qYuZsbu4UZJDQmDTRK8UJiaan2wA5BVSdpH9MI4dkJMMs1WAe29HB7eG05bYmNQOchzsmmgLpAniP3TwQpyYCLl+VqePDsi04ZNmuzYTLqpJLpW6M6tzNUtrvBo+9E5DZ8C0vRL7tg38x1RHZuYE5iDnIbhr+YNqSLRr1selBQZXsjMDA+Ci7tmHKymyx2p4fZe+fpzTE5MG+R0QTY6LunLV4wL8jrkz7i5R9sLYVPW9puTjkMBuQF4FI+JUH5oUVboDnY6Pr3w9CxrrtdYVbXm2buAbPMgbPGl3zLTxoSuCt4tRieGzv3QocB2DRXsRl+hcS5KEhOMw8Bjy5z6WhnSFGY+5QBRiIlv2wlMCe5QftxjEQ1UTEDScnKG2NcIPGdM0osmsGTqyeA0U9cw+jaYhhYnIfJBkjwZ2ymf0b1vM=',
    url: route.params.url,
    isPOI: true,
  };
  const takeScreenshot = () => {
    if (wikitudeView.current) {
      wikitudeView.current.captureScreen(true);
    }
  
    //wikitudeView.current.callJavascript('World.loadPoisFromJsonData(' + JSON.stringify(LOCAL_POIS_DATA) +')')
  };
  const onFinishLoading = event => {
    // event.success = true
    //This event is called when the html is finish loaded
    //For fail event, use onFailLoading
    console.log('On Finish Loading');
  };
  const onJsonReceived = object => {
    console.log("onJsonReceived",JSON.stringify(object));

  };

  const onFailLoading = event => {
    console.log(event);
  };

  const onScreenCaptured = event => {
    if (!event.image) return;

    Share.open({
      message: 'Share this awesome AR with Wikitude',
      title: 'SpinAR Team',
      url: event.image,
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        err && console.log(err);
      });
  };
  if (!isFocus) {
    return <SafeAreaView />;
  } else {
    return (
      <SafeAreaView style={styles.container}>
        <WikitudeView
          ref={wikitudeView}
          licenseKey={config.licenseKey}
          url={config.url}
          isPOI={config.isPOI}
          style={styles.AR}
          feature={WikitudeView.Geo}
          onFailLoading={onFailLoading}
          onJsonReceived={onJsonReceived}
          onFinishLoading={onFinishLoading}
          onScreenCaptured={onScreenCaptured}
          hasCameraPermissions={true}
        />
      </SafeAreaView>
    );
  }
};
export default Wikitude;