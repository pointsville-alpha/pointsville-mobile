/**
 * @format
 */

import React from 'react';
import { Navigation } from "react-native-navigation";
import App from './App';
import Amplify, { Auth } from 'aws-amplify'
import config from './aws-config'
import { ApolloClient, InMemoryCache, ApolloProvider, HttpLink } from '@apollo/client'
import { setContext } from '@apollo/client/link/context';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import { Linking } from 'react-native'

// XMLHttpRequest = GLOBAL.originalXMLHttpRequest ? 
//   GLOBAL.originalXMLHttpRequest : GLOBAL.XMLHttpRequest;

async function urlOpener(url, redirectUrl) {
  await InAppBrowser.isAvailable();
  const { type, url: newUrl } = await InAppBrowser.openAuth(url, redirectUrl, {
    showTitle: false,
    enableUrlBarHiding: true,
    enableDefaultShare: false,
    ephemeralWebSession: false,
  });

  if (type === 'success') {
    Linking.openURL(newUrl);
  }
}

Amplify.configure({
  ...config,
  oauth: {
    ...config.oauth,
    urlOpener
  }
})

const httpLink = new HttpLink({ uri: 'http://13.127.63.7:4000/graphQL' });

const authLink = setContext(async (_, { headers }) => {
  // get the authentication token from local storage if it exists
  let user = await Auth.currentAuthenticatedUser();
  //console.log(user.signInUserSession.accessToken)
  let token = user?.signInUserSession?.accessToken?.jwtToken;
  // const token = localStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : "",
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});
const appWithApolloClint = () => {
  return (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  )

}
Navigation.registerComponent('com.tringapps.pointsville', () => appWithApolloClint);
Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: 'com.tringapps.pointsville'
            }
          }
        ],
        options: {
          topBar: {
            visible: false,
            height: 0
          }
        }
      }
    }
  });
});
